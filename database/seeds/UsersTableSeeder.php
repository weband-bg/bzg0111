<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'custom_id' => null,
        	'name' => 'bezgishe',
        	'email' => 'bezgishe@gmail.com',
        	'password' => bcrypt('bezgishe'),
        	'admin' => 1
        ]);
    }
}
