<?php

use App\QuestionAnswerCategory;
use Illuminate\Database\Seeder;

class QuestionAnswerCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questionAnswerCategory = new QuestionAnswerCategory;
        $questionAnswerCategory->name = 'Как работим?';
        $questionAnswerCategory->save();

        $questionAnswerCategory = new QuestionAnswerCategory;
        $questionAnswerCategory->name = 'FAQ';
        $questionAnswerCategory->save();
    }
}
