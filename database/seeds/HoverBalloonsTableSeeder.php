<?php

use Illuminate\Database\Seeder;

use App\HoverBalloon;

class HoverBalloonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // EOOD
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Име на фирмата';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = 'Как ще се казва фирмата?';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Как ще се изписва името на чужд език';
        $hover_balloon->form_field = 'company_foreign_language_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление';
        $hover_balloon->form_field = 'different_address';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'address_2_zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'address_2_township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'address_2_populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'address_2_postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'address_2_quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'address_2_street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'address_2_street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'address_2_building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'address_2_entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'address_2_floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'address_2_apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Допълнителни дейности (разделени със запетая)';
        $hover_balloon->form_field = 'additional_activities';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Капитал';
        $hover_balloon->form_field = 'money';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'owner_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'owner_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'owner_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'owner_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'manager[0][firstname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'manager[0][secondname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'manager[0][lastname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'manager[0][id_number]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'manager[0][id_date_created]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 1;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'manager[0][id_created_by]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   OOD
        *
        */
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Име на фирмата';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = 'Как ще се казва фирмата?';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Как ще се изписва името на чужд език';
        $hover_balloon->form_field = 'company_foreign_language_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление';
        $hover_balloon->form_field = 'different_address';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'address_2_zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'address_2_township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'address_2_populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'address_2_postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'address_2_quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'address_2_street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'address_2_street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'address_2_building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'address_2_entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'address_2_floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'address_2_apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Допълнителни дейности (разделени със запетая)';
        $hover_balloon->form_field = 'additional_activities';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Капитал';
        $hover_balloon->form_field = 'money';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner[0][firstname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner[0][secondname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner[0][lastname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'owner[0][pin]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'owner[0][id_number]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'owner[0][id_date_created]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'owner[0][id_created_by]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'manager[0][firstname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'manager[0][secondname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'manager[0][lastname]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'manager[0][id_number]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'manager[0][id_date_created]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'manager[0][id_created_by]';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Заедно и поотделно';
        $hover_balloon->form_field = 'representation-method';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 2;
        $hover_balloon->label = 'Заедно';
        $hover_balloon->form_field = 'representation-method-together';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   ET
        *
        */
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Име на ЕТ';
        $hover_balloon->form_field = 'sole_trader_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Как ще се изписва името на чужд език';
        $hover_balloon->form_field = 'sole_trader_foreign_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление';
        $hover_balloon->form_field = 'different_address';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'address_2_zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'address_2_township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'address_2_populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'address_2_postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'address_2_quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'address_2_street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'address_2_street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'address_2_building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'address_2_entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'address_2_floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'address_2_apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Допълнителни дейности (разделени със запетая)';
        $hover_balloon->form_field = 'additional_activities';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'owner_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'owner_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'owner_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 3;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'owner_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Property weight
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Физическо лице';
        $hover_balloon->form_field = 'individual';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Юридическо лице';
        $hover_balloon->form_field = 'legal-entity';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 4;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /*
        *
        *   Property lawsuit
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Физическо лице';
        $hover_balloon->form_field = 'individual';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Юридическо лице';
        $hover_balloon->form_field = 'legal-entity';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();  

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save(); 

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Улица/Булевард';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Улица/Булевард - номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'entrance';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 5;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'owner_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Owner estates
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Физическо лице';
        $hover_balloon->form_field = 'individual';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Юридическо лице';
        $hover_balloon->form_field = 'legal-entity';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 6;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Company participations
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 7;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 7;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 7;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 7;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Change company activity
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Име на фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'ЕИК на фирма';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Допълнителни дейности (разделени със запетая)';
        $hover_balloon->form_field = 'additional_activities';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Email';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Дата на валидност на лична карта';
        $hover_balloon->form_field = 'owner_id_expiry_date';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 8;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   General commecrial proxy
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Име на фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'manager_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'manager_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'manager_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'manager_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'manager_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'manager_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'manager_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'proxy_manager_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'proxy_manager_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'proxy_manager_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'proxy_manager_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'proxy_manager_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'proxy_manager_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 9;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'proxy_manager_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Wedding contract
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'husband_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'husband_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'husband_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'husband_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'husband_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'husband_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'husband_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'wife_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'wife_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'wife_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'wife_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'wife_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'wife_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'wife_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = '';
        $hover_balloon->form_field = 'mariage_step';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Допълнителни клаузи';
        $hover_balloon->form_field = 'additional_clauses';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'owner_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =10;
        $hover_balloon->label = 'Email';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();


        /**
        *
        *  Car sale proxy 
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'seller_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'seller_eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'seller_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'seller_manager_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'seller_manager_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'seller_manager_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'seller_manager_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'seller_manager_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'seller_manager_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'proxy_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'proxy_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'proxy_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'proxy_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'proxy_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'proxy_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'proxy_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Моля приложете сканирано копие от големия талон на автомобила';
        $hover_balloon->form_field = 'talon';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Тип';
        $hover_balloon->form_field = 'car_type';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Марка';
        $hover_balloon->form_field = 'car_make';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Модел';
        $hover_balloon->form_field = 'car_model';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Номер на шаси';
        $hover_balloon->form_field = 'car_chassis';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Регистрационен номер';
        $hover_balloon->form_field = 'car_registration';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Цвят';
        $hover_balloon->form_field = 'car_color';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Минимална продажна цена (в лева)';
        $hover_balloon->form_field = 'price';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 11;
        $hover_balloon->label = 'Клауза за договаряне сам със себе си';
        $hover_balloon->form_field = 'self_contract_clause';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *  Website owner
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =12;
        $hover_balloon->label = 'Домеин';
        $hover_balloon->form_field = 'domain';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /** 
         *  Estate opening
         *
         */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 13;
        $hover_balloon->label = 'Линк към информация за имот';
        $hover_balloon->form_field = 'info_link';
        $hover_balloon->hover_text = 'Линк към информация за имот';
        $hover_balloon->save();

        /**
        *   
        * Signature sample
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =14;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =14;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =14;
        $hover_balloon->label = 'Памилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =14;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Company fortune
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 15;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 15;
        $hover_balloon->label = 'Име на Фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Switch management address
        *
        */        
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Име на Фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Област';
        $hover_balloon->form_field = 'zone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Община';
        $hover_balloon->form_field = 'township';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Населено място';
        $hover_balloon->form_field = 'populated_place';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
       
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Пощенски код';
        $hover_balloon->form_field = 'postcode';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Квартал';
        $hover_balloon->form_field = 'quarter';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Улица';
        $hover_balloon->form_field = 'street';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Номер';
        $hover_balloon->form_field = 'street_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Блок';
        $hover_balloon->form_field = 'building';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Вход';
        $hover_balloon->form_field = 'vhod';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Етаж';
        $hover_balloon->form_field = 'floor';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Апартамент';
        $hover_balloon->form_field = 'apartment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'И-мейл';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Дата на валидност на лична карта';
        $hover_balloon->form_field = 'owner_id_expiry_date';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =16;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Manager operations
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Име на Фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'ЕИК на фирма';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Смяна, Добавяне или Заличаване на Управител';
        $hover_balloon->form_field = 'operation';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'add_manager_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'add_manager_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'add_manager_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'add_manager_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'add_manager_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'add_manager_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'add_manager_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Име на стар управител';
        $hover_balloon->form_field = 'switch_remove_manager_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Име на стар управител';
        $hover_balloon->form_field = 'switch_manager_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'switch_manager_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'switch_manager_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'switch_manager_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'switch_manager_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Номер на лична карта';
        $hover_balloon->form_field = 'switch_manager_id_number';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Дата на издаване';
        $hover_balloon->form_field = 'switch_manager_id_date_created';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 17;
        $hover_balloon->label = 'Издадена от';
        $hover_balloon->form_field = 'switch_manager_id_created_by';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Company revenue
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 18;
        $hover_balloon->label = 'Име на Фирма';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 18;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Change company name
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 19;
        $hover_balloon->label = 'Настоящо име на фирма';
        $hover_balloon->form_field = 'current_company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 19;
        $hover_balloon->label = 'ЕИК на фирма';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 19;
        $hover_balloon->label = 'Ново име на фирмата';
        $hover_balloon->form_field = 'new_company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id = 19;
        $hover_balloon->label = 'Ново име на фирмата на чужд език';
        $hover_balloon->form_field = 'new_company_foreign_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Annual financial
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Име на фирмата';
        $hover_balloon->form_field = 'company_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'ЕИК на фирмата';
        $hover_balloon->form_field = 'eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Е-мейл';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Дата на валидност на лична карта';
        $hover_balloon->form_field = 'owner_id_expiry_date';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =20;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *   Website sale contract
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'seller_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'seller_eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'seller_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'seller_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'seller_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'seller_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
         
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'buyer_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'buyer_eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

// 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'buyer_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'buyer_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
         
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'buyer_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'buyer_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Домеин';
        $hover_balloon->form_field = 'domain';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        //
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Е-майл';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Дата на валидност на лична карта';
        $hover_balloon->form_field = 'owner_id_expiry_date';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =21;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        /**
        *
        *   Car sale contract
        *
        */ 
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Талон на кола';
        $hover_balloon->form_field = 'talon';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'seller_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'seller_eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
           
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'buyer_name';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'ЕИК';
        $hover_balloon->form_field = 'buyer_eik';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'buyer_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'buyer_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
           
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'buyer_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'ЕГН';
        $hover_balloon->form_field = 'buyer_pin';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Тип кола';
        $hover_balloon->form_field = 'type';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Марка';
        $hover_balloon->form_field = 'make';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Модел';
        $hover_balloon->form_field = 'model';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Номер на шаси';
        $hover_balloon->form_field = 'chassis';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Регистрационен номер';
        $hover_balloon->form_field = 'registration';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
           
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Продажна цена';
        $hover_balloon->form_field = 'price';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Начин на плащане';
        $hover_balloon->form_field = 'payment';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
           
        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Разноски. За сметка на ...';
        $hover_balloon->form_field = 'expenses';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Име';
        $hover_balloon->form_field = 'owner_firstname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Презиме';
        $hover_balloon->form_field = 'owner_secondname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Фамилия';
        $hover_balloon->form_field = 'owner_lastname';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Е-мейл';
        $hover_balloon->form_field = 'owner_email';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();

        $hover_balloon = new HoverBalloon();
        $hover_balloon->module_id =22;
        $hover_balloon->label = 'Телефон';
        $hover_balloon->form_field = 'owner_phone';
        $hover_balloon->hover_text = '';
        $hover_balloon->save();
    }
}