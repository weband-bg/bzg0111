<?php

use Illuminate\Database\Seeder;
use App\CarMake;

class CarMakesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = array('AC', 'Acura', 'Aixam', 'Alfa Romeo', 'Aston martin', 'Audi', 'Austin', 'BMW', 'Bentley', 'Berliner', 'Borgward', 'Brilliance', 'Bugatti', 'Buick', 'Cadillac', 'Chevrolet', 'Chrysler', 'Citroen', 'Corvette', 'Dacia', 'Daewoo', 'Daihatsu', 'Daimler', 'Datsun', 'Dkw', 'Dodge', 'Dr', 'Eagle', 'FSO', 'Ferrari', 'Fiat', 'Ford', 'Geo', 'Great Wall', 'Heinkel', 'Honda', 'Hyundai', 'Ifa', 'Infiniti', 'Innocenti', 'Isuzu', 'Jaguar', 'Kia', 'Lada', 'Lamborghini', 'Lancia', 'Lexus', 'Lifan', 'Lincoln', 'Lotus', 'Maserati', 'Matra', 'Maybach', 'Mazda', 'McLaren', 'Mercedes-Benz', 'Mercury', 'Mg', 'Microcar', 'Mini', 'Mitsubishi', 'Morgan', 'Moskvich', 'Nissan', 'Oldsmobile', 'Opel', 'Perodua', 'Peugeot', 'Pgo', 'Plymouth', 'Polonez', 'Pontiac', 'Porsche', 'Proton', 'Renault', 'Rolls-Royce', 'Rover', 'SECMA', 'Saab', 'Samand', 'Saturn', 'Scion', 'Seat', 'Shatenet', 'Shuanghuan', 'Simca', 'Skoda', 'Smart', 'Ssang yong', 'Subaru', 'Suzuki', 'Talbot', 'Tata', 'Tavria', 'Tazzari', 'Terberg', 'Tesla', 'Tofas', 'Toyota', 'Trabant', 'Triumph', 'VROMOS', 'VW', 'Volga', 'Volvo', 'Warszawa', 'Wartburg', 'Wiesmann', 'Xinshun', 'Zastava', 'Zaz', 'Други', 'Победа', 'София', 'Чайка');

        foreach ($brands as $brand) {
            $make = new CarMake();
            $make->make = $brand;
            $make->save();
        }
    }
}
