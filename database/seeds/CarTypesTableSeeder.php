<?php

use Illuminate\Database\Seeder;
use App\CarType;

class CarTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new CarType();
        $type->type = 'Лек Автомобил';
        $type->save();

        $type = new CarType();
        $type->type = 'Товарен';
        $type->save();

        $type = new CarType();
        $type->type = 'Мотоциклет';
        $type->save();
    }
}
