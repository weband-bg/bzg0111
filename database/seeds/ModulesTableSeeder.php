<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = new Module();
        $module->name = 'Регистрация на ЕООД';
        $module->save();

        $module = new Module();
        $module->name = 'Регистрация на ООД';
        $module->save();

        $module = new Module();
        $module->name = 'Регистрация на ЕТ';
        $module->save();

        $module = new Module;
        $module->name = 'Има ли тежести върху имота ми';
        $module->save();

        $module = new Module;
        $module->name = 'Водят ли се съдебни дела за имота ми';
        $module->save();

        $module = new Module;
        $module->name = 'Проверка за недвижими имоти на лице';
        $module->save();

        $module = new Module;
        $module->name = 'Проверка за участие в дружества';
        $module->save();

        $module = new Module();
        $module->name = 'Промяна на предмета на дейност';
        $module->save();

        $module = new Module;
        $module->name = 'Генерално турговско пълномощно';
        $module->save();

        $module = new Module();
        $module->name = 'Брачен договор';
        $module->save();

        $module = new Module();
        $module->name = 'Пълномощно за продажба на МПС';
        $module->save();

        $module = new Module;
        $module->name = 'Проверка за собствеността на уеб сайт';
        $module->save();

        $module = new Module;
        $module->name = 'Откриване на имот';
        $module->save();

        $module = new Module;
        $module->name = 'Образец от подпис';
        $module->save();

        $module = new Module;
        $module->name = 'Актуално състояние на фирма';
        $module->save();

        $module = new Module();
        $module->name = 'Промяна на адрес на управление';
        $module->save();

        $module = new Module();
        $module->name = 'Смяна, Добавяне или Заличаване на Управител';
        $module->save();

        $module = new Module;
        $module->name = 'Приходи на фирма';
        $module->save();

        $module = new Module();
        $module->name = 'Промяна на наименование на фирма';
        $module->save();

        $module = new Module;
        $module->name = 'Обявяване на годишен финансов отчет';
        $module->save();

        $module = new Module();
        $module->name = 'Договор за продажба на уеб сайт';
        $module->save();

        $module = new Module();
        $module->name = 'Договор за продажба на МПС';
        $module->save();
    }
}
