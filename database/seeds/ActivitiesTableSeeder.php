<?php

use App\Company\Activity;
use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_activity = new Activity();
        $company_activity->name = 'Бъркане на цимент';
        $company_activity->save();
    }
}
