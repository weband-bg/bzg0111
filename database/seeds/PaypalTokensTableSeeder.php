<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PaypalTokensTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paypal_tokens')->insert([
            'token' => 'token',
            'created_at' => Carbon::now()
        ]);
    }
}
