<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(HoverBalloonsTableSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(TownshipsTableSeeder::class);
        $this->call(PopulatedPlacesTableSeeder::class);
        $this->call(PaypalTokensTableSeeder::class);
        $this->call(ActivitiesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(CarTypesTableSeeder::class);
        $this->call(CarMakesTableSeeder::class);
        $this->call(QuestionAnswerCategoriesTableSeeder::class);
    }
}