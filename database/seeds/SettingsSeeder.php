<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Home page
    	DB::table('page_settings')->insert([
    		'name' => 'home',
    		'page_title' => 'Начало',
    		'meta_description' => 'bezgishe',
    		'meta_keywords' => 'bezgishe',
    	]);

        //Registration EOOD
        DB::table('page_settings')->insert([
        	'name' => 'registration-eood',
        	'page_title' => 'Регистрация на ЕООД',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'В случай, че при попълване на формата за регистрация на фирма имате каквито и да е допълнителни въпроси, свържете се с нас на тел. 0887 397 106. Ние ще ви консултаираме напълно безплатно и ще ви навигираме в процеса на попълване, до самото приключване на заявката.'
        ]);

        //Registration OOD
        DB::table('page_settings')->insert([
        	'name' => 'registration-ood',
        	'page_title' => 'Регистрация на ООД',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Registration ET
        DB::table('page_settings')->insert([
        	'name' => 'registration-et',
        	'page_title' => 'Регистрация на ЕТ',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Property weight
        DB::table('page_settings')->insert([
        	'name' => 'property-weight',
        	'page_title' => 'Има ли тежести върху имота ми?',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Company revenue
        DB::table('page_settings')->insert([
        	'name' => 'company-revenue',
        	'page_title' => 'Приходи на фирма',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Website owner
        DB::table('page_settings')->insert([
        	'name' => 'website-owner',
        	'page_title' => 'Проверка за собтсвеноста на уебсайт',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Estate opening
        DB::table('page_settings')->insert([
        	'name' => 'estate-opening',
        	'page_title' => 'Откриване на имот',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Property lawsuits
        DB::table('page_settings')->insert([
        	'name' => 'property-lawsuit',
        	'page_title' => 'Водят ли се съдебни дела за имота ми?',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Annual financial statements declatation
        DB::table('page_settings')->insert([
        	'name' => 'annual-financial-statements-declaration',
        	'page_title' => 'Обявяване на годишен финансов отчет',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Change company activity subject
        DB::table('page_settings')->insert([
        	'name' => 'change-company-activity-subject',
        	'page_title' => 'Промяна на предмета на дейност',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Change company name
        DB::table('page_settings')->insert([
        	'name' => 'change-company-name',
        	'page_title' => 'Промяна на наименование на фирма',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Company fortune
        DB::table('page_settings')->insert([
        	'name' => 'company-fortune',
        	'page_title' => 'Актуално състояние на фирма',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Company paper translation
        DB::table('page_settings')->insert([
        	'name' => 'company-paper-translation',
        	'page_title' => 'Превод на фирмени документи',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Company participations
        DB::table('page_settings')->insert([
        	'name' => 'company-participations',
        	'page_title' => 'Проверка за участие в дружества',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

       	//General commercial proxy
       	DB::table('page_settings')->insert([
       		'name' => 'general-commercial-proxy',
       		'page_title' => 'Генерално търговско пълномощно',
       		'meta_description' => 'bezgishe',
       		'meta_keywords' => 'bezgishe',
       		'text' => 'Текст, който може да бъде променен от админ панела'
       	]);

       	//Manager operation
       	DB::table('page_settings')->insert([
        	'name' => 'manager-operation',
        	'page_title' => 'Смяна, добавяне или заличаване на управител',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Owner estates
        DB::table('page_settings')->insert([
        	'name' => 'owner-estates',
        	'page_title' => 'Проверка за недвижими имоти на лице',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Signature sample
        DB::table('page_settings')->insert([
        	'name' => 'signature-sample',
        	'page_title' => 'Образец от подпис',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Switch management address
        DB::table('page_settings')->insert([
        	'name' => 'switch-management-address',
        	'page_title' => 'Промяна на адреса на управление',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Website sale contract
        DB::table('page_settings')->insert([
        	'name' => 'website-sale-contract',
        	'page_title' => 'Договор за продажба на уебсайт',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Wedding contract
        DB::table('page_settings')->insert([
        	'name' => 'wedding-contract',
        	'page_title' => 'Брачен договор',
        	'meta_description' => 'bezgishe',
        	'meta_keywords' => 'bezgishe',
        	'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Car sale contract
        DB::table('page_settings')->insert([
            'name' => 'car-sale-contract',
            'page_title' => 'Договор за продажба на МПС',
            'meta_description' => 'bezgishe',
            'meta_keywords' => 'bezgishe',
            'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        //Car sale proxy
        DB::table('page_settings')->insert([
            'name' => 'car-sale-proxy',
            'page_title' => 'Пълномощно за продажба на МПС',
            'meta_description' => 'bezgishe',
            'meta_keywords' => 'bezgishe',
            'text' => 'Текст, който може да бъде променен от админ панела'
        ]);

        // About
        DB::table('page_settings')->insert([
            'name' => 'about',
            'page_title' => 'Как Работим',
            'meta_description' => 'bezgishe',
            'meta_keywords' => 'bezgishe',
            'text' => 'Текст, който може да бъде променен от админ панела'
        ]);
    }
}
