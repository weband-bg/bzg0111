<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyWeightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_weights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('firstname')->nullable();
            $table->string('secondname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('pin')->nullable();
            $table->string('name')->nullable();
            $table->string('eik')->nullable();
            $table->integer('zone_id')->unsigned();
            $table->integer('township_id')->unsigned();
            $table->integer('populated_place_id')->nullable();
            $table->string('postcode');
            $table->string('quarter');
            $table->string('street');
            $table->integer('street_number');
            $table->string('building');
            $table->string('entrance');
            $table->string('floor');
            $table->string('apartment');
            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_pin');
            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_weights');
    }
}
