<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoverBalloonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hover_balloons', function (Blueprint $table) {
            $table->increments('id');
            // update
            $table->string('module_id')->nullable();
            $table->string('label')->nullable();
            $table->string('form_field');
            $table->text('hover_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hover_balloons');
    }
}
