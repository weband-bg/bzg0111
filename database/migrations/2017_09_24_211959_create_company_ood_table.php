<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyOodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_ood', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_foreign_name');
            $table->integer('zone_id')->nullable();
            $table->integer('township_id')->nullable();
            $table->integer('populated_place_id')->nullable();
            $table->string('postcode')->nullable();
            $table->string('quarter')->nullable();
            $table->string('street')->nullable();
            $table->string('street_number')->nullable();
            $table->string('building')->nullable();
            $table->string('vhod')->nullable();
            $table->string('floor')->nullable();
            $table->string('apartment')->nullable();
            $table->boolean('different_address')->nullable();
            $table->integer('address_2_zone_id')->nullable();
            $table->integer('address_2_township_id')->nullable();
            $table->integer('address_2_populated_place_id')->nullable();
            $table->string('address_2_postcode')->nullable();
            $table->string('address_2_quarter')->nullable();
            $table->string('address_2_street')->nullable();
            $table->string('address_2_street_number')->nullable();
            $table->string('address_2_building')->nullable();
            $table->string('address_2_vhod')->nullable();
            $table->string('address_2_floor')->nullable();
            $table->string('address_2_apartment')->nullable();
            $table->text('additional_activities')->nullable();
            $table->decimal('money', 11, 2);

            $table->string('representation_method');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_ood');
    }
}
