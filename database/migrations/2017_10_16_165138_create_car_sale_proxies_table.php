<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarSaleProxiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_sale_proxies', function (Blueprint $table) {
            $table->increments('id');

            $table->string('seller_type');

            $table->string('seller_firstname')->nullable();
            $table->string('seller_secondname')->nullable();
            $table->string('seller_lastname')->nullable();
            $table->string('seller_pin')->nullable();
            $table->string('seller_id_number')->nullable();
            $table->string('seller_id_date_created')->nullable();
            $table->string('seller_id_created_by')->nullable();

            $table->string('seller_name')->nullable();
            $table->string('seller_eik')->nullable();
            $table->string('manager_firstname')->nullable();
            $table->string('manager_secondname')->nullable();
            $table->string('manager_lastname')->nullable();
            $table->string('manager_pin')->nullable();
            $table->string('manager_id_number')->nullable();
            $table->string('manager_id_date_created')->nullable();
            $table->string('manager_id_created_by')->nullable();

            $table->string('proxy_firstname');
            $table->string('proxy_secondname');
            $table->string('proxy_lastname');
            $table->string('proxy_pin');
            $table->string('proxy_id_number');
            $table->string('proxy_id_date_created');
            $table->string('proxy_id_created_by');

            $table->string('talon');

            $table->string('car_type_id')->nullable();
            $table->string('car_make_id')->nullable();
            $table->string('car_model')->nullable();
            $table->string('car_chassis')->nullable();
            $table->string('car_registration')->nullable();
            $table->string('car_color')->nullable();
            
            $table->decimal('price', 11, 2);

            $table->boolean('self_contract_clause')->default(false);

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_sale_proxies');
    }
}
