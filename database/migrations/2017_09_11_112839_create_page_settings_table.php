<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('page_title');
            $table->text('meta_description');
            $table->text('meta_keywords');
            $table->text('text')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 11 ,2)->default(0);
            $table->decimal('foreign_price', 11, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_settings');
    }
}
