<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopulatedPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('populated_places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ekatte');
            $table->string('name');
            $table->integer('township_id')->unsigned();

            $table->foreign('township_id')->references('id')->on('townships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('populated_places');
    }
}
