<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralCommercialProxiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('general_commercial_proxies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('eik');

            $table->string('manager_firstname')->nullable();
            $table->string('manager_secondname')->nullable();
            $table->string('manager_lastname')->nullable();
            $table->string('manager_pin')->nullable();
            $table->string('manager_id_number')->nullable();
            $table->string('manager_id_date_created')->nullable();
            $table->string('manager_id_created_by')->nullable();

            $table->string('proxy_firstname')->nullable();
            $table->string('proxy_secondname')->nullable();
            $table->string('proxy_lastname')->nullable();
            $table->string('proxy_pin')->nullable();
            $table->string('proxy_id_number')->nullable();
            $table->string('proxy_id_date_created')->nullable();
            $table->string('proxy_id_created_by')->nullable();

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('general_commercial_proxies');
    }

}
