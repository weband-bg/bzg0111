<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyRevenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_revenues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('eik');
            // $table->string('payment_method')->default('office');
            // $table->string('payer_firstname');
            // $table->string('payer_lastname');
            // $table->string('payer_email');
            // $table->string('payer_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_revenues');
    }
}
