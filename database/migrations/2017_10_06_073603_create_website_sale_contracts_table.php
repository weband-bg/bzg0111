<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteSaleContractsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('website_sale_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('seller_type');
            $table->string('seller_firstname')->nullable();
            $table->string('seller_secondname')->nullable();
            $table->string('seller_lastname')->nullable();
            $table->string('seller_pin')->nullable();
            $table->string('seller_name')->nullable();
            $table->string('seller_eik')->nullable();

            $table->string('buyer_type');
            $table->string('buyer_firstname')->nullable();
            $table->string('buyer_secondname')->nullable();
            $table->string('buyer_lastname')->nullable();
            $table->string('buyer_pin')->nullable();
            $table->string('buyer_name')->nullable();
            $table->string('buyer_eik')->nullable();

            $table->string('domain');
            $table->string('additional_website_info');

            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_email');
            $table->string('owner_id_expiry_date');
            $table->string('owner_phone');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('website_sale_contracts');
    }

}
