<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_settings_id')->unsigned();
            $table->string('question');
            $table->text('answer');
            $table->integer('question_answer_category_id')->unsigned();
            $table->timestamps();

            $table->foreign('page_settings_id')->references('id')->on('page_settings');
            $table->foreign('question_answer_category_id')->references('id')->on('question_answer_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_answers');
    }
}
