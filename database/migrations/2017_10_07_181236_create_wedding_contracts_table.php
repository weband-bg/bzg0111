<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeddingContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wedding_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('husband_firstname');
            $table->string('husband_secondname');
            $table->string('husband_lastname');
            $table->string('husband_pin');
            $table->string('husband_id_number');
            $table->string('husband_id_date_created');
            $table->string('husband_id_created_by');
            
            $table->string('wife_firstname');
            $table->string('wife_secondname');
            $table->string('wife_lastname');
            $table->string('wife_pin');
            $table->string('wife_id_number');
            $table->string('wife_id_date_created');
            $table->string('wife_id_created_by');

            $table->string('additional_clauses')->nullable();
            $table->string('marriage_step');
            $table->string('marriage_future_date')->nullable();
            $table->string('marriage_past_date')->nullable();
            
            $table->boolean('future_spouses_dispose_property_freely')->default(false);
            $table->boolean('future_spouses_no_property_right')->default(false);
            $table->boolean('future_spouses_divisive_duties')->default(false);
            $table->boolean('future_spouses_property_acquired_during_marriage')->default(false);
            $table->boolean('future_spouses_inheritance_property')->default(false);
            $table->boolean('future_spouses_no_right_claim_part_property')->default(false);

            $table->boolean('past_acquired_estate')->default(false);
            $table->boolean('past_acquired_vehicle')->default(false);
            $table->boolean('past_spouses_dispose_property_freely')->default(false);
            $table->boolean('past_spouses_no_right_claim_part_property')->default(false);
            $table->boolean('no_duties')->default(false);
            $table->boolean('own_property')->default(false);
            $table->boolean('no_heritage_claims')->default(false);
            $table->boolean('part_of_ownership_claim')->default(false);

            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_pin');
            $table->string('owner_email');

            $table->string('owner_phone');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wedding_contracts');
    }
}
