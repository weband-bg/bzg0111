<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSwitchManagementAddressesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('switch_management_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('eik');
            $table->integer('zone_id');
            $table->integer('township_id');
            $table->integer('populated_place_id')->nullable();
            $table->string('postcode');
            $table->string('quarter');
            $table->string('street');
            $table->string('street_number');
            $table->string('building');
            $table->string('vhod');
            $table->string('floor');
            $table->string('apartment');
            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_email');
            $table->string('owner_id_expiry_date');
            $table->string('owner_phone');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('switch_management_addresses');
    }

}
