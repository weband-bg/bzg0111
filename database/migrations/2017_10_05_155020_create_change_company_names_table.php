<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeCompanyNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_company_names', function (Blueprint $table) {
            $table->increments('id');
            $table->string('current_company_name');
            $table->string('eik');
            $table->string('new_company_name');
            $table->string('new_company_foreign_name');
            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_email');
            $table->string('owner_id_expiry_date');
            $table->string('owner_phone');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_company_names');
    }
}
