<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner_estates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('name')->nullable();
            $table->string('eik')->nullable();
            $table->string('firstname')->nullable();
            $table->string('secondname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('pin')->nullable();
            // $table->string('payment_method')->default('office');
            // $table->string('payer_firstname');
            // $table->string('payer_lastname');
            // $table->string('payer_email');
            // $table->string('payer_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owner_estates');
    }
}
