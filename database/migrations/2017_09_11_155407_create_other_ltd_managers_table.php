<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherLtdManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_ltd_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_ltd_id')->unsigned();
            $table->string('firstname');
            $table->string('secondname');
            $table->string('lastname');
            $table->string('pin');
            $table->string('id_number');
            $table->string('id_date_created');
            $table->string('id_created_by');
            $table->timestamps();

            $table->foreign('company_ltd_id')->references('id')->on('company_ltd')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_ltd_managers');
    }
}
