<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagerOperationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('manager_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('eik');
            $table->string('operation');

            $table->string('add_manager_firstname')->nullable();
            $table->string('add_manager_secondname')->nullable();
            $table->string('add_manager_lastname')->nullable();
            $table->string('add_manager_pin')->nullable();
            $table->string('add_manager_id_number')->nullable();
            $table->string('add_manager_id_date_created')->nullable();
            $table->string('add_manager_id_created_by')->nullable();

            $table->string('remove_manager_name')->nullable();

            $table->string('switch_manager_name')->nullable();
            $table->string('switch_manager_firstname')->nullable();
            $table->string('switch_manager_secondname')->nullable();
            $table->string('switch_manager_lastname')->nullable();
            $table->string('switch_manager_pin')->nullable();
            $table->string('switch_manager_id_number')->nullable();
            $table->string('switch_manager_id_date_created')->nullable();
            $table->string('switch_manager_id_created_by')->nullable();
            
            $table->string('owner_firstname');
            $table->string('owner_secondname');
            $table->string('owner_lastname');
            $table->string('owner_email');
            $table->string('owner_id_expiry_date');
            $table->string('owner_phone');

            $table->string('payment_method')->default('office');
            $table->string('payer_firstname');
            $table->string('payer_lastname');
            $table->string('payer_email');
            $table->string('payer_phone');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('manager_operations');
    }

}
