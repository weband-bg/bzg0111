@extends('layouts.main')

@section('title', 'Актуално състояние на фирма')
@section('page_title', 'Актуално състояние на фирма')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

        @if(isset($settings))
            <h2>{{ $settings->text }}</h2>
        @endif

        <form action="{{ route('store-company-fortune') }}" method="POST">
            {{csrf_field()}}
            <div class="row">
                <h3>ЕИК и Име на Фирма</h3>
                <div class="col-md-3 col-md-offset-3">
                    <div class="form-group">
                        <label for="eik">ЕИК</label>
                        <input type="text" name="eik" id="eik" class="form-control" required value="{{ old('eik') }}">
                        @if($errors->has('eik'))
                        <span class="help-block">{{ $errors->first('eik') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="company_name">Име на Фирма</label>
                        <input type="text" name="company_name" id="company_name" class="form-control" required value="{{ old('company_name') }}">
                        @if($errors->has('company_name'))
                        <span class="help-block">{{ $errors->first('company_name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-6 col-md-offset-3">
                <div class="text-center">
                    <button type="submit" id="submit" class="btn btn-success"> <span class="fa fa-legal"> </span> Изпрати заявка </button>
                </div>
            </div> --}}

            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="text-center">Начин на плащане</h3>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="office" value="office" checked>
                            <span>На място в офиса</span>
                        </label>
                    </div>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="econt" value="econt">
                            <span>Еконт</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <h3 class="text-center">Вашата информация</h3>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_firstname">Име</label>
                        <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                        @if($errors->has('payer_firstname'))
                            <span>{{ $errors->first('payer_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_lastname">Фамилия</label>
                        <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                        @if($errors->has('payer_lastname'))
                            <span>{{ $errors->first('payer_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_email">Email</label>
                        <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                        @if($errors->has('payer_email'))
                            <span>{{ $errors->first('payer_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_phone">Телефон</label>
                        <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                        @if($errors->has('payer_phone'))
                            <span>{{ $errors->first('payer_phone') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            {{-- Submit --}}
            <div class="gap-10"></div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
                </div>
            </div>

            {{-- @if($settings->price > 0)
                <div style="display: none;" id="payments">
                    <div class="dt-sc-hr-invisible-medium"></div>
                    <div class="column dt-sc-one-column payments text-center">
                        <h2 class="text-center">
                            Начин на плащане
                            <br>
                            ({{ $settings->price }}) EUR
                        </h2>

                        <div class="column dt-sc-one-fourth first">
                            <div id="paypal-button"></div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="epay">Epay</label>
                                <input type="radio" name="payment" value="epay" id="epay">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="econt">Еконт</label>
                                <input type="radio" name="payment" value="econt" id="econt">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="office">На място в офиса</label>
                                <input type="radio" name="payment" value="office" id="office">
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}
		</form>

        @include('reviews.review-form')

@endsection

@section('scripts')
<script type="text/javascript">
	(function($){
		$('input[name=type]').click(function(event) {
			if ($(this).val() == 'individual') {
				$('#legal-entity-form').hide('150')
				$('#individual-form').show('300')
			}
			else {
				$('#individual-form').hide('150')
				$('#legal-entity-form').show('300')
			}
		})
	})(jQuery)
</script>

{{-- Paypal script --}}
@if($settings->price > 0)
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script type="text/javascript">
    (function ($) {
        $('#submit').click(function(event) {
            event.preventDefault()

            $('#payments').fadeIn('slow')

            var CREATE_PAYMENT_URL  = '{{ route('company-fortune-paypal-create') }}'

            var EXECUTE_PAYMENT_URL = '{{ route('company-fortune-paypal-execute') }}'

            paypal.Button.render({

                env: 'sandbox', // Or 'sandbox'

                commit: true, // Show a 'Pay Now' button

                payment: function() {
                    return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function(data) {
                        // console.log(data)
                        return data.id
                    })
                },

                // onAuthorize() is called when the buyer approves the payment
                onAuthorize: function(data, actions) {
                    console.log(data)
                    // Set up the data you need to pass to your server
                    var data = {
                        paymentID: data.paymentID,
                        payerID: data.payerID,
                        _token: '{{ csrf_token() }}'
                    };

                    let form_data = $('form').serialize()

                    //Make a POST request with the form data
                    $.ajax({
                        url: '{{ route('store-company-fortune') }}',
                        type: 'POST',
                        data: form_data,
                        success: function(response) {
                            // Make a call to your server to execute the payment
                            return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                .then(function (res) {
                                    res = JSON.parse(res)
                                    console.log(res)
                                    
                                    //clear form and show success message
                                    $('form input').each(function(index, el) {
                                        $(this).val('')
                                    })

                                    alert('Заявката ви беше успешно изпратена')
                                })
                        },
                        error: function(data) {
                            let errors = data.responseJSON.errors

                            for(error in errors) {
                                $('input[name='+error+']').after('<span class="help-block">'+errors[error][0]+'</span>')
                            }
                        }
                    })
                }

            }, '#paypal-button')

            console.log('paypal')  
        })
    })(jQuery)
</script>
@endif
@endsection
