<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin/custom.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login</title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>{{ config('app.name') }}</h1>
      </div>
      <div class="login-box">
        <form class="login-form" action="{{ route('register') }}" method="POST">
          {{csrf_field()}}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Регистрирай се</h3>
          <div class="form-group">
              <label for="username" class="control-label">ПОТРЕБИТЕЛСКО ИМЕ</label>
              <input type="text" class="form-control" name="name" id="username" placeholder="username" value="{{ old('name') }}">
              @if($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
          </div>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="email" name="email" value="{{ old('email') }}" placeholder="{{ strtolower(config('app.name')) }}@example.com">
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
            <label class="control-label">ПАРОЛА</label>
            <input class="form-control" type="password" placeholder="парола" name="password">
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
          </div>
          <div class="form-group">
              <label for="password-confirmation">ПОТВЪРДИ ПАРОЛА</label>
              <input type="password" class="form-control" id="password-confirmation" name="password_confirmation">
              @if($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
              @endif
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>РЕГИСТРИРАЙ СЕ</button>
          </div>
          {{-- <div class="form-group btn-container">
            <a href="{{ route('facebook-login') }}" class="btn btn-primary btn-block fb-login"><i class="fa fa-facebook fa-lg fa-fw"></i>ВПИШИ СЕ С FACEBOOK</a>
          </div> --}}
        </form>
      </div>
    </section>
  </body>
  <script src="{{ asset('js/admin/jquery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/admin/plugins/pace.min.js') }}"></script>
  <script src="{{ asset('js/admin/main.js') }}"></script>
</html>