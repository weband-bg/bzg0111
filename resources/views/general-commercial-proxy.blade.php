@extends('layouts.main')

@section('title', 'Генерално турговско пълномощно')
@section('page_title', 'Генерално турговско пълномощно')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
    <h3>{{ $settings->text }}</h3>
</div>

<form action="{{ route('store-general-commercial-proxy') }}" method="POST">
    {{ csrf_field() }}

    <div class="row">
        <h4 class="text-center">Данни на упълномощителя</h4>

        <div class="col-md-6">
            <div class="form-group">
                <label for="company_name">Име на фирма</label>
                <input type="text" class="form-control" id="company_name" name="company_name" value="{{ old('company_name') }}">
                @if($errors->has('company_name'))
                    <span>{{ $errors->first('company_name') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="eik">ЕИК</label>
                <input type="text" class="form-control" id="eik" name="eik" value="{{ old('eik') }}">
                @if($errors->has('eik'))
                    <span>{{ $errors->first('eik') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <h4 class="text-center">Лични данни на представляващия фирмата</h4>

        <div class="col-md-3">
            <div class="form-group">
                <label for="manager_firstname">Име</label>
                <input type="text" class="form-control" id="manager_firstname" name="manager_firstname" value="{{ old('manager_firstname') }}">
                @if($errors->has('manager_firstname'))
                    <span>{{ $errors->first('manager_firstname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="manager_secondname">Презиме</label>
                <input type="text" class="form-control" id="manager_secondname" name="manager_secondname" value="{{ old('manager_secondname') }}">
                @if($errors->has('manager_secondname'))
                    <span>{{ $errors->first('manager_secondname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="manager_lastname">Фамилия</label>
                <input type="text" class="form-control" id="manager_lastname" name="manager_lastname" value="{{ old('manager_lastname') }}">
                @if($errors->has('manager_lastname'))
                    <span>{{ $errors->first('manager_lastname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="manager_pin">ЕГН</label>
                <input type="text" class="form-control" id="manager_pin" name="manager_pin" value="{{ old('manager_pin') }}">
                @if($errors->has('manager_pin'))
                    <span>{{ $errors->first('manager_pin') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="manager_id_number">Номер на лична карта</label>
                <input type="text" class="form-control" id="manager_id_number" name="manager_id_number" value="{{ old('manager_id_number') }}">
                @if($errors->has('manager_id_number'))
                    <span>{{ $errors->first('manager_id_number') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="manager_id_date_created">Дата на издаване</label>
                <input type="date" class="form-control" id="manager_id_date_created" name="manager_id_date_created" value="{{ old('manager_id_date_created') }}">
                @if($errors->has('manager_id_date_created'))
                    <span>{{ $errors->first('manager_id_date_created') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="manager_id_created_by">Издадена от</label>
                <input type="text" class="form-control" id="manager_id_created_by" name="manager_id_created_by" value="{{ old('manager_id_created_by') }}">
                @if($errors->has('manager_id_created_by'))
                    <span>{{ $errors->first('manager_id_created_by') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <h4 class="text-center">Лични данни на пълномощника</h4>

        <div class="col-md-3">
            <div class="form-group">
                <label for="proxy_firstname">Име</label>
                <input type="text" class="form-control" id="proxy_firstname" name="proxy_firstname" value="{{ old('proxy_firstname') }}">
                @if($errors->has('proxy_firstname'))
                    <span>{{ $errors->first('proxy_firstname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="proxy_secondname">Презиме</label>
                <input type="text" class="form-control" id="proxy_secondname" name="proxy_secondname" value="{{ old('proxy_secondname') }}">
                @if($errors->has('proxy_secondname'))
                    <span>{{ $errors->first('proxy_secondname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="proxy_lastname">Фамилия</label>
                <input type="text" class="form-control" id="proxy_lastname" name="proxy_lastname" value="{{ old('proxy_lastname') }}">
                @if($errors->has('proxy_lastname'))
                    <span>{{ $errors->first('proxy_lastname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="proxy_pin">ЕГН</label>
                <input type="text" class="form-control" id="proxy_pin" name="proxy_pin" value="{{ old('proxy_pin') }}">
                @if($errors->has('proxy_pin'))
                    <span>{{ $errors->first('proxy_pin') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="proxy_id_number">Номер на лична карта</label>
                <input type="text" class="form-control" id="proxy_id_number" name="proxy_id_number" value="{{ old('proxy_id_number') }}">
                @if($errors->has('proxy_id_number'))
                    <span>{{ $errors->first('proxy_id_number') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="proxy_id_date_created">Дата на издаване</label>
                <input type="date" class="form-control" id="proxy_id_date_created" name="proxy_id_date_created" value="{{ old('proxy_id_date_created') }}">
                @if($errors->has('proxy_id_date_created'))
                    <span>{{ $errors->first('proxy_id_date_created') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="proxy_id_created_by">Издадена от</label>
                <input type="text" class="form-control" id="proxy_id_created_by" name="proxy_id_created_by" value="{{ old('proxy_id_created_by') }}">
                @if($errors->has('proxy_id_created_by'))
                    <span>{{ $errors->first('proxy_id_created_by') }}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h3 class="text-center">Начин на плащане</h3>
            <div class="radio radio__custom radio__style1">
                <label>
                    <input type="radio" name="payment_method" id="office" value="office" checked>
                    <span>На място в офиса</span>
                </label>
            </div>
            <div class="radio radio__custom radio__style1">
                <label>
                    <input type="radio" name="payment_method" id="econt" value="econt">
                    <span>Еконт</span>
                </label>
            </div>
        </div>
    </div>

    <div class="row">
        <h3 class="text-center">Вашата информация</h3>
        <div class="col-md-3">
            <div class="form-group">
                <label for="payer_firstname">Име</label>
                <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                @if($errors->has('payer_firstname'))
                    <span>{{ $errors->first('payer_firstname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="payer_lastname">Фамилия</label>
                <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                @if($errors->has('payer_lastname'))
                    <span>{{ $errors->first('payer_lastname') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="payer_email">Email</label>
                <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                @if($errors->has('payer_email'))
                    <span>{{ $errors->first('payer_email') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="payer_phone">Телефон</label>
                <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                @if($errors->has('payer_phone'))
                    <span>{{ $errors->first('payer_phone') }}</span>
                @endif
            </div>
        </div>
    </div>

    {{-- Submit --}}
    <div class="gap-10"></div>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 text-center">
            <button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
        </div>
    </div>
</form>

@include('reviews.review-form')

@endsection