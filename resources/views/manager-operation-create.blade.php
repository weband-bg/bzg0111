@extends('layouts.main')

@section('title', 'Смяна, Добавяне или Заличаване на Управител')
@section('page_title', 'Смяна, Добавяне или Заличаване на Управител')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

    @if(isset($settings))
        <h2>{{ $settings->text }}</h2>
    @endif

        <form action="{{ route('store-manager-operation') }}" method="POST">
            {{csrf_field()}}
            <h3>ЕИК и Име на Фирма</h3>
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label for="company_name">Име на Фирма</label>
                    <input type="text" name="company_name" id="company_name" class="form-control" value="{{ old('company_name') }}" required>
                    @if($errors->has('company_name'))
                    <span class="help-block">{{ $errors->first('company_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="eik">ЕИК на фирма</label>
                    <input type="text" name="eik" id="eik" class="form-control" value="{{ old('eik') }}" required>
                    @if($errors->has('eik'))
                    <span class="help-block">{{ $errors->first('eik') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4 col-md-offset-4">
                <div class="form-group">
                    <label for="operation">Операция</label>
                    <select name="operation" id="operation" class="form-control">
                        <option value="">-- Операция --</option>
                        <option value="1">Добавяне на още един управител</option>
                        <option value="2">Заличаване на управител</option>
                        <option value="3">Замяна на стар управител с нов</option>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="display: none" id="add-manager-operation">
                <div class="col-md-4">
                    <div class="form-group">
                            <label for="add_manager_firstname">Име</label>
                            <input type="text" id="add_manager_firstname" class="form-control" name="add_manager_firstname" value="{{ old('add_manager_firstname') }}">
                            @if($errors->has('manager.add_manager_firstname'))
                                <span class="help-block">{{ $errors->first('add_manager_firstname') }}</span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="add_manager_secondname">Презиме</label>
                        <input type="text" id="add_manager_secondname" class="form-control" name="add_manager_secondname" value="{{ old('add_manager_secondname') }}">
                        @if($errors->has('manager.add_manager_secondname'))
                            <span class="help-block">{{ $errors->first('add_manager_secondname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="add_manager_lastname">Фамилия</label>
                        <input type="text" id="add_manager_lastname" class="form-control" name="add_manager_lastname" value="{{ old('add_manager_lastname') }}">
                        @if($errors->has('manager.add_manager_lastname'))
                            <span class="help-block">{{ $errors->first('add_manager_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="add_manager_pin">ЕГН</label>
                        <input type="text" id="add_manager_pin" class="form-control" name="add_manager_pin" value="{{ old('add_manager_pin') }}">
                        @if($errors->has('manager.add_manager_pin'))
                            <span class="help-block">{{ $errors->first('add_manager_pin') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="add_manager_id_number">Номер на лична карта</label>
                        <input type="text" id="add_manager_id_number" class="form-control" name="add_manager_id_number" value="{{ old('add_manager_id_number') }}">
                        @if($errors->has('manager.add_manager_id_number'))
                            <span class="help-block">{{ $errors->first('add_manager_id_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="add_manager_id_date_created">Дата на издаване</label>
                        <input type="date" id="add_manager_id_date_created" class="form-control" name="add_manager_id_date_created" value="{{ old('add_manager_id_date_created') }}">
                        @if($errors->has('manager.add_manager_id_date_created'))
                            <span class="help-block">{{ $errors->first('add_manager_id_date_created') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="add_manager_id_created_by">Издадена от</label>
                        <input type="text" id="add_manager_id_created_by" class="form-control" name="add_manager_id_created_by" value="{{ old('add_manager_id_created_by') }}">
                        @if($errors->has('manager.add_manager_id_created_by'))
                            <span class="help-block">{{ $errors->first('add_manager_id_created_by') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="display: none" id="remove-manager-operation">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="switch_remove_manager_name">Име на стар управител</label>
                        <input type="text" id="switch_remove_manager_name" class="form-control" name="switch_remove_manager_name" value="{{ old('switch_remove_manager_name') }}">
                        @if($errors->has('switch_remove_manager_name'))
                            <span class="help-block">{{ $errors->first('switch_remove_manager_name') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div style="display: none;" id="switch-manager-operation">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="switch_manager_name">Име на стар управител</label>
                        <input type="text" id="switch_manager_name" class="form-control" name="switch_manager_name" value="{{ old('switch_manager_name') }}">
                        @if($errors->has('switch_manager_name'))
                            <span class="help-block">{{ $errors->first('switch_manager_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                            <label for="switch_manager_firstname">Име</label>
                            <input type="text" id="switch_manager_firstname" class="form-control" name="switch_manager_firstname" value="{{ old('switch_manager_firstname') }}">
                            @if($errors->has('manager.switch_manager_firstname'))
                                <span class="help-block">{{ $errors->first('switch_manager_firstname') }}</span>
                            @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="switch_manager_secondname">Презиме</label>
                        <input type="text" id="switch_manager_secondname" class="form-control" name="switch_manager_secondname" value="{{ old('switch_manager_secondname') }}">
                        @if($errors->has('manager.switch_manager_secondname'))
                            <span class="help-block">{{ $errors->first('switch_manager_secondname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="switch_manager_lastname">Фамилия</label>
                        <input type="text" id="switch_manager_lastname" class="form-control" name="switch_manager_lastname" value="{{ old('switch_manager_lastname') }}">
                        @if($errors->has('manager.switch_manager_lastname'))
                            <span class="help-block">{{ $errors->first('switch_manager_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="switch_manager_pin">ЕГН</label>
                        <input type="text" id="switch_manager_pin" class="form-control" name="switch_manager_pin" value="{{ old('switch_manager_pin') }}">
                        @if($errors->has('manager.switch_manager_pin'))
                            <span class="help-block">{{ $errors->first('switch_manager_pin') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="switch_manager_id_number">Номер на лична карта</label>
                        <input type="text" id="switch_manager_id_number" class="form-control" name="switch_manager_id_number" value="{{ old('switch_manager_id_number') }}">
                        @if($errors->has('manager.switch_manager_id_number'))
                            <span class="help-block">{{ $errors->first('switch_manager_id_number') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="switch_manager_id_date_created">Дата на издаване</label>
                        <input type="date" id="switch_manager_id_date_created" class="form-control" name="switch_manager_id_date_created" value="{{ old('switch_manager_id_date_created') }}">
                        @if($errors->has('manager.switch_manager_id_date_created'))
                            <span class="help-block">{{ $errors->first('switch_manager_id_date_created') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="switch_manager_id_created_by">Издадена от</label>
                        <input type="text" id="switch_manager_id_created_by" class="form-control" name="switch_manager_id_created_by" value="{{ old('switch_manager_id_created_by') }}">
                        @if($errors->has('manager.switch_manager_id_created_by'))
                            <span class="help-block">{{ $errors->first('switch_manager_id_created_by') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-3">
                <div class="text-center">
                    <button type="button" class="btn btn-success" id="additional-info-button"><span class="fa fa-legal"> </span> Потвърди заявка</button>
                    <p>80 лв. хонорар (не включва държавна и нотариална такса)</p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="column dt-sc-one-column additional-info" style="display: none">
                <h2>Данни на заявител</h2>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_firstname">Име</label>
                        <input type="text" class="form-control" id="owner_firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
                        @if($errors->has('owner_firstname'))
                            <span class="help-block">{{ $errors->first('owner_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_secondname">Презиме</label>
                        <input type="text" class="form-control" id="owner_secondname" name="owner_secondname" value="{{ old('owner_secondname') }}">
                        @if($errors->has('owner_secondname'))
                            <span class="help-block">{{ $errors->first('owner_secondname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_lastname">Фамилия</label>
                        <input type="text" class="form-control" id="owner_lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
                        @if($errors->has('owner_lastname'))
                            <span class="help-block">{{ $errors->first('owner_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_email">И-мейл</label>
                        <input type="text" class="form-control" id="owner_email" name="owner_email" value="{{ old('owner_email') }}">
                        @if($errors->has('owner_email'))
                            <span class="help-block">{{ $errors->first('owner_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_id_expiry_date">Дата на валидност на лична карта</label>
                        <input type="date" class="form-control" id="owner_id_expiry_date" name="owner_id_expiry_date" value="{{ old('owner_id_expiry_date') }}">
                        @if($errors->has('owner_id_expiry_date'))
                            <span class="help-block">{{ $errors->first('owner_id_expiry_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_phone">Телефон</label>
                        <input type="text" class="form-control" id="owner_phone" name="owner_phone" value="{{ old('owner_phone') }}">
                        @if($errors->has('owner_phone'))
                            <span class="help-block">{{ $errors->first('owner_phone') }}</span>
                        @endif
                    </div>
                </div>
                
            </div>

            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="text-center">Начин на плащане</h3>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="office" value="office" checked>
                            <span>На място в офиса</span>
                        </label>
                    </div>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="econt" value="econt">
                            <span>Еконт</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <h3 class="text-center">Вашата информация</h3>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_firstname">Име</label>
                        <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                        @if($errors->has('payer_firstname'))
                            <span>{{ $errors->first('payer_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_lastname">Фамилия</label>
                        <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                        @if($errors->has('payer_lastname'))
                            <span>{{ $errors->first('payer_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_email">Email</label>
                        <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                        @if($errors->has('payer_email'))
                            <span>{{ $errors->first('payer_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_phone">Телефон</label>
                        <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                        @if($errors->has('payer_phone'))
                            <span>{{ $errors->first('payer_phone') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-md-offset-3 additional-info" style="display: none">
                <div class="text-center">
                    <button class="btn btn-success" type="submit" id="submit"><span class="fa fa-legal"> </span> Завърши заявка</button>
                </div>
            </div>
            @foreach($errors->all() as $error)
                <span class="help-block">{{ $error }}</span>
            @endforeach

            {{-- @if($settings->price > 0)
                <div style="display: none;" id="payments">
                    <div class="dt-sc-hr-invisible-medium"></div>
                    <div class="column dt-sc-one-column payments text-center">
                        <h2 class="text-center">
                            Начин на плащане
                            <br>
                            ({{ $settings->price }}) EUR
                        </h2>

                        <div class="column dt-sc-one-fourth first">
                            <div id="paypal-button"></div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="epay">Epay</label>
                                <input type="radio" name="payment" value="epay" id="epay">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="econt">Еконт</label>
                                <input type="radio" name="payment" value="econt" id="econt">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="office">На място в офиса</label>
                                <input type="radio" name="payment" value="office" id="office">
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}
        </form>

        @include('reviews.review-form')

@endsection

@section('scripts')
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            var additionalInfoButton = $('#additional-info-button');
            var additionalInfoDiv = $('.additional-info');
            var operation = $('#operation');

            var addManagerOperation = $('#add-manager-operation');
            var removeManagerOperation = $('#remove-manager-operation');
            var switchManagerOperation = $('#switch-manager-operation');

            additionalInfoButton.click(function (event) {
                event.preventDefault();

                additionalInfoDiv.fadeIn('slow');

            });

            operation.on('change', function (event) {
                const selectedOperation = event.target.value;

                addManagerOperation.hide();
                removeManagerOperation.hide();
                switchManagerOperation.hide();

                switch (selectedOperation) {
                    case '1':
                        addManagerOperation.fadeIn('slow');
                        break;
                    case '2':
                        removeManagerOperation.fadeIn('slow');
                        break;
                    case '3':
                        switchManagerOperation.fadeIn('slow');
                        break;
                    default:
                        break;
                }
            });

//    submitButton.click(function (event) {
//        event.preventDefault();
//
//    });


            $('[data-toggle="tooltip"]').tooltip();
        })(jQuery)
    </script>

    {{-- Paypal script --}}
    @if($settings->price > 0)
        <script src="https://www.paypalobjects.com/api/checkout.js"></script>
        <script type="text/javascript">
            (function ($) {
                $('#submit').click(function (event) {
                    event.preventDefault()

                    $('#payments').fadeIn('slow')

                    var CREATE_PAYMENT_URL = '{{ route('manager-operation-paypal-create') }}'

                    var EXECUTE_PAYMENT_URL = '{{ route('manager-operation-paypal-execute') }}'

                    paypal.Button.render({

                        env: 'sandbox', // Or 'sandbox'

                        commit: true, // Show a 'Pay Now' button

                        payment: function () {
                            return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function (data) {
                                // console.log(data)
                                return data.id
                            })
                        },

                        // onAuthorize() is called when the buyer approves the payment
                        onAuthorize: function (data, actions) {
                            console.log(data)
                            // Set up the data you need to pass to your server
                            var data = {
                                paymentID: data.paymentID,
                                payerID: data.payerID,
                                _token: '{{ csrf_token() }}'
                            };

                            let form_data = $('form').serialize()

                            //Make a POST request with the form data
                            $.ajax({
                                url: '{{ route('store-manager-operation') }}',
                                type: 'POST',
                                data: form_data,
                                success: function (response) {
                                    // Make a call to your server to execute the payment
                                    return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                        .then(function (res) {
                                            res = JSON.parse(res)
                                            console.log(res)

                                            //clear form and show success message
                                            $('form input').each(function (index, el) {
                                                $(this).val('')
                                            })

                                            alert('Заявката ви беше успешно изпратена')
                                        })
                                },
                                error: function (data) {
                                    let errors = data.responseJSON.errors

                                    for (error in errors) {
                                        $('input[name=' + error + ']').after('<span class="help-block">' + errors[error][0] + '</span>')
                                    }
                                }
                            })
                        }

                    }, '#paypal-button')

                    console.log('paypal')
                })
            })(jQuery)
        </script>
    @endif
@endsection

