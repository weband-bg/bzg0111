@extends('layouts.main')

@section('title', 'Договор за продажба на уеб сайт')
@section('page_title', 'Договор за продажба на уеб сайт')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

    @if(isset($settings))
        <h2>{{ $settings->text }}</h2>
    @endif
        
    <form action="{{ route('store-website-sale-contract') }}" method="POST">
    {{csrf_field()}}
    
    <div class="col-md-12 text-center">
        <h2>Продавач</h2>
        <div class="col-md-6">
            <label for="seller-legal-entity">Юридическо лице</label>
            <input type="radio" name="seller_type" id="seller-legal-entity" value="seller-legal-entity" {{ old('seller_type') == 'seller-legal-entity' ? 'checked' : '' }}>
        </div>
        <div class="col-md-6">
            <label for="seller-individual">Физическо лице</label>
            <input type="radio" name="seller_type" id="seller-individual" value="seller-individual" {{ old('seller_type') == 'seller-individual' ? 'checked' : '' }}>
        </div>
        @if($errors->has('seller_type'))
            <span class="help-block">{{ $errors->first('seller_type') }}</span>
        @endif

        <div class="col-md-12" id="seller-individual-form" {!! old('type') != 'seller-individual' ? 'style="display: none;"' : '' !!}>
            <h2 class="text-center">Физическо лице</h2>
            <div class="col-md-12">
                <div class="col-md-3">
                    <label for="seller_firstname">Име</label>
                    <input type="text" name="seller_firstname" id="seller_firstname" class="form-control" value="{{ old('seller_firstname') }}">
                    @if($errors->has('seller_firstname'))
                        <span class="help-block">{{ $errors->first('seller_firstname') }}</span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="seller_secondname">Презиме</label>
                    <input type="text" name="seller_secondname" id="seller_secondname" class="form-control" value="{{ old('seller_secondname') }}">
                    @if($errors->has('seller_secondname'))
                        <span class="help-block">{{ $errors->first('seller_secondname') }}</span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="seller_lastname">Фамилия</label>
                    <input type="text" name="seller_lastname" id="seller_lastname" class="form-control" value="{{ old('seller_lastname') }}">
                    @if($errors->has('seller_lastname'))
                        <span class="help-block">{{ $errors->first('seller_lastname') }}</span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="seller_pin">ЕГН</label>
                    <input type="text" name="seller_pin" id="seller_pin" class="form-control" value="{{ old('seller_pin') }}">
                    @if($errors->has('seller_pin'))
                        <span class="help-block">{{ $errors->first('seller_pin') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" id="seller-legal-entity-form" {!! old('seller_type') != 'seller-legal-entity' ? 'style="display: none;"' : '' !!}>
        <h2 class="text-center">Юридическо лице</h2>
        <div class="col-md-12">
            <div class="col-md-6">
                <label for="name">Име</label>
                <input type="text" name="seller_name" id="seller_name" class="form-control" value="{{ old('seller_name') }}">
                @if($errors->has('seller_name'))
                    <span class="help-block">{{ $errors->first('seller_name') }}</span>
                @endif
            </div>
            <div class="col-md-6">
                <label for="seller_eik">ЕИК</label>
                <input type="text" name="seller_eik" id="seller_eik" class="form-control" value="{{ old('seller_eik') }}">
                @if($errors->has('seller_eik'))
                    <span class="help-block">{{ $errors->first('seller_eik') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <h2>Купувач</h2>
        <div class="col-md-6">
            <label for="buyer-legal-entity">Юридическо лице</label>
            <input type="radio" name="buyer_type" id="buyer-legal-entity" value="buyer-legal-entity" {{ old('buyer_type') == 'buyer-legal-entity' ? 'checked' : '' }}>
        </div>
        <div class="col-md-6">
            <label for="buyer-individual">Физическо лице</label>
            <input type="radio" name="buyer_type" id="buyer-individual" value="buyer-individual" {{ old('buyer_type') == 'buyer-individual' ? 'checked' : '' }}>
        </div>
        @if($errors->has('type'))
            <span class="help-block">{{ $errors->first('type') }}</span>
        @endif
    </div>
    <div class="col-md-12" id="buyer-individual-form" {!! old('buyer_type') != 'buyer-individual' ? 'style="display: none;"' : '' !!}>
        <h2 class="text-center">Физическо лице</h2>
        <div class="col-md-12">
            <div class="col-md-3">
                <label for="firstname">Име</label>
                <input type="text" name="buyer_firstname" id="buyer_firstname" class="form-control" value="{{ old('buyer_firstname') }}">
                @if($errors->has('buyer_firstname'))
                    <span class="help-block">{{ $errors->first('buyer_firstname') }}</span>
                @endif
            </div>
            <div class="col-md-3">
                <label for="buyer_secondname">Презиме</label>
                <input type="text" name="buyer_secondname" id="buyer_secondname" class="form-control" value="{{ old('buyer_secondname') }}">
                @if($errors->has('buyer_secondname'))
                    <span class="help-block">{{ $errors->first('buyer_secondname') }}</span>
                @endif
            </div>
            <div class="col-md-3">
                <label for="lastname">Фамилия</label>
                <input type="text" name="buyer_lastname" id="buyer_lastname" class="form-control" value="{{ old('buyer_lastname') }}">
                @if($errors->has('buyer_lastname'))
                    <span class="help-block">{{ $errors->first('buyer_lastname') }}</span>
                @endif
            </div>
            <div class="col-md-3">
                <label for="pin">ЕГН</label>
                <input type="text" name="buyer_pin" id="buyer_pin" class="form-control" value="{{ old('buyer_pin') }}">
                @if($errors->has('buyer_pin'))
                    <span class="help-block">{{ $errors->first('buyer_pin') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12" id="buyer-legal-entity-form" {!! old('buyer_type') != 'buyer-legal-entity' ? 'style="display: none;"' : '' !!}>
        <h2 class="text-center">Юридическо лице</h2>
        <div class="dt-sc-one-column">
            <div class="col-md-6">
                <label for="name">Име</label>
                <input type="text" name="buyer_name" id="buyer_name" class="form-control" value="{{ old('buyer_name') }}">
                @if($errors->has('buyer_name'))
                    <span class="help-block">{{ $errors->first('buyer_name') }}</span>
                @endif
            </div>
            <div class="col-md-6">
                <label for="eik">ЕИК</label>
                <input type="text" name="buyer_eik" id="buyer_eik" class="form-control" value="{{ old('buyer_eik') }}">
                @if($errors->has('buyer_eik'))
                    <span class="help-block">{{ $errors->first('buyer_eik') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h2 class="text-center">Домейн</h2>
        <div class="col-md-4 col-md-offset-4 text-center">
            <label for="name">Домейн</label>
            <input type="text" name="domain" id="domain" class="form-control" value="{{ old('domain') }}">
            @if($errors->has('domain'))
                <span class="help-block">{{ $errors->first('domain') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <h2 class="text-center">Допълнителна информация за сайта</h2>
        <div class="form-group">
            <label for="name">Допълнителна информация за сайта</label>
            <input type="text" name="additional_website_info" id="additional_website_info" class="form-control" value="{{ old('additional_website_info') }}">
            @if($errors->has('additional_website_info'))
                <span class="help-block">{{ $errors->first('additional_website_info') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-12 text-center">
        <button class="btn btn-success" type="submit" id="additional-info-button"><span class="fa fa-legal"> </span> Потвърди заявка
        </button>
        <p>75 лв. с вкл. държавна, банкова такса и хонорар.</p>
    </div>

    <div class="additional-info" style="display: none">
        <div class="col-md-12 additional-info" style="display: none">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner-firstname">Име</label>
                    <input type="text" class="form-control" id="owner-firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
                    @if($errors->has('owner_firstname'))
                        <span class="help-block">{{ $errors->first('owner_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner-second-name">Презиме</label>
                    <input type="text" class="form-control" id="owner-second-name" name="owner_secondname" value="{{ old('owner_secondname') }}">
                    @if($errors->has('owner_secondname'))
                        <span class="help-block">{{ $errors->first('owner_secondname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner-lastname">Фамилия</label>
                    <input type="text" class="form-control" id="owner-lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
                    @if($errors->has('owner_lastname'))
                        <span class="help-block">{{ $errors->first('owner_lastname') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-12 additional-info" style="display: none">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Е-мейл</label>
                    <input type="text" class="owner_email form-control" name="owner_email" value="{{ old('owner_email') }}">
                    @if($errors->has('owner_email'))
                        <span class="help-block">{{ $errors->first('owner_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_id_expiry_date">Дата на валидност на лична карта</label>
                    <input type="date" class="owner_id_expiry_date form-control" name="owner_id_expiry_date" value="{{ old('owner_id_expiry_date') }}">
                    @if($errors->has('owner_id_expiry_date'))
                        <span class="help-block">{{ $errors->first('owner_id_expiry_date') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input type="text" class="owner_phone form-control" name="owner_phone" value="{{ old('owner_phone') }}">
                    @if($errors->has('owner_phone'))
                        <span class="help-block">{{ $errors->first('owner_phone') }}</span>
                    @endif
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="text-center">Начин на плащане</h3>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="office" value="office" checked>
                        <span>На място в офиса</span>
                    </label>
                </div>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="econt" value="econt">
                        <span>Еконт</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <h3 class="text-center">Вашата информация</h3>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_firstname">Име</label>
                    <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                    @if($errors->has('payer_firstname'))
                        <span>{{ $errors->first('payer_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                    @if($errors->has('payer_lastname'))
                        <span>{{ $errors->first('payer_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_email">Email</label>
                    <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                    @if($errors->has('payer_email'))
                        <span>{{ $errors->first('payer_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_phone">Телефон</label>
                    <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                    @if($errors->has('payer_phone'))
                        <span>{{ $errors->first('payer_phone') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="dt-sc-hr-invisible-medium"></div>
        <div class="col-md-12 text-center additional-info" style="display: none">
            <button class="btn btn-success" type="submit" id="submit"><span class="fa fa-legal"> </span> Завърши заявка
            </button>
        </div>
    </div>

    {{-- @if($settings->price > 0)
    <div style="display: none;" id="payments">
        <div class="dt-sc-hr-invisible-medium"></div>
            <div class="column dt-sc-one-column payments text-center">
                <h2 class="text-center">
                    Начин на плащане
                    <br>
                    ({{ $settings->price }}) EUR
                </h2>

                <div class="column dt-sc-one-fourth first">
                    <div id="paypal-button"></div>
                </div>

                <div class="column dt-sc-one-fourth">
                    <div class="form-group">
                        <label for="epay">Epay</label>
                        <input type="radio" name="payment" value="epay" id="epay">
                    </div>
                </div>

                <div class="column dt-sc-one-fourth">
                    <div class="form-group">
                        <label for="econt">Еконт</label>
                        <input type="radio" name="payment" value="econt" id="econt">
                    </div>
                </div>

                <div class="column dt-sc-one-fourth">
                    <div class="form-group">
                        <label for="office">На място в офиса</label>
                        <input type="radio" name="payment" value="office" id="office">
                    </div>
                </div>
            </div>
        </div>
        @endif --}}
    </form>
    
    @include('reviews.review-form')

@endsection

@section('scripts')
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            var additionalInfoButton = $('#additional-info-button');
            var additionalInfoDiv = $('.additional-info');

            $('input[name=seller_type]').click(function (event) {
                if ($(this).val() == 'seller-individual') {
                    $('#seller-legal-entity-form').hide('150')
                    $('#seller-individual-form').show('300')
                } else {
                    $('#seller-individual-form').hide('150')
                    $('#seller-legal-entity-form').show('300')
                }
            })

            $('input[name=buyer_type]').click(function (event) {
                if ($(this).val() == 'buyer-individual') {
                    $('#buyer-legal-entity-form').hide('150')
                    $('#buyer-individual-form').show('300')
                } else {
                    $('#buyer-individual-form').hide('150')
                    $('#buyer-legal-entity-form').show('300')
                }
            })

            $('[data-toggle="tooltip"]').tooltip();

            additionalInfoButton.click(function (event) {
                event.preventDefault();

                additionalInfoDiv.fadeIn('slow');

            });
        })(jQuery)
    </script>

    {{-- Paypal script --}}
    @if($settings->price > 0)
        <script src="https://www.paypalobjects.com/api/checkout.js"></script>
        <script type="text/javascript">
            (function ($) {
                $('#submit').click(function (event) {
                    event.preventDefault()

                    $('#payments').fadeIn('slow')

                    var CREATE_PAYMENT_URL = '{{ route('website-sale-contract-paypal-create') }}'

                    var EXECUTE_PAYMENT_URL = '{{ route('website-sale-contract-paypal-execute') }}'

                    paypal.Button.render({

                        env: 'sandbox', // Or 'sandbox'

                        commit: true, // Show a 'Pay Now' button

                        payment: function () {
                            return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function (data) {
                                // console.log(data)
                                return data.id
                            })
                        },

                        // onAuthorize() is called when the buyer approves the payment
                        onAuthorize: function (data, actions) {
                            console.log(data)
                            // Set up the data you need to pass to your server
                            var data = {
                                paymentID: data.paymentID,
                                payerID: data.payerID,
                                _token: '{{ csrf_token() }}'
                            };

                            let form_data = $('form').serialize()

                            //Make a POST request with the form data
                            $.ajax({
                                url: '{{ route('store-website-sale-contract') }}',
                                type: 'POST',
                                data: form_data,
                                success: function (response) {
                                    // Make a call to your server to execute the payment
                                    return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                        .then(function (res) {
                                            res = JSON.parse(res)
                                            console.log(res)

                                            //clear form and show success message
                                            $('form input').each(function (index, el) {
                                                $(this).val('')
                                            })

                                            alert('Заявката ви беше успешно изпратена')
                                        })
                                },
                                error: function (data) {
                                    let errors = data.responseJSON.errors

                                    for (error in errors) {
                                        $('input[name=' + error + ']').after('<span class="help-block">' + errors[error][0] + '</span>')
                                    }
                                }
                            })
                        }

                    }, '#paypal-button')

                    console.log('paypal')
                })
            })(jQuery)
        </script>
    @endif
@endsection

