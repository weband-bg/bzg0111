@extends('layouts.main')

@section('title', 'Пълномощно за продажба на МПС')
@section('page_title', 'Пълномощно за продажба на МПС')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
    <h3>{{ $settings->text }}</h3>
</div>

<div id="app">
    <form action="{{ route('store-car-sale-proxy') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-12">
                <h3 class="text-center">Данни на упълномощителя</h3>

                <h4 class="text-center">Упълномощителя е</h4>

                <div class="col-md-6 col-md-offset-3 text-center">
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="seller_type" id="seller_individual" value="individual" v-model="seller_type">
                            <span>Физическо лице</span>
                        </label>
                    </div>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="seller_type" id="seller_legal-entity" value="legal-entity" v-model="seller_type">
                            <span>Юридическо лице</span>
                        </label>
                    </div>
                    @if($errors->has('seller_type'))
                        <span>{{ $errors->first('seller_type') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <transition name="fade" mode="out-in">
        {{-- Individual --}}
            <div v-if="seller_type == 'individual'" key="individual">
                <div class="row">  
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="seller_firstname">Име</label>
                            <input type="text" class="form-control" id="seller_firstname" name="seller_firstname" value="{{ old('seller_firstname') }}">
                            @if($errors->has('seller_firstname'))
                                <span>{{ $errors->first('seller_firstname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="secondname">Презиме</label>
                            <input type="text" class="form-control" id="seller_secondname" name="seller_secondname" value="{{ old('seller_secondname') }}">
                            @if($errors->has('seller_secondname'))
                                <span>{{ $errors->first('seller_secondname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="seller_lastname">Фамилия</label>
                            <input type="text" class="form-control" id="seller_lastname" name="seller_lastname" value="{{ old('seller_lastname') }}">
                            @if($errors->has('seller_lastname'))
                                <span>{{ $errors->first('seller_lastname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="seller_pin">ЕГН</label>
                            <input type="text" class="form-control" id="seller_pin" name="seller_pin" value="{{ old('seller_pin') }}">
                            @if($errors->has('seller_pin'))
                                <span>{{ $errors->first('seller_pin') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seller_id_number">Номер на лична карта</label>
                            <input type="text" class="form-control" id="seller_id_number" name="seller_id_number" value="{{ old('seller_id_number') }}">
                            @if($errors->has('seller_id_number'))
                                <span>{{ $errors->first('seller_id_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seller_id_date_created">Дата на издаване</label>
                            <input type="date" class="form-control" id="seller_id_date_created" name="seller_id_date_created" value="{{ old('seller_id_date_created') }}">
                            @if($errors->has('seller_id_date_created'))
                                <span>{{ $errors->first('seller_id_date_created') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="seller_id_created_by">Издадена от</label>
                            <input type="text" class="form-control" id="seller_id_created_by" name="seller_id_created_by" value="{{ old('seller_id_created_by') }}">
                            @if($errors->has('seller_id_created_by'))
                                <span>{{ $errors->first('seller_id_created_by') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        {{-- Legal entity --}}
            <div v-else-if="seller_type == 'legal-entity'" key="legal-entity">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="seller_name">Име</label>
                            <input type="text" class="form-control" id="seller_name" seller_name="seller_name" value="{{ old('seller_name') }}">
                            @if($errors->has('seller_name'))
                                <span>{{ $errors->first('seller_name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="seller_eik">ЕИК</label>
                            <input type="text" class="form-control" id="seller_eik" seller_name="seller_eik" value="{{ old('seller_eik') }}">
                            @if($errors->has('seller_eik'))
                                <span>{{ $errors->first('seller_eik') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <h4 class="text-center">Данни на управителя на фирмата</h4>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="manager_firstname">Име</label>
                            <input type="text" class="form-control" id="manager_firstname" name="manager_firstname" value="{{ old('manager_firstname') }}">
                            @if($errors->has('manager_firstname'))
                                <span>{{ $errors->first('manager_firstname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="manager_secondname">Презиме</label>
                            <input type="text" class="form-control" id="manager_secondname" name="manager_secondname" value="{{ old('manager_secondname') }}">
                            @if($errors->has('manager_secondname'))
                                <span>{{ $errors->first('manager_secondname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="manager_lastname">Фамилия</label>
                            <input type="text" class="form-control" id="manager_lastname" name="manager_lastname" value="{{ old('manager_lastname') }}">
                            @if($errors->has('manager_lastname'))
                                <span>{{ $errors->first('manager_lastname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="manager_pin">ЕГН</label>
                            <input type="text" class="form-control" id="manager_pin" name="manager_pin" value="{{ old('manager_pin') }}">
                            @if($errors->has('manager_pin'))
                                <span>{{ $errors->first('manager_pin') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="manager_id_number">Номер на лична карта</label>
                            <input type="text" class="form-control" id="manager_id_number" name="manager_id_number" value="{{ old('manager_id_number') }}">
                            @if($errors->has('manager_id_number'))
                                <span>{{ $errors->first('manager_id_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="manager_id_date_created">Дата на издаване</label>
                            <input type="date" class="form-control" id="manager_id_date_created" name="manager_id_date_created" value="{{ old('manager_id_date_created') }}">
                            @if($errors->has('manager_id_date_created'))
                                <span>{{ $errors->first('manager_id_date_created') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="manager_id_created_by">Издадена от</label>
                            <input type="text" class="form-control" id="manager_id_created_by" name="manager_id_created_by" value="{{ old('manager_id_created_by') }}">
                            @if($errors->has('manager_id_created_by'))
                                <span>{{ $errors->first('manager_id_created_by') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </transition>

        <div class="row">
            <h3 class="text-center">Данни на пълномощника</h3>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="proxy_firstname">Име</label>
                    <input type="text" class="form-control" id="proxy_firstname" name="proxy_firstname" value="{{ old('proxy_firstname') }}">
                    @if($errors->has('proxy_firstname'))
                        <span>{{ $errors->first('proxy_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="secondname">Презиме</label>
                    <input type="text" class="form-control" id="proxy_secondname" name="proxy_secondname" value="{{ old('proxy_secondname') }}">
                    @if($errors->has('proxy_secondname'))
                        <span>{{ $errors->first('proxy_secondname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="proxy_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="proxy_lastname" name="proxy_lastname" value="{{ old('proxy_lastname') }}">
                    @if($errors->has('proxy_lastname'))
                        <span>{{ $errors->first('proxy_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="proxy_pin">ЕГН</label>
                    <input type="text" class="form-control" id="proxy_pin" name="proxy_pin" value="{{ old('proxy_pin') }}">
                    @if($errors->has('proxy_pin'))
                        <span>{{ $errors->first('proxy_pin') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="proxy_id_number">Номер на лична карта</label>
                    <input type="text" class="form-control" id="proxy_id_number" name="proxy_id_number" value="{{ old('proxy_id_number') }}">
                    @if($errors->has('proxy_id_number'))
                        <span>{{ $errors->first('proxy_id_number') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="proxy_id_date_created">Дата на издаване</label>
                    <input type="date" class="form-control" id="proxy_id_date_created" name="proxy_id_date_created" value="{{ old('proxy_id_date_created') }}">
                    @if($errors->has('proxy_id_date_created'))
                        <span>{{ $errors->first('proxy_id_date_created') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="proxy_id_created_by">Издадена от</label>
                    <input type="text" class="form-control" id="proxy_id_created_by" name="proxy_id_created_by" value="{{ old('proxy_id_created_by') }}">
                    @if($errors->has('proxy_id_created_by'))
                        <span>{{ $errors->first('proxy_id_created_by') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="form-group">
                    <label for="talon">Моля приложете сканирано копие от големия талон на автомобила</label>
                    <input type="file" class="form-control" id="talon" name="talon" @change="talon = true">
                    @if($errors->has('talon'))
                        <span>{{ old('talon') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div v-if="!talon">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_type">Тип</label>
                        <select name="car_type" id="car_type" class="form-control">
                            @foreach($car_types as $car_type)
                                <option value="{{ $car_type->id }}">{{ $car_type->type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_make">Марка</label>
                        <select name="car_make" id="car_make" class="form-control">
                            @foreach($car_makes as $car_make)
                                <option value="{{ $car_make->id }}">{{ $car_make->make }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_model">Модел</label>
                        <input type="text" class="form-control" id="car_model" name="car_model" value="{{ old('car_model') }}">
                        @if($errors->has('car_model'))
                            <span>{{ $errors->first('car_model') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_chassis">Номер на шаси</label>
                        <input type="text" class="form-control" id="car_chassis" name="car_chassis" value="{{ old('car_chassis') }}">
                        @if($errors->has('car_chassis'))
                            <span>{{ $errors->first('car_chassis') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_registration">Регистрационен номер</label>
                        <input type="text" class="form-control" id="car_registration" name="car_registration" value="{{ old('car_registration') }}">
                        @if($errors->has('car_registration'))
                            <span>{{ $errors->first('car_registration') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="car_color">Цвят</label>
                        <input type="text" class="form-control" name="car_color" name="car_color" value="{{ old('car_color') }}">
                        @if($errors->has('car_color'))
                            <span>{{ $errors->first('car_color') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="price">Минимална продажна цена (в лева)</label>
                    <input type="text" class="form-control" id="price" name="price" value="{{ old('price') }}">
                    @if($errors->has('price'))
                        <span>{{ $errors->first('price') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <div class="checkbox checkbox__custom checkbox__style1">
                    <label>
                        <input type="checkbox" name="self_contract_clause">
                        <span>Клауза за договаряне сам със себе си</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="text-center">Начин на плащане</h3>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="office" value="office" checked>
                        <span>На място в офиса</span>
                    </label>
                </div>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="econt" value="econt">
                        <span>Еконт</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <h3 class="text-center">Вашата информация</h3>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_firstname">Име</label>
                    <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                    @if($errors->has('payer_firstname'))
                        <span>{{ $errors->first('payer_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                    @if($errors->has('payer_lastname'))
                        <span>{{ $errors->first('payer_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_email">Email</label>
                    <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                    @if($errors->has('payer_email'))
                        <span>{{ $errors->first('payer_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_phone">Телефон</label>
                    <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                    @if($errors->has('payer_phone'))
                        <span>{{ $errors->first('payer_phone') }}</span>
                    @endif
                </div>
            </div>
        </div>

        {{-- Submit --}}
        <div class="gap-10"></div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
            </div>
        </div>
    </form>   
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    let app = new Vue({
        el: '#app',
        data: {
            seller_type: {{ old('seller_type') ? old('seller_type') : 'null' }},
            talon: null
        }
    })
</script>

<style>
    .fade-enter-active, .fade-leave-active {
      transition: opacity .4s
    }
    .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
      opacity: 0
    }
</style>

@endsection