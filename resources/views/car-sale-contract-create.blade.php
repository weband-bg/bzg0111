@extends('layouts.main')


@section('title', 'Договор за продажба на МПС')
@section('page_title', 'Договор за продажба на МПС')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

@if(isset($settings))
    <h2>{{ $settings->text }}</h2>
@endif

                    <form action="{{ route('store-car-sale-contract') }}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="talon">Талон на кола</label>
                                <input type="file" name="talon" id="talon" class="form-control">
                                @if($errors->has('talon'))
                                <span class="help-block">{{ $errors->first('talon') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <h2>Продавач</h2>
                            <div class="col-md-6">
                                <label for="seller-legal-entity">Юридическо лице</label>
                                <input type="radio" name="seller_type" id="seller-legal-entity" value="seller-legal-entity" {{ old('seller_type') == 'seller-legal-entity' ? 'checked' : '' }}>
                            </div>
                            <div class="col-md-6">
                                <label for="seller-individual">Физическо лице</label>
                                <input type="radio" name="seller_type" id="seller-individual" value="seller-individual" {{ old('seller_type') == 'seller-individual' ? 'checked' : '' }}>
                            </div>
                            @if($errors->has('seller_type'))
                                <span class="help-block">{{ $errors->first('seller_type') }}</span>
                            @endif
                        </div>
                        <div class="col-md-12" id="seller-individual-form" {!! old('type') != 'seller-individual' ? 'style="display: none;"' : '' !!}>
                            <h2 class="text-center">Физическо лице</h2>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label for="firstname">Име</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" value="{{ old('firstname') }}">
                                    @if($errors->has('firstname'))
                                        <span class="help-block">{{ $errors->first('firstname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="secondname">Презиме</label>
                                    <input type="text" name="secondname" id="secondname" class="form-control"
                                           value="{{ old('secondname') }}">
                                    @if($errors->has('secondname'))
                                        <span class="help-block">{{ $errors->first('secondname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="lastname">Фамилия</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" value="{{ old('lastname') }}">
                                    @if($errors->has('lastname'))
                                        <span class="help-block">{{ $errors->first('lastname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="pin">ЕГН</label>
                                    <input type="text" name="pin" id="pin" class="form-control" value="{{ old('pin') }}">
                                    @if($errors->has('pin'))
                                        <span class="help-block">{{ $errors->first('pin') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center"
                             id="seller-legal-entity-form" {!! old('seller_type') != 'seller-legal-entity' ? 'style="display: none;"' : '' !!}>
                            <h2 class="text-center">Юридическо лице</h2>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="name">Име</label>
                                    <input type="text" name="seller_name" id="seller_name" class="form-control" 
                                           value="{{ old('seller_name') }}">
                                    @if($errors->has('seller_name'))
                                        <span class="help-block">{{ $errors->first('seller_name') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="seller_eik">ЕИК</label>
                                    <input type="text" name="seller_eik" id="seller_eik" class="form-control" value="{{ old('seller_eik') }}">
                                    @if($errors->has('seller_eik'))
                                        <span class="help-block">{{ $errors->first('seller_eik') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <h2>Купувач</h2>
                            <div class="col-md-6">
                                <label for="buyer-legal-entity">Юридическо лице</label>
                                <input type="radio" name="buyer_type" id="buyer-legal-entity"
                                       value="buyer-legal-entity" {{ old('buyer_type') == 'buyer-legal-entity' ? 'checked' : '' }}>
                            </div>
                            <div class="col-md-6">
                                <label for="buyer-individual">Физическо лице</label>
                                <input type="radio" name="buyer_type" id="buyer-individual"
                                       value="buyer-individual" {{ old('buyer_type') == 'buyer-individual' ? 'checked' : '' }}>
                            </div>
                            @if($errors->has('buyer_type'))
                                <span class="help-block">{{ $errors->first('buyer_type') }}</span>
                            @endif
                        </div>
                        <div class="col-md-12"
                             id="buyer-individual-form" {!! old('buyer_type') != 'buyer-individual' ? 'style="display: none;"' : '' !!}>
                            <h2 class="text-center">Физическо лице</h2>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label for="buyer_firstname">Име</label>
                                    <input type="text" name="buyer_firstname" id="buyer_firstname" class="form-control"
                                           value="{{ old('buyer_firstname') }}">
                                    @if($errors->has('buyer_firstname'))
                                        <span class="help-block">{{ $errors->first('buyer_firstname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="buyer_secondname">Презиме</label>
                                    <input type="text" name="buyer_secondname" id="buyer_secondname" class="form-control"
                                           value="{{ old('buyer_secondname') }}">
                                    @if($errors->has('buyer_secondname'))
                                        <span class="help-block">{{ $errors->first('buyer_secondname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="buyer_lastname">Фамилия</label>
                                    <input type="text" name="buyer_lastname" id="buyer_lastname" class="form-control"
                                           value="{{ old('buyer_lastname') }}">
                                    @if($errors->has('buyer_lastname'))
                                        <span class="help-block">{{ $errors->first('buyer_lastname') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-3">
                                    <label for="buyer_pin">ЕГН</label>
                                    <input type="text" name="buyer_pin" id="buyer_pin" class="form-control" value="{{ old('buyer_pin') }}">
                                    @if($errors->has('buyer_pin'))
                                        <span class="help-block">{{ $errors->first('buyer_pin') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12"
                             id="buyer-legal-entity-form" {!! old('buyer_type') != 'buyer-legal-entity' ? 'style="display: none;"' : '' !!}>
                            <h2 class="text-center">Юридическо лице</h2>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <label for="buyer_name">Име</label>
                                    <input type="text" name="buyer_name" id="buyer_name" class="form-control"
                                           value="{{ old('buyer_name') }}">
                                    @if($errors->has('buyer_name'))
                                        <span class="help-block">{{ $errors->first('buyer_name') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="eik">ЕИК</label>
                                    <input type="buyer_eik" name="buyer_eik" id="buyer_eik" class="form-control" value="{{ old('buyer_eik') }}">
                                    @if($errors->has('buyer_eik'))
                                        <span class="help-block">{{ $errors->first('buyer_eik') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="col-md-12 text-center">
                        <h2 class="text-center">Допълнителна информация</h2>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="type">Тип кола</label>
                                    <select name="type" id="type" class="form-control">
                                        @foreach($types as $type)
                                            @if($type->id==old('type'))
                                                <option value="{{ $type->id }}" selected>{{ $type->type }}</option>
                                            @endif
                                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('type'))
                                        <span class="help-block">{{ $errors->first('type') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="make">Марка</label>
                                    <select name="make" id="make" class="form-control">
                                        @foreach($makes as $make)
                                            <option value="{{ $make->id }}">{{ $make->make }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('make'))
                                        <span class="help-block">{{ $errors->first('make') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label for="model">Модел</label>
                                <input type="text" name="model" id="model" class="form-control" value="{{ old('model') }}">
                                @if($errors->has('model'))
                                    <span class="help-block">{{ $errors->first('model') }}</span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <label for="chassis">Номер на шаси</label>
                                <input type="text" name="chassis" id="chassis" class="form-control" value="{{ old('chassis') }}">
                                @if($errors->has('chassis'))
                                    <span class="help-block">{{ $errors->first('chassis') }}</span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <label for="registration">Регистрационен номер</label>
                                <input type="text" name="registration" id="registration" class="form-control"
                                       value="{{ old('registration') }}">
                                @if($errors->has('registration'))
                                    <span class="help-block">{{ $errors->first('registration') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <label for="price">Продажна цена</label>
                                <input type="text" name="price" id="price" class="form-control" value="{{ old('price') }}">
                                @if($errors->has('price'))
                                    <span class="help-block">{{ $errors->first('price') }}</span>
                                @endif
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="payment">Начин на плащане</label>
                                    <select name="payment" id="payment" class="form-control">
                                        <option value="1">Продажната цена е платен предварително и продавачът заявява това
                                            пред
                                            нотариуса
                                        </option>
                                        <option value="2">Продажната цена се заплаща в брой в офиса на нотрариуса</option>
                                        <option value="3">Продажната цена се заплаща непосредствено след подписване на
                                            договора, по
                                            банков път
                                        </option>
                                        <option value="4">Продажната цена ще бъде заплатена в срок до _________
                                        </option>
                                    </select>
                                    @if($errors->has('payment'))
                                    <span class="help-block">{{ $errors->first('payment') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 payment-date" style="display: none;">
                            <div class="form-group">
                                <label for="payment_date">Крайната дата за плащане</label>
                                <input type="date" id="payment_date" class="form-control" name="payment_date" value="{{ old('payment_date') }}">
                                @if($errors->has('payment_date'))
                                <span class="help-block">{{ $errors->first('payment_date') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-4">
                                <div class="form-group">
                                    <label for="expenses">Разноски. За сметка на ...</label>
                                    <select name="expenses" id="expenses" class="form-control">
                                        <option value="1">Продавача</option>
                                        <option value="2">Купувача</option>
                                    </select>
                                    @if($errors->has('expenses'))
                                    <span class="help-block">{{ $errors->first('expenses') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        {{-- @if(isset($settings))
                            <h2 class="dt-sc-hr-icon-title">{{ $settings->text }}</h2>
                        @endif --}}
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_firstname">Име</label>
                                    <input type="text" class="form-control"
                                           id="owner_firstname" name="owner_firstname"
                                           value="{{ old('owner_firstname') }}">
                                    @if($errors->has('owner_firstname'))
                                        <span class="help-block">{{ $errors->first('owner_firstname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_secondname">Презиме</label>
                                    <input type="text"
                                            class="form-control"
                                            id="owner_secondname" name="owner_secondname"
                                            value="{{ old('owner_secondname') }}">
                                    @if($errors->has('owner_secondname'))
                                        <span class="help-block">{{ $errors->first('owner_secondname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="owner_lastname">Фамилия</label>
                                    <input type="text" class="form-control"
                                           id="owner_lastname" name="owner_lastname"
                                           value="{{ old('owner_lastname') }}">
                                    @if($errors->has('owner_lastname'))
                                        <span class="help-block">{{ $errors->first('owner_lastname') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="owner_email">Е-мейл</label>
                                    <input type="text" class="owner_email form-control" name="owner_email">
                                    @if($errors->has('owner_email'))
                                        <span class="help-block">{{ $errors->first('owner_email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="owner_phone">Телефон</label>
                                    <input type="text" class="owner_phone form-control" name="owner_phone">
                                    @if($errors->has('owner_phone'))
                                        <span class="help-block">{{ $errors->first('owner_phone') }}</span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <h3 class="text-center">Начин на плащане</h3>
                                <div class="radio radio__custom radio__style1">
                                    <label>
                                        <input type="radio" name="payment_method" id="office" value="office" checked>
                                        <span>На място в офиса</span>
                                    </label>
                                </div>
                                <div class="radio radio__custom radio__style1">
                                    <label>
                                        <input type="radio" name="payment_method" id="econt" value="econt">
                                        <span>Еконт</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <h3 class="text-center">Вашата информация</h3>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="payer_firstname">Име</label>
                                    <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                                    @if($errors->has('payer_firstname'))
                                        <span>{{ $errors->first('payer_firstname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="payer_lastname">Фамилия</label>
                                    <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                                    @if($errors->has('payer_lastname'))
                                        <span>{{ $errors->first('payer_lastname') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="payer_email">Email</label>
                                    <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                                    @if($errors->has('payer_email'))
                                        <span>{{ $errors->first('payer_email') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="payer_phone">Телефон</label>
                                    <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                                    @if($errors->has('payer_phone'))
                                        <span>{{ $errors->first('payer_phone') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 text-center">
                            <button class="btn btn-success" type="submit" id="submit"><span class="fa fa-legal"> </span> Завърши заявка
                            </button>
                        </div>


                    </form>
            
            @include('reviews.review-form')

{{--         </div>
    </section> --}}

@endsection

@section('scripts')
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        (function ($) {
            var additionalInfoButton = $('#additional-info-button');
            var additionalInfoDiv = $('.additional-info');
            var paymentSelect = $('#payment');

            $('input[name=seller_type]').click(function (event) {
                if ($(this).val() == 'seller-individual') {
                    $('#seller-legal-entity-form').hide('150')
                    $('#seller-individual-form').show('300')
                } else {
                    $('#seller-individual-form').hide('150')
                    $('#seller-legal-entity-form').show('300')
                }
            })

            $('input[name=buyer_type]').click(function (event) {
                if ($(this).val() == 'buyer-individual') {
                    $('#buyer-legal-entity-form').hide('150')
                    $('#buyer-individual-form').show('300')
                } else {
                    $('#buyer-individual-form').hide('150')
                    $('#buyer-legal-entity-form').show('300')
                }
            })

            $('[data-toggle="tooltip"]').tooltip();

            additionalInfoButton.click(function (event) {
                event.preventDefault();

                additionalInfoDiv.fadeIn('slow');

            });

            paymentSelect.on('change', function (event) {
                console.log(event.target.value);

                if (event.target.value == 4) {
                    $('.payment-date').show('150');

                } else {
                    $('.payment-date').hide('150');
                }
            })
        })(jQuery)
    </script>
@endsection

