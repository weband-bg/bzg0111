@extends('layouts.main')

@section('title', 'Промяна на предмета на дейност')
@section('page_title', 'Промяна на предмета на дейност')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
    <h3>{{ $settings->text }}</h3>
</div>

<div id="app">
    <form action="{{ route('store-change-company-activity-subject') }}" method="POST">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="company_name">Име на фирма</label>
                    <input type="text" class="form-control" id="company_name" name="company_name" value="{{ old('company_name') }}">
                    @if($errors->has('company_name'))
                        <span>{{ $errors->first('company_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="eik">ЕИК на фирма</label>
                    <input type="text" class="form-control" id="eik" name="eik" value="{{ old('eik') }}">
                    @if($errors->has('eik'))
                        <span>{{ $errors->first('eik') }}</span>
                    @endif
                </div>
            </div>
        </div> 

        <div class="drag">
            <h4 class="text-center">Предмет на дейност</h4>
            <draggable :list="activities" class="dragArea">
                <div v-for="(activity, index) in activities" class="col-md-4">
                    <div class="company-activity">
                        <input type="hidden" :name="'activities['+index+']'" :value="activity.id">
                        @{{activity.name}}
                        <i class="fa fa-close" @click="removeActivity(index)"></i>
                    </div>
                </div>
             </draggable>
             <div class="clearfix"></div>
         </div>

        <div class="gap-20"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="additional_activities">Допълнителни дейности (разделени със запетая)</label>
                    <textarea class="form-control" name="additional_activities" id="additional_activities" rows="5">{{ old('additional_activities') }}</textarea>
                </div>
            </div>
        </div>  

        {{-- Confirm --}}
        <div class="gap-10"></div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <button class="btn btn-success btn-block btn-has-icon" type="button" @click="confirmed = true"><i class="fa fa-gavel"></i> Потвърди заявка</button>
                <span>(100 лв. с вкл. държавна, банкова такса и хонорар)</span>
            </div>
        </div>

        <transition name="fade">
            <div v-show="confirmed">
                <div class="row">
                    <h4 class="text-center">Лични данни</h4>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_firstname">Име</label>
                            <input type="text" class="form-control" id="owner_firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
                            @if($errors->has('owner_firstname'))
                                <span>{{ $errors->first('owner_firstname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_secondname">Презиме</label>
                            <input type="text" class="form-control" id="owner_secondname" name="owner_secondname" value="{{ old('owner_secondname') }}">
                            @if($errors->has('owner_secondname'))
                                <span>{{ $errors->first('owner_secondname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_lastname">Фамилия</label>
                            <input type="text" class="form-control" id="owner_lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
                            @if($errors->has('owner_lastname'))
                                <span>{{ $errors->first('owner_lastname') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_email">Email</label>
                            <input type="email" class="form-control" id="owner_email" name="owner_email" value="{{ old('owner_email') }}">
                            @if($errors->first('owner_email'))
                                <span>{{ $errors->first('owner_email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_id_expiry_date">Дата на валидност на лична карта</label>
                            <input type="date" class="form-control" id="owner_id_expiry_date" name="owner_id_expiry_date" value="{{ old('owner_id_expiry_date') }}">
                            @if($errors->first('owner_id_expiry_date'))
                                <span>{{ $errors->first('owner_id_expiry_date') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="owner_phone">Телефон</label>
                            <input type="tel" class="form-control" id="owner_phone" name="owner_phone" value="{{ old('owner_phone') }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <h3 class="text-center">Начин на плащане</h3>
                        <div class="radio radio__custom radio__style1">
                            <label>
                                <input type="radio" name="payment_method" id="office" value="office" checked>
                                <span>На място в офиса</span>
                            </label>
                        </div>
                        <div class="radio radio__custom radio__style1">
                            <label>
                                <input type="radio" name="payment_method" id="econt" value="econt">
                                <span>Еконт</span>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <h3 class="text-center">Вашата информация</h3>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="payer_firstname">Име</label>
                            <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                            @if($errors->has('payer_firstname'))
                                <span>{{ $errors->first('payer_firstname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="payer_lastname">Фамилия</label>
                            <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                            @if($errors->has('payer_lastname'))
                                <span>{{ $errors->first('payer_lastname') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="payer_email">Email</label>
                            <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                            @if($errors->has('payer_email'))
                                <span>{{ $errors->first('payer_email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="payer_phone">Телефон</label>
                            <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                            @if($errors->has('payer_phone'))
                                <span>{{ $errors->first('payer_phone') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                {{-- Submit --}}
                <div class="gap-10"></div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
                    </div>
                </div>
            </div>
        </transition>
    </form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.7.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.15.0/vuedraggable.min.js"></script>

<script>
    let app = new Vue({
        el: '#app',
        data: {
            confirmed: false,
            activities: []
        },
        created: function() {
            this.getActivities()
        },
        methods: {
            getActivities: function() {
                let app = this

                axios.get('{{ route('get-activities') }}')
                    .then(function(response) {
                        app.activities = response.data
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            },
            removeActivity: function(index) {
                console.log('remove activity ' + index)

                this.activities.splice(index, 1)
            }
        }
    })
</script>

<style>
    .fade-enter-active, .fade-leave-active {
      transition: opacity .4s
    }
    .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
      opacity: 0
    }
    .company-activity {
        color: black;
        background: #F9F9F9;
        padding: 10px;
        border: 1px solid #F3F3F3;
    }
    .company-activity .fa {
        display: inline-block;
        float: right;
        cursor: pointer;
    }
</style>
@endsection