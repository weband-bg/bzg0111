@extends('layouts.main')

@section('content')
    <section id="primary" class="content-full-width">
        <div class="container">
            <h2 class="dt-sc-hr-title">Как работим</h2>

            @if(isset($settings))
                <h2 class="dt-sc-hr-icon-title">{{ $settings->text }}</h2>
            @endif

            <div class="dt-sc-one-column">
                <div class="dt-sc-one-column">

                </div>
            </div>
        </div>
    </section>
@endsection
