@extends('layouts.main')

@section('title', 'Регистрация на ЕТ')
@section('page_title', 'Регистрация на ЕТ')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
    <h3>{{ $settings->text }}</h3>
</div>

<div id="app">	
	<form action="{{ route('store-et') }}" method="POST">
		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="sole_trader_name">Име на ЕТ</label>
					<input type="text" class="form-control" id="sole_trader_name" name="sole_trader_name" value="{{ old('sole_trader_name') }}">
					@if($errors->has('sole_trader_name'))
						<span>{{ $errors->first('sole_trader_name') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">	
				<div class="form-group">
					<label for="sole_trader_foreign_name">Как ще се изписва името на чужд език</label>
					<input type="text" class="form-control" id="sole_trader_foreign_name" name="sole_trader_foreign_name" value="{{ old('sole_trader_foreign_name') }}">
					@if($errors->has('sole_trader_foreign_name'))
						<span>{{ $errors->first('sole_trader_foreign_name') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Address --}}
		<div class="row">
			<h4 class="text-center">Адрес</h4>

			<div class="col-md-4">
				<div class="form-group">
					<label for="zone">Област</label>
					<select class="form-control" id="zone" name="zone" v-model="currentZone" @change="getTownships">
						<option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>
					</select>
					@if($errors->has('zone'))
						<span>{{ $errors->first('zone') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="township">Община</label>
					<select class="form-control" id="township" name="township" v-model="currentTownship" @change="getPopulatedPlaces">
						<option v-for="township in townships" :value="township.id">@{{ township.name }}</option>
					</select>
					@if($errors->has('township'))
						<span>{{ $errors->first('township') }}</span>
					@endif	
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="populated_place">Населено място</label>
					<select class="form-control" id="populated_place" name="populated_place" v-model="currentPopulatedPlace">
						<option v-for="populated_place in populated_places" :value="populated_place.id">@{{ populated_place.name }}</option>
					</select>
					@if($errors->has('populated_place'))
						<span>{{ $errors->first('populated_place') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="postcode">Пощенски код</label>
					<input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode') }}">
					@if($errors->has('postcode'))
						<span>{{ $errors->first('postcode') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="quarter">Квартал</label>
					<input type="text" class="form-control" id="quarter" name="quarter" value="{{ old('quarter') }}">
					@if($errors->has('quarter'))
						<span>{{ $errors->first('quarter') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street">Улица/Булевард</label>
					<input type="text" class="form-control" id="street" name="street" value="{{ old('street') }}">
					@if($errors->has('street'))
						<span>{{ $errors->first('street') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street-number">Улица/Булевард - номер</label>
					<input type="text" class="form-control" id="street-number" name="street_number" value="{{ old('street_number') }}">
					@if($errors->has('street_number'))
						<span>{{ $errors->first('street_number') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="building">Блок</label>
					<input type="text" class="form-control" id="building" name="building" value="{{ old('building') }}">
					@if($errors->has('building'))
						<span>{{ $errors->first('building') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="entrance">Вход</label>
					<input type="text" class="form-control" id="entrance" name="entrance" value="{{ old('entrance') }}">
					@if($errors->has('entrance'))
						<span>{{ $errors->first('entrance') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="floor">Етаж</label>
					<input type="text" class="form-control" id="floor" name="floor" value="{{ old('floor') }}">
					@if($errors->has('floor'))
						<span>{{ $errors->first('floor') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="apartment">Апартамент</label>
					<input type="text" class="form-control" id="apartment" name="apartment" value="{{ old('apartment') }}">
					@if($errors->has('apartment'))
						<span>{{ $errors->first('apartment') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Different address --}}
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="checkbox checkbox__custom checkbox__style1">
					<label>
						<input type="checkbox" value="different_address" v-model="different_address">
						<span>Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление</span>
					</label>
				</div>
			</div>
		</div>

		<transition name="fade" mode="out-in">
			<div v-if="different_address">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="address-2-zone">Област</label>
							<select class="form-control" id="address-2-zone" name="address_2_zone" v-model="currentZone2" @change="getTownships2">
								<option v-for="zone in zones2" :value="zone.id">@{{ zone.name }}</option>
							</select>
							@if($errors->has('address_2_zone'))
								<span>{{ $errors->first('address_2_zone') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="address-2-township">Община</label>
							<select class="form-control" id="address-2-township" name="address_2_township" v-model="currentTownship2" @change="getPopulatedPlaces2">
								<option v-for="township in townships2" :value="township.id">@{{ township.name }}</option>
							</select>
							@if($errors->has('address_2_township'))
								<span>{{ $errors->first('address_2_township') }}</span>
							@endif	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="address_2_populated_place">Населено място</label>
							<select class="form-control" id="address_2_populated_place" name="address_2_populated_place" v-model="currentPopulatedPlace2">
								<option v-for="populated_place in populated_places2" :value="populated_place.id">@{{ populated_place.name }}</option>
							</select>
							@if($errors->has('address_2_populated_place'))
								<span>{{ $errors->first('address_2_populated_place') }}</span>
							@endif
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-postcode">Пощенски код</label>
							<input type="text" class="form-control" id="address-2-postcode" name="address_2_postcode" value="{{ old('address_2_postcode') }}">
							@if($errors->has('address_2_postcode'))
								<span>{{ $errors->first('address_2_postcode') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-quarter">Квартал</label>
							<input type="text" class="form-control" id="address-2-quarter" name="address_2_quarter" value="{{ old('address_2_quarter') }}">
							@if($errors->has('address_2_quarter'))
								<span>{{ $errors->first('address_2_quarter') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-street">Улица/Булевард</label>
							<input type="text" class="form-control" id="address-2-street" name="address_2_street" value="{{ old('address_2_street') }}">
							@if($errors->has('address_2_street'))
								<span>{{ $errors->first('address_2_street') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-street-number">Улица/Булевард - номер</label>
							<input type="text" class="form-control" id="address-2-street-number" name="address_2_street_number" value="{{ old('address_2_street_number') }}">
							@if($errors->has('address_2_street_number'))
								<span>{{ $errors->first('address_2_street_number') }}</span>
							@endif
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-building">Блок</label>
							<input type="text" class="form-control" id="address-2-building" name="address_2_building" value="{{ old('address_2_building') }}">
							@if($errors->has('address_2_building'))
								<span>{{ $errors->first('address_2_building') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-entrance">Вход</label>
							<input type="text" class="form-control" id="address-2-entrance" name="address_2_entrance" value="{{ old('address_2_entrance') }}">
							@if($errors->has('address_2_entrance'))
								<span>{{ $errors->first('address_2_entrance') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-floor">Етаж</label>
							<input type="text" class="form-control" id="address-2-floor" name="address_2_floor" value="{{ old('address_2_floor') }}">
							@if($errors->has('address_2_floor'))
								<span>{{ $errors->first('address_2_floor') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-apartment">Апартамент</label>
							<input type="text" class="form-control" id="address-2-apartment" name="address_2_apartment" value="{{ old('address_2_apartment') }}">
							@if($errors->has('address_2_apartment'))
								<span>{{ $errors->first('address_2_apartment') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</transition>

		<div class="drag">
            <h4 class="text-center">Предмет на дейност</h4>
            <draggable :list="activities" class="dragArea">
            	<div v-for="(activity, index) in activities" class="col-md-4">
            		<div class="company-activity">
            			<input type="hidden" :name="'activities['+index+']'" :value="activity.id">
            			@{{activity.name}}
            			<i class="fa fa-close" @click="removeActivity(index)"></i>
            		</div>
            	</div>
             </draggable>
             <div class="clearfix"></div>
         </div>

		<div class="gap-20"></div>

		<div class="row">
		    <div class="col-md-12">
		        <div class="form-group">
		            <label for="additional_activities">Допълнителни дейности (разделени със запетая)</label>
		            <textarea class="form-control" name="additional_activities" id="additional_activities" rows="5">{{ old('additional_activities') }}</textarea>
		        </div>
		    </div>
		</div> 

		<div class="row">
			<h4 class="text-center">Физическо лице търговец</h4>

			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_firstname">Име</label>
					<input type="text" class="form-control" id="owner_firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
					@if($errors->has('owner_firstname'))
						<span>{{ $errors->first('owner_firstname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_secondname">Презиме</label>
					<input type="text" class="form-control" id="owner_secondname" name="owner_secondname" value="{{ old('owner_secondname') }}">
					@if($errors->has('owner_secondname'))
						<span>{{ $errors->first('owner_secondname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_lastname">Фамилия</label>
					<input type="text" class="form-control" id="owner_lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
					@if($errors->has('owner_lastname'))
						<span>{{ $errors->first('owner_lastname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_pin">ЕГН</label>
					<input type="text" class="form-control" id="owner_pin" name="owner_pin" value="{{ old('owner_pin') }}">
					@if($errors->has('owner_pin'))
						<span>{{ $errors->first('owner_pin') }}</span>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="owner_id_number">Номер на лична карта</label>
					<input type="text" class="form-control" id="owner_id_number" name="owner_id_number" value="{{ old('owner_id_number') }}">
					@if($errors->has('owner_id_number'))
						<span>{{ $errors->first('owner_id_number') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="owner_id_date_created">Дата на издаване</label>
					<input type="date" class="form-control" id="owner_id_date_created" name="owner_id_date_created" value="{{ old('owner_id_date_created') }}">
					@if($errors->has('owner_id_date_created'))
						<span>{{ $errors->first('owner_id_date_created') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="owner_id_created_by"></label>
					<input type="text" class="form-control" id="owner_id_created_by" name="owner_id_created_by" value="{{ old('owner_id_created_by') }}">
					@if($errors->has('owner_id_created_by'))
						<span>{{ $errors->first('owner_id_created_by') }}</span>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h3 class="text-center">Начин на плащане</h3>
				<div class="radio radio__custom radio__style1">
					<label>
						<input type="radio" name="payment_method" id="office" value="office" checked>
						<span>На място в офиса</span>
					</label>
				</div>
				<div class="radio radio__custom radio__style1">
					<label>
						<input type="radio" name="payment_method" id="econt" value="econt">
						<span>Еконт</span>
					</label>
				</div>
			</div>
		</div>

		<div class="row">
			<h3 class="text-center">Вашата информация</h3>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_firstname">Име</label>
					<input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
					@if($errors->has('payer_firstname'))
						<span>{{ $errors->first('payer_firstname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_lastname">Фамилия</label>
					<input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
					@if($errors->has('payer_lastname'))
						<span>{{ $errors->first('payer_lastname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_email">Email</label>
					<input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
					@if($errors->has('payer_email'))
						<span>{{ $errors->first('payer_email') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_phone">Телефон</label>
					<input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
					@if($errors->has('payer_phone'))
						<span>{{ $errors->first('payer_phone') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Submit --}}
		<div class="gap-10"></div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
			</div>
		</div>
	</form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.7.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.15.0/vuedraggable.min.js"></script>

<script>
	let app = new Vue({
		el: '#app',
		data: {
			currentZone: {{ old('zone') ? old('zone') : 1 }},
			currentZone2: {{ old('address_2_zone') ? old('address_2_zone') : 1 }},
			currentTownship: {{ old('township') ? old('township') : 1 }},
			currentTownship2: {{ old('address_2_township') ? old('address_2_township') : 1 }},
			currentPopulatedPlace: {{ old('populated_place') ? old('populated_place') : 1 }},
			currentPopulatedPlace2: {{ old('address_2_populated_place') ? old('address_2_populated_place') : 1 }},
			zones: [],
			zones2: [],
			townships: [],
			townships2: [],
			populated_places: [],
			populated_places2: [],
			different_address: {{ old('different_address') !== null ? old('different_address') : 'false' }},
			activities: []
		},
		created: function() {
			this.getZones(),
			this.getTownships(),
			this.getZones2(),
			this.getTownships2(),
			this.getActivities()
		},
		methods: {
			getZones: function() {
				let app = this

				axios.get('{{ route('zones-json') }}')
					.then(function(response) {
						app.zones = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getTownships: function() {
				let app = this

				axios.get('/api/zone/'+this.currentZone+'/townships')
					.then(function(response) {
						app.townships = response.data
						app.currentTownship = response.data[0].id

						app.getPopulatedPlaces()
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getPopulatedPlaces: function() {
				let app = this

				axios.get('/api/township/'+this.currentTownship+'/populated-places')
					.then(function(response) {
						app.populated_places = response.data
						app.currentPopulatedPlace = response.data[0].id
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getZones2: function() {
				let app = this

				axios.get('{{ route('zones-json') }}')
					.then(function(response) {
						app.zones2 = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getTownships2: function() {
				let app = this

				axios.get('/api/zone/'+this.currentZone2+'/townships')
					.then(function(response) {
						app.townships2 = response.data
						app.currentTownship2 = response.data[0].id

						app.getPopulatedPlaces2()
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getPopulatedPlaces2: function() {
				let app = this

				axios.get('/api/township/'+this.currentTownship2+'/populated-places')
					.then(function(response) {
						app.populated_places2 = response.data
						app.currentPopulatedPlace2 = response.data[0].id
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getActivities: function() {
				let app = this

				axios.get('{{ route('get-activities') }}')
					.then(function(response) {
						app.activities = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			removeActivity: function(index) {
				console.log('remove activity ' + index)

				this.activities.splice(index, 1)
			}
		}
	})
</script>

<style>
	.fade-enter-active, .fade-leave-active {
	  transition: opacity .4s
	}
	.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
	  opacity: 0
	}
	.company-activity {
		color: black;
		background: #F9F9F9;
		padding: 10px;
		border: 1px solid #F3F3F3;
	}
	.company-activity .fa {
		display: inline-block;
		float: right;
		cursor: pointer;
	}
</style>
@endsection