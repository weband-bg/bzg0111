@extends('layouts.main')

@section('content')
<section id="primary" class="content-full-width">
	<div class="container">
		<h2 class="dt-sc-hr-title"> Регистрация на фирма </h2>
		<div class="row">
			<div class="column dt-sc-one-third first text-center">
				<a href="{{ route('register-ltd') }}" class="dt-sc-button large button-color-class"> <span class="fontawesome-icon-class"> </span> Регистрация на ЕООД </a>
			</div>
			<div class="column dt-sc-one-third text-center">
				<a href="{{ route('register-ood') }}" class="dt-sc-button large button-color-class"> <span class="fontawesome-icon-class"> </span> Регистрация на ООД </a>
			</div>
			<div class="column dt-sc-one-third text-center">
				<a href="{{ route('register-et') }}" class="dt-sc-button large button-color-class"> <span class="fontawesome-icon-class"> </span> Регистрация на ЕТ </a>
			</div>
		</div>
	</div>
</section>
@endsection