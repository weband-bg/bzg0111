@extends('layouts.main')

@section('title', 'Регистрация на ООД')
@section('page_title', 'Регистрация на ООД')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
	<div class="alert alert-success alert-dismissible fade in text-uppercase">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
		<strong>{{ Session::get('success') }}</strong>
	</div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
	<strong>{{ Session::has('error') }}</strong>
</div>
@endif

@if($errors->any())

@foreach($errors->all() as $error)
	<div class="alert alert-danger alert-dismissible fade in text-uppercase">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
		<strong>{{ $error }}</strong>
	</div>
@endforeach

@endif

<div class="title-bordered border__dashed">
	<h3>{{ $settings->text }}</h3>
</div>

<div id="app">
	<form action="{{ route('store-ood') }}" method="POST">
		{{csrF_field()}}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label for="company-name">Име на фирмата</label>
					<input type="text" class="form-control" id="company-name" name="company_name" value="{{ old('company_name') }}">
					<a href="https://public.brra.bg/CheckUps/Verifications/RightsOverCompany.ra" target="_blank">Проверете дали избраното от вас име не е заето</a>
					@if($errors->has('company_name'))
						<span>{{ $errors->first('company_name') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="foreign-company-name">Как ще се изписва името на чужд език</label>
					<input type="text" class="form-control" id="foreign-company-name" name="company_foreign_language_name" value="{{ old('company_foreign_language_name') }}">
					@if($errors->has('company_foreign_language_name'))
						<span>{{ $errors->first('company_foreign_language_name') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Address --}}
		<div class="row">
			<h4 class="text-center">Адрес</h4>

			<div class="col-md-4">
				<div class="form-group">
					<label for="zone">Област</label>
					<select class="form-control" id="zone" name="zone" v-model="currentZone" @change="getTownships">
						<option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>
					</select>
					@if($errors->has('zone'))
						<span>{{ $errors->first('zone') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="township">Община</label>
					<select class="form-control" id="township" name="township" v-model="currentTownship" @change="getPopulatedPlaces">
						<option v-for="township in townships" :value="township.id">@{{ township.name }}</option>
					</select>
					@if($errors->has('township'))
						<span>{{ $errors->first('township') }}</span>
					@endif	
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="populated_place">Населено място</label>
					<select class="form-control" id="populated_place" name="populated_place" v-model="currentPopulatedPlace">
						<option v-for="populated_place in populated_places" :value="populated_place.id">@{{ populated_place.name }}</option>
					</select>
					@if($errors->has('populated_place'))
						<span>{{ $errors->first('populated_place') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="postcode">Пощенски код</label>
					<input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode') }}">
					@if($errors->has('postcode'))
						<span>{{ $errors->first('postcode') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="quarter">Квартал</label>
					<input type="text" class="form-control" id="quarter" name="quarter" value="{{ old('quarter') }}">
					@if($errors->has('quarter'))
						<span>{{ $errors->first('quarter') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street">Улица/Булевард</label>
					<input type="text" class="form-control" id="street" name="street" value="{{ old('street') }}">
					@if($errors->has('street'))
						<span>{{ $errors->first('street') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street-number">Улица/Булевард - номер</label>
					<input type="text" class="form-control" id="street-number" name="street_number" value="{{ old('street_number') }}">
					@if($errors->has('street_number'))
						<span>{{ $errors->first('street_number') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="building">Блок</label>
					<input type="text" class="form-control" id="building" name="building" value="{{ old('building') }}">
					@if($errors->has('building'))
						<span>{{ $errors->first('building') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="entrance">Вход</label>
					<input type="text" class="form-control" id="entrance" name="entrance" value="{{ old('entrance') }}">
					@if($errors->has('entrance'))
						<span>{{ $errors->first('entrance') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="floor">Етаж</label>
					<input type="text" class="form-control" id="floor" name="floor" value="{{ old('floor') }}">
					@if($errors->has('floor'))
						<span>{{ $errors->first('floor') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="apartment">Апартамент</label>
					<input type="text" class="form-control" id="apartment" name="apartment" value="{{ old('apartment') }}">
					@if($errors->has('apartment'))
						<span>{{ $errors->first('apartment') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Different address --}}
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center">
				<div class="checkbox checkbox__custom checkbox__style1">
					<label>
						<input type="checkbox" value="different_address" v-model="different_address">
						<span>Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление</span>
					</label>
				</div>
			</div>
		</div>

		<transition name="fade" mode="out-in">
			<div v-if="different_address">
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="address-2-zone">Област</label>
							<select class="form-control" id="address-2-zone" name="address_2_zone" v-model="currentZone2" @change="getTownships2">
								<option v-for="zone in zones2" :value="zone.id">@{{ zone.name }}</option>
							</select>
							@if($errors->has('address_2_zone'))
								<span>{{ $errors->first('address_2_zone') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="address-2-township">Община</label>
							<select class="form-control" id="address-2-township" name="address_2_township" v-model="currentTownship2" @change="getPopulatedPlaces2">
								<option v-for="township in townships2" :value="township.id">@{{ township.name }}</option>
							</select>
							@if($errors->has('address_2_township'))
								<span>{{ $errors->first('address_2_township') }}</span>
							@endif	
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="address_2_populated_place">Населено място</label>
							<select class="form-control" id="address_2_populated_place" name="address_2_populated_place" v-model="currentPopulatedPlace2">
								<option v-for="populated_place in populated_places2" :value="populated_place.id">@{{ populated_place.name }}</option>
							</select>
							@if($errors->has('address_2_populated_place'))
								<span>{{ $errors->first('address_2_populated_place') }}</span>
							@endif
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-postcode">Пощенски код</label>
							<input type="text" class="form-control" id="address-2-postcode" name="address_2_postcode" value="{{ old('address_2_postcode') }}">
							@if($errors->has('address_2_postcode'))
								<span>{{ $errors->first('address_2_postcode') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-quarter">Квартал</label>
							<input type="text" class="form-control" id="address-2-quarter" name="address_2_quarter" value="{{ old('address_2_quarter') }}">
							@if($errors->has('address_2_quarter'))
								<span>{{ $errors->first('address_2_quarter') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-street">Улица/Булевард</label>
							<input type="text" class="form-control" id="address-2-street" name="address_2_street" value="{{ old('address_2_street') }}">
							@if($errors->has('address_2_street'))
								<span>{{ $errors->first('address_2_street') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-street-number">Улица/Булевард - номер</label>
							<input type="text" class="form-control" id="address-2-street-number" name="address_2_street_number" value="{{ old('address_2_street_number') }}">
							@if($errors->has('address_2_street_number'))
								<span>{{ $errors->first('address_2_street_number') }}</span>
							@endif
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-building">Блок</label>
							<input type="text" class="form-control" id="address-2-building" name="address_2_building" value="{{ old('address_2_building') }}">
							@if($errors->has('address_2_building'))
								<span>{{ $errors->first('address_2_building') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-entrance">Вход</label>
							<input type="text" class="form-control" id="address-2-entrance" name="address_2_entrance" value="{{ old('address_2_entrance') }}">
							@if($errors->has('address_2_entrance'))
								<span>{{ $errors->first('address_2_entrance') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-floor">Етаж</label>
							<input type="text" class="form-control" id="address-2-floor" name="address_2_floor" value="{{ old('address_2_floor') }}">
							@if($errors->has('address_2_floor'))
								<span>{{ $errors->first('address_2_floor') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="address-2-apartment">Апартамент</label>
							<input type="text" class="form-control" id="address-2-apartment" name="address_2_apartment" value="{{ old('address_2_apartment') }}">
							@if($errors->has('address_2_apartment'))
								<span>{{ $errors->first('address_2_apartment') }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</transition>

		<div class="drag">
            <h4 class="text-center">Предмет на дейност</h4>
            <draggable :list="activities" class="dragArea">
            	<div v-for="(activity, index) in activities" class="col-md-4">
            		<div class="company-activity">
            			<input type="hidden" :name="'activities['+index+']'" :value="activity.id">
            			@{{activity.name}}
            			<i class="fa fa-close" @click="removeActivity(index)"></i>
            		</div>
            	</div>
             </draggable>
             <div class="clearfix"></div>
         </div>

		<div class="gap-20"></div>

		<div class="row">
		    <div class="col-md-12">
		        <div class="form-group">
		            <label for="additional_activities">Допълнителни дейности (разделени със запетая)</label>
		            <textarea class="form-control" name="additional_activities" id="additional_activities" rows="5">{{ old('additional_activities') }}</textarea>
		        </div>
		    </div>
		</div> 

		{{-- Money --}}
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<label for="money">Капитал</label>
					<input type="number" class="form-control" id="money" name="money" min="2" value="{{ old('money') }}">
					@if($errors->has('money'))
						<span>{{ $errors->first('money') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Owners --}}
		<transition-group name="fade" tag="div">
			<div v-for="(owner, index) in owners" :key="index">
				<div class="row">
					<h4 class="text-center">Информация за собственика</h4>

					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-firstname'">Име</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-firstname'" :name="'owner['+index+'][firstname]'" v-model="owners[index].firstname">
							@if($errors->has('owner.*.firstname'))
								<span>{{ $errors->first('owner.*.firstname') }}</span>
							@endif
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-secondname'">Презиме</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-secondname'" :name="'owner['+index+'][secondname]'" v-model="owners[index].secondname">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-lastname'">Фамилия</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-lastname'" :name="'owner['+index+'][lastname]'" v-model="owners[index].lastname">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-pin'">ЕГН</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-pin'" :name="'owner['+index+'][pin]'" v-model="owners[index].pin">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-id-number'">Номер на лична карта</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-id-number'" :name="'owner['+index+'][id_number]'" v-model="owners[index].id_number">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="'owner-'+index+'-id-created-at'">Дата на издаване</label>
							<input type="date" class="form-control" :id="'owner-'+index+'-id-created-at'" :name="'owner['+index+'][id_date_created]'" v-model="owners[index].id_date_created">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-id-created-by'">Издадена от</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-id-created-by'" :name="'owner['+index+'][id_created_by]'" v-model="owners[index].id_created_by">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label :for="'owner-'+index+'-money'">Дялово учасите</label>
							<input type="text" class="form-control" :id="'owner-'+index+'-money'" :name="'owner['+index+'][money]'" v-model="owners[index].money">
						</div>
					</div>
				</div>
				<div class="row" v-if="index > 1">
					<div class="col-md-4 col-md-offset-4 text-center">
						<button class="btn btn-danger btn-has-icon" type="button" @click="removeOwner(index)"><i class="fa fa-minus"></i> Премахни</button>
					</div>
				</div>
				<div class="gap-20"></div>
			</div>
		</transition-group>

		{{-- Add owner --}}
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-primary btn-has-icon" type="button" @click="addOwner"><i class="fa fa-plus"></i> Добави съдружник</button>
			</div>
		</div>

		<div class="gap-20"></div>

		{{-- Managers --}}
		<div class="row">
			<h4 class="text-center">Управители</h4>

			<div class="col-md-4 col-md-offset-4 text-center">
				<div v-for="(owner, index) in owners" class="checkbox checkbox__custom checkbox__style1">
					<label v-if="owner.firstname && owner.secondname && owner.lastname">
						<input type="checkbox" :name="'owner['+index+'][manager]'" v-model="owners[index].manager">
						<span>@{{ owner.firstname + ' ' + owner.secondname + ' ' + owner.lastname }}</span>
					</label>
				</div>
			</div>
		</div>

		<div v-for="(manager, index) in managers">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label :for="'manager-'+index+'firstname'">Име</label>
						<input type="text" class="form-control" :id="'manager-'+index+'firstname'" :name="'manager['+index+'][firstname]'" v-model="managers[index].firstname">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label :for="'manager-'+index+'secondname'">Презиме</label>
						<input type="text" class="form-control" :id="'manager-'+index+'secondname'" :name="'manager['+index+'][secondname]'" v-model="managers[index].secondname">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label :for="'manager-'+index+'lastname'">Фамилия</label>
						<input type="text" class="form-control" :id="'manager-'+index+'lastname'" :name="'manager['+index+'][lastname]'" v-model="managers[index].lastname">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label :for="'manager-'+index+'pin'">ЕГН</label>
						<input type="text" class="form-control" :id="'manager-'+index+'pin'" :name="'manager['+index+'][pin]'" v-model="managers[index].pin">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="'manager-'+index+'-id-number'">Номер на лична карта</label>
						<input type="text" class="form-control" :id="'manager-'+index+'-id-number'" :name="'manager['+index+'][id_number]'" v-model="managers[index].id_number">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="'manager-'+index+'-id-created-at'">Дата на издаване</label>
						<input type="date" class="form-control" :id="'manager-'+index+'-id-created-at'" :name="'manager['+index+'][id_date_created]'" v-model="managers[index].id_date_created">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="'manager-'+index+'-id-created-by'">Издадена от</label>
						<input type="text" class="form-control" :id="'manager-'+index+'-id-created-by'" :name="'manager['+index+'][id_created_by]'" v-model="managers[index].id_created_by">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4 text-center">
					<button class="btn btn-danger btn-has-icon" type="button" @click="removeManager(index)"><i class="fa fa-minus"></i> Премахни</button>
				</div>
			</div>
			<div class="gap-20"></div>
		</div>

		{{-- Add manager --}}
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-primary btn-has-icon" type="button" @click="addManager"><i class="fa fa-plus"></i> Добави управител</button>
			</div>
		</div>

		{{-- Representation method --}}
		<transition name="fade">
			<div v-if="managerCount > 1">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 text-center">
						<div class="radio radio__custom radio__style1">
							<label>
								<input type="radio" name="representation_method" id="representation-method" value="0" checked>
								<span>Заедно и поотделно</span>
							</label>
						</div>
						<div class="radio radio__custom radio__style1">
							<label>
								<input type="radio" name="representation_method" id="representation-method-together" value="1">
								<span>Заедно</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</transition>

		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<h3 class="text-center">Начин на плащане</h3>
				<div class="radio radio__custom radio__style1">
					<label>
						<input type="radio" name="payment_method" id="office" value="office" checked>
						<span>На място в офиса</span>
					</label>
				</div>
				<div class="radio radio__custom radio__style1">
					<label>
						<input type="radio" name="payment_method" id="econt" value="econt">
						<span>Еконт</span>
					</label>
				</div>
			</div>
		</div>

		<div class="row">
			<h3 class="text-center">Вашата информация</h3>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_firstname">Име</label>
					<input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
					@if($errors->has('payer_firstname'))
						<span>{{ $errors->first('payer_firstname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_lastname">Фамилия</label>
					<input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
					@if($errors->has('payer_lastname'))
						<span>{{ $errors->first('payer_lastname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_email">Email</label>
					<input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
					@if($errors->has('payer_email'))
						<span>{{ $errors->first('payer_email') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="payer_phone">Телефон</label>
					<input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
					@if($errors->has('payer_phone'))
						<span>{{ $errors->first('payer_phone') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Submit --}}
		<div class="gap-10"></div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
			</div>
		</div>
	</form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdn.jsdelivr.net/npm/sortablejs@1.7.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.15.0/vuedraggable.min.js"></script>

<script>
	let app = new Vue({
		el: '#app',
		data: {
			currentZone: {{ old('zone') ? old('zone') : 1 }},
			currentZone2: {{ old('address_2_zone') ? old('address_2_zone') : 1 }},
			currentTownship: {{ old('township') ? old('township') : 1 }},
			currentTownship2: {{ old('address_2_township') ? old('address_2_township') : 1 }},
			currentPopulatedPlace: {{ old('populated_place') ? old('populated_place') : 1 }},
			currentPopulatedPlace2: {{ old('address_2_populated_place') ? old('address_2_populated_place') : 1 }},
			zones: [],
			zones2: [],
			townships: [],
			townships2: [],
			populated_places: [],
			populated_places2: [],
			different_address: {{ old('different_address') !== null ? old('different_address') : 'false' }},
			activities: [],
			@if(old('owner'))
				owners: JSON.parse('{!! json_encode(old('owner'), JSON_UNESCAPED_UNICODE) !!}'),
			@else
				owners: [
					{firstname: '', secondname: '', lastname: '', pin: '', id_number: '', id_date_created: '', id_created_by: '', money: '', manager: true},
					{firstname: '', secondname: '', lastname: '', pin: '', id_number: '', id_date_created: '', id_created_by: '', money: '', manager: true}
				],
			@endif

			@if(old('manager'))
				managers: JSON.parse('{!! json_encode(old('manager'), JSON_UNESCAPED_UNICODE) !!}')
			@else
				managers: []
			@endif
		},
		created: function() {
			this.getZones(),
			this.getTownships(),
			this.getZones2(),
			this.getTownships2(),
			this.getActivities()
		},
		computed: {
			managerCount: function() {
				let managerCount = 0

				for(owner of this.owners) {
					if (owner.manager) {
						managerCount++
					}
				}

				managerCount += this.managers.length

				return managerCount
			}
		},
		methods: {
			getZones: function() {
				let app = this

				axios.get('{{ route('zones-json') }}')
					.then(function(response) {
						app.zones = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getTownships: function() {
				let app = this

				axios.get('/api/zone/'+this.currentZone+'/townships')
					.then(function(response) {
						app.townships = response.data
						app.currentTownship = response.data[0].id

						app.getPopulatedPlaces()
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getPopulatedPlaces: function() {
				let app = this

				axios.get('/api/township/'+this.currentTownship+'/populated-places')
					.then(function(response) {
						app.populated_places = response.data
						app.currentPopulatedPlace = response.data[0].id
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getZones2: function() {
				let app = this

				axios.get('{{ route('zones-json') }}')
					.then(function(response) {
						app.zones2 = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getTownships2: function() {
				let app = this

				axios.get('/api/zone/'+this.currentZone2+'/townships')
					.then(function(response) {
						app.townships2 = response.data
						app.currentTownship2 = response.data[0].id

						app.getPopulatedPlaces2()
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getPopulatedPlaces2: function() {
				let app = this

				axios.get('/api/township/'+this.currentTownship2+'/populated-places')
					.then(function(response) {
						app.populated_places2 = response.data
						app.currentPopulatedPlace2 = response.data[0].id
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			addOwner: function() {
				let owner = {firstname: '', secondname: '', lastname: '', pin: '', id_number: '', id_date_created: '', id_created_by: '', money: '', manager: true}

				this.owners.push(owner)
			},
			removeOwner: function(index) {
				this.owners.splice(index, 1)
			},
			addManager: function() {
				let manager = {firstname: '', secondname: '', lastname: '', pin: '', id_number: '', id_date_created: '', id_created_by: ''}

				this.managers.push(manager)
			},
			removeManager: function(index) {
				// console.log(this.managers[index])
				this.managers.splice(index, 1)
			},
			getActivities: function() {
				let app = this

				axios.get('{{ route('get-activities') }}')
					.then(function(response) {
						app.activities = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			removeActivity: function(index) {
				console.log('remove activity ' + index)

				this.activities.splice(index, 1)
			}
		}
	})
</script>

<style>
	.fade-enter-active, .fade-leave-active {
	  transition: opacity .4s
	}
	.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
	  opacity: 0
	}
	.company-activity {
		color: black;
		background: #F9F9F9;
		padding: 10px;
		border: 1px solid #F3F3F3;
	}
	.company-activity .fa {
		display: inline-block;
		float: right;
		cursor: pointer;
	}
</style>
@endsection