@extends('layouts.main')

@section('title', 'Брачен договор')
@section('page_title', 'Брачен договор')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
    <h3>{{ $settings->text }}</h3>
</div>

<div id="app">
    <form action="" method="POST">
        {{ csrf_field() }}

        {{-- Husband --}}
        <div class="row">
            <h4 class="text-center">Лични данни на съпруга</h4>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="husband_firstname">Име</label>
                    <input type="text" class="form-control" id="husband_firstname" name="husband_firstname" value="{{ old('husband_firstname') }}">
                    @if($errors->has('husband_firstname'))
                        <span>{{ $errors->first('husband_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="husband_secondname">Презиме</label>
                    <input type="text" class="form-control" id="husband_secondname" name="husband_secondname" value="{{ old('husband_secondname') }}">
                    @if($errors->has('husband_secondname'))
                        <span>{{ $errors->first('husband_secondname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="husband_lastname">Име</label>
                    <input type="text" class="form-control" id="husband_lastname" name="husband_lastname" value="{{ old('husband_lastname') }}">
                    @if($errors->has('husband_lastname'))
                        <span>{{ $errors->first('husband_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="husband_pin">ЕГН</label>
                    <input type="text" class="form-control" id="husband_pin" name="husband_pin" value="{{ old('husband_pin') }}">
                    @if($errors->has('husband_pin'))
                        <span>{{ $errors->first('husband_pin') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="husband_id_number">Номер на лична карта</label>
                    <input type="text" class="form-control" id="husband_id_number" name="husband_id_number" value="{{ old('husband_id_number') }}">
                    @if($errors->has('husband_id_number'))
                        <span>{{ $errors->first('husband_id_number') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="husband_id_date_created">Дата на издаване</label>
                    <input type="date" class="form-control" id="husband_id_date_created" name="husband_id_date_created" value="{{ old('husband_id_date_created') }}">
                    @if($errors->has('husband_id_date_created'))
                        <span>{{ $errors->first('husband_id_date_created') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="husband_id_created_by">Издадена от</label>
                    <input type="text" class="form-control" id="husband_id_created_by" name="husband_id_created_by" value="{{ old('husband_id_created_by') }}">
                    @if($errors->has('husband_id_created_by'))
                        <span>{{ $errors->first('husband_id_created_by') }}</span>
                    @endif
                </div>
            </div>
        </div>

        {{-- Wife --}}
        <div class="row">
            <h4 class="text-center">Лични данни на съпругата</h4>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="wife_firstname">Име</label>
                    <input type="text" class="form-control" id="wife_firstname" name="wife_firstname" value="{{ old('wife_firstname') }}">
                    @if($errors->has('wife_firstname'))
                        <span>{{ $errors->first('wife_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="wife_secondname">Презиме</label>
                    <input type="text" class="form-control" id="wife_secondname" name="wife_secondname" value="{{ old('wife_secondname') }}">
                    @if($errors->has('wife_secondname'))
                        <span>{{ $errors->first('wife_secondname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="wife_lastname">Име</label>
                    <input type="text" class="form-control" id="wife_lastname" name="wife_lastname" value="{{ old('wife_lastname') }}">
                    @if($errors->has('wife_lastname'))
                        <span>{{ $errors->first('wife_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="wife_pin">ЕГН</label>
                    <input type="text" class="form-control" id="wife_pin" name="wife_pin" value="{{ old('wife_pin') }}">
                    @if($errors->has('wife_pin'))
                        <span>{{ $errors->first('wife_pin') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="wife_id_number">Номер на лична карта</label>
                    <input type="text" class="form-control" id="wife_id_number" name="wife_id_number" value="{{ old('wife_id_number') }}">
                    @if($errors->has('wife_id_number'))
                        <span>{{ $errors->first('wife_id_number') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="wife_id_date_created">Дата на издаване</label>
                    <input type="date" class="form-control" id="wife_id_date_created" name="wife_id_date_created" value="{{ old('wife_id_date_created') }}">
                    @if($errors->has('wife_id_date_created'))
                        <span>{{ $errors->first('wife_id_date_created') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="wife_id_created_by">Издадена от</label>
                    <input type="text" class="form-control" id="wife_id_created_by" name="wife_id_created_by" value="{{ old('wife_id_created_by') }}">
                    @if($errors->has('wife_id_created_by'))
                        <span>{{ $errors->first('wife_id_created_by') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="gap-10"></div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="form-group">
                    <select name="mariage_step" id="mariage_step" class="form-control" v-model="mariage_step">
                        <option value="" selected>--</option>
                        <option value="1">Бракът предстои да бъде сключен</option>
                        <option value="2">Бракът вече е сключен</option>
                    </select>
                </div>
            </div>
        </div>

        <transition name="fade" mode="out-in">
            <div v-if="mariage_step == 1" key="1">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="marriage_future_date">Бракът ще бъде сключен на</label>
                            <input type="date" class="form-control" id="marriage_future_date" name="marriage_future_date" value="{{ old('marriage_future_date') }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_dispose_property_freely">
                                <span>Всеки един от съпрузите се разпорежда свободно с личното си имущество</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_no_property_right">
                                <span>Съпрузите нямат право на собственост и/или претенции за дял във връзка с недвижимо имущество на другия съпруг</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_divisive_duties">
                                <span>Съпрузите поемат задължения в режим на разделност – съпругът не отговаря за задълженията (кредити, заеми, обезщетения) на другия съпруг</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_property_acquired_during_marriage">
                                <span>Имуществото, придобито от всеки един от съпрузите по време на брака е негова лична собственост</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_inheritance_property">
                                <span>Придобитото по наследство и/или дарение е лична собственост на съпруга и другият съпруг не може да има претенции</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="future_spouses_no_right_claim_part_property">
                                <span>Съпрузите нямат право да претендират част от стойността на придобитото от другия по време на брака</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div v-else-if="mariage_step == 2" key="2">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="form-group">
                            <label for="marriage_past_date">Бракът е бил сключен на</label>
                            <input type="date" class="form-control" id="marriage_past_date" name="marriage_past_date" value="{{ old('marriage_past_date') }}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="past_acquired_estate">
                                <span>От брака има придобит недвижим имот</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="past_acquired_vehicle">
                                <span>От брака има придобито МПС</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="past_spouses_dispose_property_freely">
                                <span>Всеки един от съпрузите се разпорежда свободно с личното си имущество</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="past_spouses_no_right_claim_part_property">
                                <span>Съпрузите нямат право на собственост и/или претенции за дял във връзка с недвижимо имущество на другия съпруг</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="no_duties">
                                <span>Съпрузите поемат задължения в режим на разделност – съпругът не отговаря за задълженията (кредити, заеми, обезщетения) на другия съпруг</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="own_property">
                                <span>Имуществото, придобито от всеки един от съпузите по време на брака е негова лична собственост</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="no_heritage_claims">
                                <span>Придобитото по наследство и/или дарение е лична собственост на съпруга и другият съпруг не може да има претенции</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox__custom checkbox__style1">
                            <label>
                                <input type="checkbox" name="part_of_ownership_claim">
                                <span>Съпрузите нямат право да претендират част от стойността на придобитото от другия по време на брака</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </transition>

        <div class="row">
            <h4 class="text-center">Допълнителни клаузи</h4>

            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control" name="additional_clauses" rows="8"></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <h4 class="text-center">Лични данни на заявителя</h4>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_firstname">Име</label>
                    <input type="text" class="form-control" id="owner_firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
                    @if($errors->has('owner_firstname'))
                        <span>{{ $errors->first('owner_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_secondname">Презиме</label>
                    <input type="text" class="form-control" id="owner_secondname" name="owner_secondname" value="{{ old('owner_secondname') }}">
                    @if($errors->has('owner_secondname'))
                        <span>{{ $errors->first('owner_secondname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="owner_lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
                    @if($errors->has('owner_lastname'))
                        <span>{{ $errors->first('owner_lastname') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_pin">ЕГН</label>
                    <input type="text" class="form-control" id="owner_pin" name="owner_pin" value="{{ old('owner_pin') }}">
                    @if($errors->has('owner_pin'))
                        <span>{{ $errors->first('owner_pin') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_phone">Телефон</label>
                    <input type="tel" class="form-control" id="owner_phone" name="owner_phone" value="{{ old('owner_phone') }}">
                    @if($errors->has('owner_phone'))
                        <span>{{ $errors->first('owner_phone') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="owner_email">Email</label>
                    <input type="email" class="form-control" id="owner_email" name="owner_email" value="{{ old('owner_email') }}">
                    @if($errors->has('owner_email'))
                        <span>{{ $errors->first('owner_email') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="text-center">Начин на плащане</h3>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="office" value="office" checked>
                        <span>На място в офиса</span>
                    </label>
                </div>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="econt" value="econt">
                        <span>Еконт</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <h3 class="text-center">Вашата информация</h3>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_firstname">Име</label>
                    <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                    @if($errors->has('payer_firstname'))
                        <span>{{ $errors->first('payer_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                    @if($errors->has('payer_lastname'))
                        <span>{{ $errors->first('payer_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_email">Email</label>
                    <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                    @if($errors->has('payer_email'))
                        <span>{{ $errors->first('payer_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_phone">Телефон</label>
                    <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                    @if($errors->has('payer_phone'))
                        <span>{{ $errors->first('payer_phone') }}</span>
                    @endif
                </div>
            </div>
        </div>

        {{-- Submit --}}
        <div class="gap-10"></div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">
                <button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
            </div>
        </div>
    </form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    let app = new Vue({
        el: '#app',
        data: {
            mariage_step: null
        }
    })
</script>

<style>
    .fade-enter-active, .fade-leave-active {
      transition: opacity .4s
    }
    .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
      opacity: 0
    }
</style>
@endsection