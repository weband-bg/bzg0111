@extends('layouts.main')

@section('title', 'Има ли тежести върху имота ми ?')
@section('page_title', 'Има ли тежести върху имота ми ?')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
	<div class="alert alert-success alert-dismissible fade in text-uppercase">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
		<strong>{{ Session::get('success') }}</strong>
	</div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
	<strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
	<h3>{{ $settings->text }}</h3>
</div>

<div id="app">
	<form action="{{ route('store-property-weight') }}" method="POST">
		{{csrf_field()}}

		<div class="row">
			<div class="col-md-12">
				<h4 class="text-center">Вие сте</h4>

				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="radio radio__custom radio__style1">
						<label>
							<input type="radio" name="type" id="individual" value="individual" v-model="type">
							<span>Физическо лице</span>
						</label>
					</div>
					<div class="radio radio__custom radio__style1">
						<label>
							<input type="radio" name="type" id="legal-entity" value="legal-entity" v-model="type">
							<span>Юридическо лице</span>
						</label>
					</div>
					@if($errors->has('type'))
						<span>{{ $errors->first('type') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Individual --}}
		<transition name="fade" mode="out-in">
			<div class="row" v-if="type == 'individual'" key="individual">
				<div class="col-md-3">
					<div class="form-group">
						<label for="firstname">Име</label>
						<input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}">
						@if($errors->has('firstname'))
							<span>{{ $errors->first('firstname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="secondname">Презиме</label>
						<input type="text" class="form-control" id="secondname" name="secondname" value="{{ old('secondname') }}">
						@if($errors->has('secondname'))
							<span>{{ $errors->first('secondname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="lastname">Фамилия</label>
						<input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}">
						@if($errors->has('lastname'))
							<span>{{ $errors->first('lastname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="pin">ЕГН</label>
						<input type="text" class="form-control" id="pin" name="pin" value="{{ old('pin') }}">
						@if($errors->has('pin'))
							<span>{{ $errors->first('pin') }}</span>
						@endif
					</div>
				</div>
			</div>

		{{-- Legal entity --}}
			<div class="row" v-else-if="type == 'legal-entity'" key="legal-entity">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name">Име</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
						@if($errors->has('name'))
							<span>{{ $errors->first('name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="eik">ЕИК</label>
						<input type="text" class="form-control" id="eik" name="eik" value="{{ old('eik') }}">
						@if($errors->has('eik'))
							<span>{{ $errors->first('eik') }}</span>
						@endif
					</div>
				</div>
			</div>
		</transition>

		{{-- Address --}}
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label for="zone">Област</label>
					<select class="form-control" id="zone" name="zone" v-model="currentZone" @change="getTownships">
						<option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>
					</select>
					@if($errors->has('zone'))
						<span>{{ $errors->first('zone') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="township">Община</label>
					<select class="form-control" id="township" name="township" v-model="currentTownship" @change="getPopulatedPlaces">
						<option v-for="township in townships" :value="township.id">@{{ township.name }}</option>
					</select>
					@if($errors->has('township'))
						<span>{{ $errors->first('township') }}</span>
					@endif	
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="populated_place">Населено място</label>
					<select class="form-control" id="populated_place" name="populated_place" v-model="currentPopulatedPlace">
						<option v-for="populated_place in populated_places" :value="populated_place.id">@{{ populated_place.name }}</option>
					</select>
					@if($errors->has('populated_place'))
						<span>{{ $errors->first('populated_place') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="postcode">Пощенски код</label>
					<input type="text" class="form-control" id="postcode" name="postcode" required value="{{ old('postcode') }}">
					@if($errors->has('postcode'))
						<span>{{ $errors->first('postcode') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="quarter">Квартал</label>
					<input type="text" class="form-control" id="quarter" name="quarter" required value="{{ old('quarter') }}">
					@if($errors->has('quarter'))
						<span>{{ $errors->first('quarter') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street">Улица/Булевард</label>
					<input type="text" class="form-control" id="street" name="street" required value="{{ old('street') }}">
					@if($errors->has('street'))
						<span>{{ $errors->first('street') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="street-number">Улица/Булевард - номер</label>
					<input type="text" class="form-control" id="street-number" name="street_number" required value="{{ old('street_number') }}">
					@if($errors->has('street_number'))
						<span>{{ $errors->first('street_number') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="building">Блок</label>
					<input type="text" class="form-control" id="building" name="building" required value="{{ old('building') }}">
					@if($errors->has('building'))
						<span>{{ $errors->first('building') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="entrance">Вход</label>
					<input type="text" class="form-control" id="entrance" name="entrance" required value="{{ old('entrance') }}">
					@if($errors->has('entrance'))
						<span>{{ $errors->first('entrance') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="floor">Етаж</label>
					<input type="text" class="form-control" id="floor" name="floor" required value="{{ old('floor') }}">
					@if($errors->has('floor'))
						<span>{{ $errors->first('floor') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="apartment">Апартамент</label>
					<input type="text" class="form-control" id="apartment" name="apartment" required value="{{ old('apartment') }}">
					@if($errors->has('apartment'))
						<span>{{ $errors->first('apartment') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Owner info --}}
		<div class="row">
			<h4 class="text-center">Информация за собственика</h4>

			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_firstname">Име</label>
					<input type="text" class="form-control" id="owner_firstname" name="owner_firstname" required value="{{ old('owner_firstname') }}">
					@if($errors->has('owner_firstname'))
						<span>{{ $errors->first('owner_firstname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_secondname">Презиме</label>
					<input type="text" class="form-control" id="owner_secondname" name="owner_secondname" required value="{{ old('owner_secondname') }}">
					@if($errors->has('owner_secondname'))
						<span>{{ $errors->first('owner_secondname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_lastname">Фамилия</label>
					<input type="text" class="form-control" id="owner_lastname" name="owner_lastname" required value="{{ old('owner_lastname') }}">
					@if($errors->has('owner_lastname'))
						<span>{{ $errors->first('owner_lastname') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label for="owner_pin">ЕГН</label>
					<input type="text" class="form-control" id="owner_pin" name="owner_pin" required value="{{ old('owner_pin') }}">
					@if($errors->has('owner_pin'))
						<span>{{ $errors->first('owner_pin') }}</span>
					@endif
				</div>
			</div>
		</div>

		<div class="row">
		    <div class="col-md-4 col-md-offset-4">
		        <h3 class="text-center">Начин на плащане</h3>
		        <div class="radio radio__custom radio__style1">
		            <label>
		                <input type="radio" name="payment_method" id="office" value="office" checked>
		                <span>На място в офиса</span>
		            </label>
		        </div>
		        <div class="radio radio__custom radio__style1">
		            <label>
		                <input type="radio" name="payment_method" id="econt" value="econt">
		                <span>Еконт</span>
		            </label>
		        </div>
		    </div>
		</div>

		<div class="row">
		    <h3 class="text-center">Вашата информация</h3>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_firstname">Име</label>
		            <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
		            @if($errors->has('payer_firstname'))
		                <span>{{ $errors->first('payer_firstname') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_lastname">Фамилия</label>
		            <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
		            @if($errors->has('payer_lastname'))
		                <span>{{ $errors->first('payer_lastname') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_email">Email</label>
		            <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
		            @if($errors->has('payer_email'))
		                <span>{{ $errors->first('payer_email') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_phone">Телефон</label>
		            <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
		            @if($errors->has('payer_phone'))
		                <span>{{ $errors->first('payer_phone') }}</span>
		            @endif
		        </div>
		    </div>
		</div>

		{{-- Submit --}}
		<div class="gap-10"></div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
			</div>
		</div>
	</form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
	let app = new Vue({
		el: '#app',
		data: {
			type: '{{ old('type') }}',
			currentZone: {{ old('zone') ? old('zone') : 1 }},
			currentTownship: {{ old('township') ? old('township') : 1 }},
			currentPopulatedPlace: {{ old('populated_place') ? old('populated_place') : 1 }},
			zones: [],
			townships: [],
			populated_places: []
		},
		created: function() {
			this.getZones(),
			this.getTownships()
		},
		methods: {
			getZones: function() {
				let app = this

				axios.get('{{ route('zones-json') }}')
					.then(function(response) {
						app.zones = response.data
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getTownships: function() {
				let app = this

				axios.get('/api/zone/'+this.currentZone+'/townships')
					.then(function(response) {
						app.townships = response.data
						app.currentTownship = response.data[0].id

						app.getPopulatedPlaces()
					})
					.catch(function(error) {
						console.log(error)
					})
			},
			getPopulatedPlaces: function() {
				let app = this

				axios.get('/api/township/'+this.currentTownship+'/populated-places')
					.then(function(response) {
						app.populated_places = response.data
						app.currentPopulatedPlace = response.data[0].id
					})
					.catch(function(error) {
						console.log(error)
					})
			}
		}
	})
</script>

<style>
	.fade-enter-active, .fade-leave-active {
	  transition: opacity .4s
	}
	.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
	  opacity: 0
	}
</style>
@endsection