@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Проверка на недвижими имоти на лице')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Проверка на недвижими имоти на лице</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>Тип</th>
			        	<th>Име</th>
			        	<th>ЕГН/ЕИК</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($owner_estates as $owner_estate)
						<tr id="{{ $owner_estate->id }}">
							<td>{{ $owner_estate->getType() }}</td>
							<td>{{ $owner_estate->getNames() }}</td>
							<td>{{ $owner_estate->getPINEIK() }}</td>
							<td>
								<a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $owner_estate->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-owner-estate') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection