@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Промяна на предемта на дейност')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Промяна на предемта на дейност</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Име</th>
                        <th>ЕИК</th>
                        <th>Нов предмет на дейност</th>
                        <th>Информация за клиент</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($changes as $change)
                        <tr id="{{ $change->id }}">
                            <td>{{ $change->company_name }}</td>
                            <td>{{ $change->eik }}</td>
                            <td>{{ $change->activities->implode('name', ', ') }}</td>
                            <td>
                                Име: {{ $change->getOwnerNames() }}
                                <br>
                                E-мейл: {{ $change->owner_email }}
                                <br>
                                Дата на валидност на лична карта: {{ $change->owner_id_expiry_date }}
                                <br>
                                Телефон: {{ $change->owner_phone }}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-info"
                                   data-id="{{ $change->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-info').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-company-activity-subject') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('tr#' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection
