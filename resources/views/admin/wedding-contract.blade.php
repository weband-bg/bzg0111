@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Договор за продажба на уеб сайт')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Договор за продажба на уеб сайт</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Съпруг</th>
                        <th>Съпруга</th>
                        <th>Брак</th>
                        <th>Допълнителни клаузи</th>
                        <th>Информация за клиент</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contracts as $contract)
                        <tr id="wedding-contract-{{ $contract->id }}">
                            <td>{{ $contract->id }}</td>
                            <td>
                                Име: {{ $contract->getHusbandNames() }}
                                <br>
                                ЕГН: {{ $contract->husband_pin }}
                                <br>
                                Номер на лична карта: {{ $contract->husband_id_number }}
                                <br>
                                Дата на издаване: {{ $contract->husband_id_date_created }}
                                <br>
                                Издадена от: {{ $contract->husband_id_created_by }}
                            </td>
                            <td>
                                Име: {{ $contract->getWifeNames() }}
                                <br>
                                ЕГН: {{ $contract->wife_pin }}
                                <br>
                                Номер на лична карта: {{ $contract->wife_id_number }}
                                <br>
                                Дата на издаване: {{ $contract->wife_id_date_created }}
                                <br>
                                Издадена от: {{ $contract->wife_id_created_by }}
                            </td>
                            <td>
                                Стъпка: {{ $contract->getMarriageStep() }}
                                <br>
                                Бракът ще бъде сключен на: {{ $contract->marriage_future_date }}
                                <br>
                                Бракът е бил сключен на: {{ $contract->marriage_past_date }}
                                <br>
                                @if($contract->marriage_step == 1)
                                    @if($contract->future_spouses_dispose_property_freely == 1)
                                        Всеки един от съпрузите се разпорежда
                                        свободно с личното си имущество
                                        <br>
                                    @endif

                                    @if($contract->future_spouses_no_property_right == 1)
                                        Съпрузите нямат право на собственост и/или
                                        претенции за дял във връзка с недвижимо имущество на другия съпруг
                                        <br>
                                    @endif

                                    @if($contract->future_spouses_divisive_duties == 1)
                                        Съпрузите поемат задължения в режим на
                                        разделност – съпругът не отговаря за задълженията (кредити, заеми, обезщетения)
                                        на другия съпруг
                                        <br>
                                    @endif

                                    @if($contract->future_spouses_property_acquired_during_marriage == 1)
                                        Имуществото, придобито от всеки един от
                                        съпузите по време на брака е негова лична собственост.
                                        <br>
                                    @endif

                                    @if($contract->future_spouses_inheritance_property == 1)
                                        Придобитото по наследство и/или дарение е
                                        лична собственост на съпруга и другият съпруг не може да има претенции.
                                        <br>
                                    @endif

                                    @if($contract->future_spouses_no_right_claim_part_property == 1)
                                        Съпрузите нямат право да претендират част
                                        от стойността на придобитото от другия по време на брака.
                                        <br>
                                    @endif

                                @else
                                    @if($contract->past_acquired_estate == 1)
                                        От брака има придобит недвижим имот
                                        <br>
                                    @endif

                                    @if($contract->past_acquired_vehicle == 1)
                                        От брака има придобито МПС
                                        <br>
                                    @endif

                                    @if($contract->past_spouses_dispose_property_freely == 1)
                                        Всеки един от съпрузите се разпорежда
                                        свободно с личното си имущество
                                        <br>
                                    @endif

                                    @if($contract->past_spouses_no_right_claim_part_property == 1)
                                        Съпрузите нямат право на собственост и/или
                                        претенции за дял във връзка с недвижимо имущество на другия съпруг
                                        <br>
                                    @endif
                                @endif

                            </td>
                            <td>{{ $contract->additional_clauses }}</td>


                            <td>
                                Име: {{ $contract->getClientNames() }}
                                <br>
                                E-мейл: {{ $contract->owner_email }}
                                <br>
                                Дата на валидност на лична карта: {{ $contract->owner_id_expiry_date }}
                                <br>
                                Телефон: {{ $contract->owner_phone }}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-wedding-contract" href="#"
                                   data-id="{{ $contract->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-wedding-contract').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-wedding-contract') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('#wedding-contract-' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection