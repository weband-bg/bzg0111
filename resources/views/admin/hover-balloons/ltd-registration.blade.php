@extends('layouts.admin')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="row">
        <div class="col-md-12">
          <div class="well bs-component">
            <form class="form-horizontal" method="POST" action="{{ route('store-ltd-hover-ballons') }}">
            	{{csrf_field()}}
              <fieldset>
                <legend>Ховър балони</legend>
                <div class="form-group">
                  <label class="col-md-2 control-label" for="company-name">Име на фирмата</label>
                  <div class="col-md-8">
                    <textarea name="company-name" id="company-name" class="form-control" rows="5">{{ $hovers['company-name'] }}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-2 control-label" for="address">Адрес</label>
                  <div class="col-md-8">
                   	<textarea name="address" id="address" class="form-control" rows="5">{{ $hovers['address'] }}</textarea>
                  </div>
                </div>
	            <div class="form-group">
	              <label class="col-md-2 control-label" for="different-address">Желая да предоставя адрес за кореспонденция с НАП, който е различен от адреса на управление</label>
	              <div class="col-md-8">
	               	<textarea name="different-address" id="different-address" class="form-control" rows="5">{{ $hovers['different-address'] }}</textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-md-2 control-label" for="money">Капитал</label>
	              <div class="col-md-8">
	               	<textarea name="money" id="money" class="form-control" rows="5">{{ $hovers['money'] }}</textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-md-2 control-label" for="owner">Собственик на фирмата</label>
	              <div class="col-md-8">
	               	<textarea name="owner" id="owner" class="form-control" rows="5">{{ $hovers['owner'] }}</textarea>
	              </div>
	            </div>
	            <div class="form-group">
	              <label class="col-md-2 control-label" for="manager">Управител</label>
	              <div class="col-md-8">
	               	<textarea name="manager" id="manager" class="form-control" rows="5">{{ $hovers['manager'] }}</textarea>
	              </div>
	            </div>
                <div class="form-group">
                  <div class="col-md-8 col-md-offset-2">
                    <button class="btn btn-primary" type="submit">Запиши</button>
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection