@extends('layouts.admin')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="row">
                <div class="col-md-12">
                    <div class="well bs-component">
                        <form  method="POST" action="">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="operation">Модул</label>
                                <select name="module" id="module" class="form-control" disabled>
                                    @foreach($modules as $module)
                                        @if($module->id == $hover_balloon->module_id)
                                            <option value="{{ $module->id }}" selected>{{ $module->name }}</option>
                                        @else
                                            <option value="{{ $module->id }}">{{ $module->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="label">Име на полето</label>
                                <input type="text" class="form-control" name="label" value="{{ $hover_balloon->label }}">
                            </div>
                            <div class="form-group">
                                <label for="hover_text">Текст</label>
                                <input type="text" class="form-control" name="hover_text" value="{{ $hover_balloon->hover_text }}">
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button class="btn btn-primary" type="submit">Промени</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection