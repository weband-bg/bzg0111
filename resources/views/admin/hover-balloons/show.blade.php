@extends('layouts.admin')

@section('fa-icon', 'commenting')
@section('page-name', 'Ховър балони')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Ховър балони</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Поле</th>
                        <th>Балон</th>
                        <th>Редактирай</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($module->hoverBalloons as $balloon)
                        <tr>
                            <td>{{ $balloon->label }}</td>
                            <td>{{ $balloon->hover_text }}</td>
                            <td>
                                <a class="btn btn-primary btn-flat"
                                   href="{{ route('edit-hover-balloon', ['id' => $balloon->id]) }}">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection