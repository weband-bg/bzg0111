@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Водят ли се съдебни дела срещу имота ми')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Водят ли се съдебни дела срещу имота ми</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
                        <th>Тип</th>
                        <th>Лице</th>
			        	<th>Адрес</th>
                        <th>Информация за собственика</th>
                        <th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($properties as $property)
						<tr id="{{ $property->id }}">
                            <td>
                                @if($property->type == 'individual')
                                    Физическо лице
                                @elseif($property->type == 'legal-entity')
                                    Юридическо лице
                                @endif
                            </td>
							<td>
								@if($property->type == 'legal-entity')
									Име: {{ $property->name }}
                                    <br>
                                    ЕИК: {{ $property->eik }}
								@elseif($property->type == 'individual')
									Име: {{ $property->getPersonNames() }}
                                    <br>
                                    ЕГН: {{ $property->pin }}
								@endif
							</td>
							<td>
								{{ $property->getAddress() }}
							</td>
                            <td>
                                Име: {{ $property->getOwnerNames() }}
                                <br>
                                {{ $property->getOwnerPIN() }}
                            </td>
                            <td>
								<a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $property->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-property-lawsuit') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection