@extends('layouts.admin')


@section('page-name', 'Модули')
@section('sub-page-name', 'Приходи на фирма')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Приходи на фирма</h3>
      </div>

      <div class="table-responsive">
      	<table class="table table-hover table-bordered">
      		<thead>
      			<tr>
      				<th>Име</th>
      				<th>ЕИК</th>
              <th>Изтрий</th>
      			</tr>
      		</thead>
      		<tbody>
      			@foreach($revenues as $revenue)
              <tr id="revenue-{{ $revenue->id }}">
                <td>{{ $revenue->name }}</td>
                <td>{{ $revenue->eik }}</td>
                <td>
                  <a class="btn btn-danger btn-flat delete-revenue" href="#" data-id="{{ $revenue->id }}">
                    <i class="fa fa-lg fa-trash"></i>
                  </a>
                </td>
              </tr>
            @endforeach
      		</tbody>
      	</table>
      </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-revenue').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-revenue') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('#revenue-' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection