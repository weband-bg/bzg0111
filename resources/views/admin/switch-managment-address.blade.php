@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Промяна на адреса на управление')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Промяна на адреса на управление</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>Име на фирма</th>
			        	<th>ЕИК</th>
			        	<th>Aдрес</th>
			        	<th>Имена</th>
			        	<th>Email</th>
			        	<th>Дата на валидност на лична карта</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($switches as $switch)
						<tr id="{{ $switch->id }}">
							<td>{{ $switch->company_name }}</td>
							<td>{{ $switch->eik }}</td>
							<td>{{ $switch->getAddress() }}</td>
							<td>{{ $switch->getNames() }}</td>
							<td>{{ $switch->owner_email }}</td>
							<td>{{ $switch->owner_id_expiry_date }}</td>
							<td>
								<a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $switch->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-switch') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection