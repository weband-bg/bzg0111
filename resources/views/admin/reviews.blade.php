@extends('layouts.admin')

@section('page-name', 'Отзиви')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Отзиви</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>Име</th>
			        	<th>Email</th>
			        	<th>Отзив</th>
			        	<th>Статус</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($reviews as $review)
						<tr id="review-{{$review->id}}">
							<td>{{ $review->getName() }}</td>
							<td>{{ $review->email }}</td>
							<td>{{ $review->text }}</td>
							<td id="review-{{$review->id}}-status">
									@if($review->approved)
										<a href="#" class="btn btn-success btn-flat approve-review" data-id="{{ $review->id }}">
											<i class="fa fa-lg fa-check"></i>
										</a>
									@else
										<a href="#" class="btn btn-warning btn-flat approve-review" data-id="{{ $review->id }}">
											<i class="fa fa-lg fa-times"></i>
										</a>
									@endif
							</td>
							<td>
								<a class="btn btn-danger btn-flat delete-review" href="#" data-id="{{ $review->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-review').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете този отзив?",
      text: "Внимание! След като веднъж продуктът е изтрит няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-review') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('#review-' + id).remove()
          swal("Успех!", "Отзивът беше изтрит успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })

  $('.approve-review').click(function(event) {
  	event.preventDefault()

  	let id = $(this).data('id')

  	swal({
  		title: "Сигурни ли сте че искате да одобрите този отзив?",
  		text: "Одобрените отзиви са достъпни за всички потребители в сайта",
  		type: "info",
  		showCancelButton: true,
  		closeOnConfirm: false,
  		showLoaderOnConfirm: true,
  		confirmButtonText: "Одобри",
  		cancelButtonText: "Не одобрявай"
  	},
  	function(){
  		$.ajax({
  			url: '{{ route('review-change-approved') }}',
  			type: 'POST',
  			data: {_token: "{{ csrf_token() }}", id: id },
  		})
  		.done(function(data) {
  			if (data == 'approved') {
  				$('#review-'+id+'-status').html('<a href="#" class="btn btn-success btn-flat approve-review" data-id="'+id+'"><i class="fa fa-lg fa-check"></i></a>')
  				swal("Успех!", "Отзивът беше одобрен успешно!", "success")
  			}
  			else if(data == 'disapproved') {
  				$('#review-'+id+'-status').html('<a href="#" class="btn btn-warning btn-flat approve-review" data-id="'+id+'"><i class="fa fa-lg fa-times"></i></a>')
  				swal("Успех!", "Отзивът вече не е видим за потребителите!", "success")	
  			}
  			else {
  				swal("Опа...", "Възникна грешка!", "error")
  			}
  		})
  		.fail(function() {
  			swal("Опа...", "Възникна грешка!", "error")
  		})
  	})
  })
})
</script>
@endsection