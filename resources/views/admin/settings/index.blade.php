@extends('layouts.admin')

@section('page-name', 'Настройки')

@section('content')
<div class="col-md-12">
	<div class="card">
        <div class="row">
            <div class="col-md-12">
                <div class="well bs-component">
                    <form class="form-horizontal" method="POST" action="{{ route('store-page-settings') }}" enctype="multipart/form-data" id="app">
            	       {{csrf_field()}}
            	       <input type="hidden" name="id" value="{{ $settings->id }}">
                        <fieldset>
                            <legend>{{ $settings->page_title }}</legend>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="title">Заглавие</label>
                                <div class="col-md-8">
                                    <input class="form-control" id="title" type="text" name="title" value="{{ $settings->page_title }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="meta-keywords">Мета ключови думи</label>
                                <div class="col-md-8">
                                    <textarea name="keywords" id="meta-keywords" class="form-control" rows="5">{{ $settings->meta_keywords }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="meta-description">Мета описание</label>
                                <div class="col-md-8">
                                    <textarea name="description" id="meta-description" class="form-control" rows="5">{{ $settings->meta_description }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="text">Текст</label>
                                <div class="col-md-8">
                                    <textarea name="text" id="text" class="form-control wysiwyg" rows="5">{{ $settings->text }}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="image">Снимка</label>
                                <div class="col-md-8">
                                    <input type="file" name="image" class="form-control">
                                    @if($settings->image)
                                        <img src="{{ asset(config('app.settings-images') . $settings->image) }}" alt="{{ $settings->page_title }}" class="img-responsive">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="price">Цена</label>
                                <div class="col-md-8">
                                    <input name="price" id="price" class="form-control" type="number" step="0.01" value="{{ $settings->price }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="foreign-price">Цена за чужденци</label>
                                <div class="col-md-8">
                                    <input type="number" step="0.01" class="form-control" id="foreign-price" name="foreign_price" value="{{ $settings->foreign_price }}">
                                </div>
                            </div>

                            <h3 class="text-center">Въпроси/отговори</h3>
                            <div v-for="(question_answer, index) in question_answers">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" :for="'question-'+index">Въпрос</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" :id="'question-'+index" :name="'question_answers['+index+'][question]'" v-model="question_answer.question">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" :for="'answer-'+index">Отговор</label>
                                    <div class="col-md-8">
                                        <textarea :name="'question_answers['+index+'][answer]'" class="wysiwyg" :id="'answer-'+index">@{{ question_answer.answer }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" :for="'category-'+index">Категория</label>
                                    <div class="col-md-8">
                                        <select :name="'question_answers['+index+'][category]'" :id="'category-'+index" class="form-control" v-model="question_answers[index].category.id">
                                            <option v-for="category in categories" :value="category.id">@{{ category.name }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-2">
                                        <button class="btn btn-danger" type="button" @click="removeQuestionAnswer(index)">Премахни</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2 text-right">
                                <button class="btn btn-primary" type="button" @click="addQuestionAnswer">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-2">
                                    <button class="btn btn-primary" type="submit">Запиши</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    let app = new Vue({
        el: '#app',
        data: {
            question_answers: [
                // {question: '', answer: '', category: 1}
            ],
            categories: []
        },
        created: function() {
            this.getCategories(),
            this.getQuestionAnswers()
        },
        methods: {
            getCategories: function() {
                let app = this

                axios.get('{{ route('settings-categories') }}')
                    .then(function(response) {
                        app.categories = response.data
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            },
            addQuestionAnswer: function() {
                let question_answer = {question: '', answer: '', category: {id: 1}}

                this.question_answers.push(question_answer)

                let app = this

                Vue.nextTick()
                    .then(function () {
                        let id = app.question_answers.length-1
                        let editorId = 'answer-'+id
                        tinymce.EditorManager.execCommand('mceAddEditor', false, editorId)
                    })
            },
            getQuestionAnswers: function() {
                let app = this

                axios.get('{{ route('get-question-answers', $settings->id) }}')
                    .then(function(response) {
                        console.log(response.data)
                        app.question_answers = response.data

                        Vue.nextTick()
                            .then(function () {
                                // let id = app.question_answers.length-1
                                // let editorId = 'answer-'+id
                                // tinymce.EditorManager.execCommand('mceAddEditor', false, editorId)
                                tinymce.init({
                                    selector: 'textarea.wysiwyg',
                                    // height: 300,
                                    plugins: [
                                    'advlist autolink lists link charmap print preview anchor',
                                    'searchreplace visualblocks code fullscreen',
                                    'insertdatetime media table contextmenu paste code',
                                    'textcolor'
                                    ],
                                    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
                                    content_css: '//www.tinymce.com/css/codepen.min.css'
                                });
                            })
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            },
            removeQuestionAnswer: function(index) {
                this.question_answers.splice(index, 1)
            }
        }
    })
</script>
@endsection

@section('scripts')
<!-- WYSIWYG -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea.wysiwyg',
        // height: 300,
        plugins: [
        'advlist autolink lists link charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code',
        'textcolor'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor',
        content_css: '//www.tinymce.com/css/codepen.min.css'
    });
</script>
<link rel="stylesheet" type="text/css" id="u0" href="http://cdn.tinymce.com/4/skins/lightgray/skin.min.css">
<!-- SELECT2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('select#category').select2()
    })
</script>
@endsection