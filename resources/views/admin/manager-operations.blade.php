@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Смяна, добавяне или заличаване на управител')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Смяна, добавяне или заличаване на управител</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Име на фирма</th>
                        <th>ЕИК</th>
                        <th>Операция</th>
                        <th>Информация за управител</th>
                        <th>Информация за клиент</th>

                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($manager_operations as $manager_operation)
                        <tr id="{{ $manager_operation->id }}">
                            <td>{{ $manager_operation->company_name }}</td>
                            <td>{{ $manager_operation->eik }}</td>
                            <td>{{ $manager_operation->getOperation() }}</td>
                            <td>
                                <br>
                                @if($manager_operation->operation == 1)
                                    Име: {{ $manager_operation->getManagerNames() }}
                                    <br>
                                    ЕГН: {{ $manager_operation->add_manager_pin }}
                                    <br>
                                    Номер на лична карта: {{ $manager_operation->add_manager_id_number }}
                                    <br>
                                    Дата на издаване: {{ $manager_operation->add_manager_id_date_created }}
                                    <br>
                                    Издадена от: {{ $manager_operation->add_manager_id_created_by }}
                                @endif

                                @if($manager_operation->operation == 2)
                                    Име: {{ $manager_operation->getManagerNames() }}
                                @endif

                                @if($manager_operation->operation == 3)
                                    Имена на управителя, който се заличава: {{ $manager_operation->switch_manager_name }}
                                    <br>
                                    Име: {{ $manager_operation->getManagerNames() }}
                                    <br>
                                    ЕГН: {{ $manager_operation->switch_manager_pin }}
                                    <br>
                                    Номер на лична карта: {{ $manager_operation->switch_manager_id_number }}
                                    <br>
                                    Дата на издаване: {{ $manager_operation->switch_manager_id_date_created }}
                                    <br>
                                    Издадена от: {{ $manager_operation->switch_manager_id_created_by }}
                                @endif

                            </td>
                            <td>
                                Име: {{ $manager_operation->getOwnerNames() }}
                                <br>
                                E-мейл: {{ $manager_operation->owner_email }}
                                <br>
                                Дата на валидност на лична карта: {{ $manager_operation->owner_id_expiry_date }}
                                <br>
                                Телефон: {{ $manager_operation->owner_phone }}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-info" href="#"
                                   data-id="{{ $manager_operation->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-info').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-manager-operation') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('tr#' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection