@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Пълномощно за продажба на МПС')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Пълномощно за продажба на МПС</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Талон</th>
                        <th>Продавач</th>
                        <th>Купувач</th>
                        <th>Пълномощник</th>
                        <th>Клауза за договаряне сам със себе си</th>
                        <th>Информация за кола</th>
                        <th>Информация за клиент</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($proxies as $proxy)
                        <tr id="car-sale-proxy-{{ $proxy->id }}">
                            <td>{{ $proxy->id }}</td>
                            <td><a href="{{ asset( '/uploads/files/' .  $proxy->talon)  }}">Талон</a></td>
                            <td>
                                @if($proxy->seller_type == 'seller-individual')
                                    Физическо лице
                                    <br>
                                    Име: {{ $proxy->getSellerNames() }}
                                    <br>
                                    ЕГН: {{ $proxy->seller_pin }}
                                @elseif($proxy->seller_type == 'seller-legal-entity')
                                    Юридическо лице
                                    <br>
                                    Име: {{ $proxy->seller_name }}
                                    <br>
                                    ЕИК: {{ $proxy->seller_eik }}
                                @endif
                            </td>
                            <td>
                                @if($proxy->buyer_type == 'buyer-individual')
                                    Физическо лице
                                    <br>
                                    Име: {{ $proxy->getBuyerNames() }}
                                    <br>
                                    ЕГН: {{ $proxy->buyer_pin }}
                                @elseif($proxy->buyer_type == 'buyer-legal-entity')
                                    Юридическо лице
                                    <br>
                                    Име: {{ $proxy->buyer_name }}
                                    <br>
                                    ЕИК: {{ $proxy->buyer_eik }}
                                @endif
                            </td>
                            <td>
                                Име: {{ $proxy->getProxyNames() }}
                                <br>
                                ЕГН: {{ $proxy->proxy_pin }}
                                <br>
                                Номер на лична карта: {{ $proxy->proxy_id_number }}
                                <br>
                                Дата на издаване: {{ $proxy->proxy_id_date_created }}
                                <br>
                                Издадена от: {{ $proxy->proxy_id_created_by }}
                            </td>
                            <td>
                                {{ $proxy->self_contract_clause ? 'Да' : 'Не' }}
                            </td>
                            <td>
                                Тип: {{ $proxy->carType->type }}
                                <br>
                                Марка: {{ $proxy->carMake->make }}
                                <br>
                                Модел: {{ $proxy->car_model }}
                                <br>
                                Номер на шаси: {{ $proxy->car_chassis }}
                                <br>
                                Регистрационен номер: {{ $proxy->car_registration }}
                                <br>
                                Цена: {{ $proxy->price }}
                                <br>
                                @if($proxy->payment == 4)
                                    Начин на плащане: Продажната цена ще бъде заплатена в срок до {{ $proxy->payment_date }}
                                    Начин на плащане: {{ $proxy->getPayment() }}
                                @endif
                            </td>
                            <td>
                                Име: {{ $proxy->getClientNames() }}
                                <br>
                                E-мейл: {{ $proxy->owner_email }}
                                <br>
                                Телефон: {{ $proxy->owner_phone }}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-car-sale-contract" href="#"
                                   data-id="{{ $proxy->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-car-sale-contract').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-car-sale-proxy') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('#car-sale-proxy-' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection