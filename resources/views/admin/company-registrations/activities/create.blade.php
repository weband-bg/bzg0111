@extends('layouts.admin')

@section('fa-icon', 'commenting')
@section('page-name', 'Дейности на фирма')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Дейности</h3>
                <form method="POST"
                      action="{{ route('create-activity') }}">
                    {{ csrf_field() }}
                    @if(count($errors) > 0)
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="alert-box error">
                                        @foreach($errors->all() as $error)
                                            {{ $error }}
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <label class="control-label">Име на Дейност</label>
                        <input class="form-control" name="name" value="{{ old('name') }}">
                    </div>

                    <button type="submit" class="btn btn-primary">Качи</button>
                </form>
            </div>
        </div>
    </div>
@endsection