@extends('layouts.admin')

@section('fa-icon', 'commenting')
@section('page-name', 'Дейности на фирма')

@section('content')
    <div class="col-md-12">
        <div style="margin-bottom: 20px;">
            <a href="{{ route('create-activity') }}" class="btn btn-primary">Добави дейност</a>
        </div>
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Дейности</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Дейност</th>
                        <th>Редактирай</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($activities as $activity)
                        <tr id="{{ $activity->id }}">
                            <td>{{ $activity->name }}</td>
                            <td>
                                <a class="btn btn-primary btn-flat" href="{{ route('edit-activity', ['id' => $activity->id]) }}">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $activity->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-activity') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection