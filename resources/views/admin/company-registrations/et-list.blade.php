@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Регистрация на ЕТ')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Заявки за регистрация на ЕТ</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>№</th>
			        	<th>Име на фирмата</th>
			        	<th>Собственик</th>
			        	<th>Виж цялата информация</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($companies as $company)
						<tr id="{{ $company->id }}">
							<td>{{ $company->id }}</td>
							<td>{{ $company->sole_trader_name }}</td>
							<td>{{ $company->fullname() }}</td>
							<td>
								<a href="#" class="btn btn-info btn-flat" data-id="{{ $company->id }}" data-toggle="modal" data-target="#info-modal-{{ $company->id }}">
									<i class="fa fa-eye"></i>
								</a>
							</td>
							<td>
								<a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $company->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>

<!-- Modals -->
@foreach($companies as $company)
<div id="info-modal-{{ $company->id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Заявка №{{ $company->id }}</h4>
      </div>
      <div class="modal-body">
        <p> Име на фирмата: {{ $company->sole_trader_name }}</p>
        <p> Име на фирмата на чужд език: {{ $company->sole_trader_foreign_name }}</p>
        @if($company->zone)
        	<p> Област: {{ $company->zone->name }} </p>
        @endif
		@if($company->township)
			<p> Община: {{ $company->township->name }} </p>
		@endif
		@if($company->postcode)
			<p> Пощенски код: {{ $company->postcode }} </p>
		@endif
		@if($company->quarter)
			<p> Квартал: {{ $company->quarter }} </p>
		@endif
		@if($company->street)
			<p> Улица: {{ $company->street }} </p>
		@endif
		@if($company->street_number)
			<p> Номер на улица: {{ $company->street_number }} </p>
		@endif
		@if($company->building)
			<p> Блок: {{ $company->building }} </p>
		@endif
		@if($company->vhod)
			<p> Вход: {{ $company->vhod }} </p>
		@endif
		@if($company->floor)
			<p> Етаж: {{ $company->floor }} </p>
		@endif
		@if($company->apartment)
			<p> Апартамент: {{ $company->apartment }} </p>
		@endif
	
		<h4 class="text-center"> Информация за собственика </h4>
		<p> Имена: {{ $company->fullname() }} </p>
		<p> ЕГН: {{ $company->owner_pin }} </p>
		<p> Номер на лична карта: {{ $company->owner_id_number }} </p>
		<p> Издадена от: {{ $company->owner_id_created_by }} </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endforeach
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-company-et') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection