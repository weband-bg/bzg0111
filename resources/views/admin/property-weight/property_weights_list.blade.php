@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Има ли тежести върху имота ми')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Има ли тежести върху имота ми</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>№</th>
			        	<th>Тип</th>
			        	<th>Лице</th>
			        	<th>Адрес</th>
			        	<th>Информация за собственика</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($property_weights as $property_weight)
						<tr id="property-weight-{{ $property_weight->id }}">
							<td>{{ $property_weight->id }}</td>
							<td>
								@if($property_weight->type == 'individual')
									Физическо лице
                                @elseif($property_weight->type == 'legal-entity')
									Юридическо лице
								@endif
							</td>
                            <td>
                                @if($property_weight->type == 'individual')
                                    Име: {{ $property_weight->getPersonNames() }}
                                    <br>
                                    ЕГН: {{ $property_weight->pin }}
                                @elseif($property_weight->type == 'legal-entity')
                                    Име: {{ $property_weight->name }}
                                    <br>
                                    ЕИК: {{ $property_weight->eik }}
                                @endif
                            </td>
							<td>{{ $property_weight->getAddress() }}</td>
							<td>{{ $property_weight->getOwnerNames() }} {{ $property_weight->getOwnerPIN() }}</td>
							<td>
								<a class="btn btn-danger btn-flat delete-property-weight" href="#" data-id="{{ $property_weight->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-property-weight').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете тази информация?",
      text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-property-weight') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('#property-weight-' + id).remove()
          swal("Успех!", "Информацията беше изтрита успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection