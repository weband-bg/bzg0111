@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Превод на фирмени документи')

@section('content')
<div class="col-md-12">
	<div class="card">
      <div class="clearfix">
        <h3 class="card-title">Превод на фирмени документи</h3>
      </div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>Домейн</th>
			        </tr>
		    	</thead>
	      		<tbody>
					
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection