@extends('layouts.admin')

@section('page-name', 'Банери')
@section('sub-page-name', 'Банери')

@section('content')
<div class="col-md-12">
	<div class="card">
		<div class="row">
			<div class="col-md-12">
				<div class="well bs-component">
					<form class="form-horizontal" method="POST" action="{{ route('update-banner', $banner->id) }}" enctype="multipart/form-data">
						{{csrf_field()}}
						<fieldset>
			                <legend>Редактирай банер</legend>
			                <div class="form-group">
			                  	<label class="col-md-2 control-label" for="name">Име</label>
			                  	<div class="col-md-8">
			                    	<input class="form-control" id="name" type="text" name="name" value="{{ old('name') ? old('name') : $banner->name }}">
			                  	</div>
			                </div>
			                <div class="form-group">
			                	<label for="link" class="col-md-2 control-label">Линк</label>
			                	<div class="col-md-8">
			                		<input class="form-control" id="link" type="url" name="url" value="{{ old('url') ? old('url') : $banner->link }}">
			                	</div>
			                </div>
			                <div class="form-group">
			                	<label for="image" class="col-md-2 control-label">Снимка</label>
			                	<div class="col-md-8">
			                		<input class="form-control" id="image" type="file" name="image">
			                		<img src="{{ asset(config('app.banner-images').$banner->image) }}" alt="{{ $banner->name }}" class="img-responsive">
			                	</div>
			                </div>
			                <div class="form-group">
			                	<label for="page" class="col-md-2 control-label">Страница</label>
			                	<div class="col-md-8">
			                		<select name="page" id="page" class="form-control">
			                			@foreach($page_settings as $page_setting)
			                				@if($page_setting->id == $banner->page_settings->id)
			                					<option value="{{ $page_setting->id }}" selected>{{ $page_setting->page_title }}</option>
			                				@else	
			                					<option value="{{ $page_setting->id }}">{{ $page_setting->page_title }}</option>
			                				@endif
			                			@endforeach
			                		</select>
			                	</div>
			                </div>
			                <div class="form-group">
			                 	<div class="col-md-8 col-md-offset-2">
			                    	<button class="btn btn-primary" type="submit">Запиши</button>
			                  	</div>
			                </div>
			              </fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection