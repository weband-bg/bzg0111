@extends('layouts.admin')

@section('page-name', 'Банери')
@section('sub-page-name', 'Банери')

@section('content')
<div class="col-md-12">
	<div style="margin-bottom: 20px;">
		<a href="{{ route('create-banner') }}" class="btn btn-primary">Добави банер</a>
	</div>
	<div class="card">
		<div class="clearfix">
			<h3 class="card-title">Банери</h3>
		</div>
  		<div class="table-responsive">
	    	<table class="table table-hover table-bordered">
      			<thead>
			        <tr>
			        	<th>Име</th>
			        	<th>Линк</th>
			        	<th>Страница</th>
			        	<th>Редактирай</th>
			        	<th>Изтрий</th>
			        </tr>
		    	</thead>
	      		<tbody>
					@foreach($banners as $banner)
						<tr id="{{ $banner->id }}">
							<td>{{ $banner->name }}</td>
							<td>
								<a href="{{ $banner->link }}" target="_blank">{{ $banner->link }}</a>
							</td>
							<td>{{ $banner->page_settings->page_title }}</td>
							<td>
								<a href="{{ route('edit-banner', $banner->id) }}" class="btn btn-info btn-flat">
									<i class="fa fa-lg fa-edit"></i>
								</a>
							</td>
							<td>
								<a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $banner->id }}">
									<i class="fa fa-lg fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
			    </tbody>
	    	</table>
	  	</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function() {
  $('.delete-info').click(function(event) {
    event.preventDefault()

    let id = $(this).data('id')

    swal({
      title: "Сигурни ли сте че искате да изтриете този банер?",
      text: "Внимание! След като веднъж банерът е изтрит няма да може да се върне.",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: "Изтрий!",
      cancelButtonText: "Не!"
    },
    function(){
      $.ajax({
        url: "{{ route('delete-banner') }}",
        type: 'POST',
        data: {_token: "{{ csrf_token() }}", id: id}
      })
      .done(function(data) {
        if (data == 'success') {
          $('tr#' + id).remove()
          swal("Успех!", "Банерът беше изтрит успешно!", "success")
        }
        else if (data == 'error') {
          swal("Опа...", "Възникна грешка!", "error");
        }
      })
      .fail(function() {
        swal("Опа...", "Възникна грешка!", "error");
      })
    })
  })
})
</script>
@endsection