@extends('layouts.admin')

@section('page-name', 'Банери')
@section('sub-page-name', 'Банери')

@section('content')
<div class="col-md-12">
	<div class="card">
		<div class="row">
			<div class="col-md-12">
				<div class="well bs-component">
					<form class="form-horizontal" method="POST" action="{{ route('store-banner') }}" enctype="multipart/form-data">
						{{csrf_field()}}
						<fieldset>
			                <legend>Добави банер</legend>
			                <div class="form-group">
			                  	<label class="col-md-2 control-label" for="name">Име</label>
			                  	<div class="col-md-8">
			                    	<input class="form-control" id="name" type="text" name="name" value="{{ old('name') }}">
			                  	</div>
			                </div>
			                <div class="form-group">
			                	<label for="link" class="col-md-2 control-label">Линк</label>
			                	<div class="col-md-8">
			                		<input class="form-control" id="link" type="url" name="url" value="{{ old('url') }}">
			                	</div>
			                </div>
			                <div class="form-group">
			                	<label for="image" class="col-md-2 control-label">Снимка</label>
			                	<div class="col-md-8">
			                		<input class="form-control" id="image" type="file" name="image">
			                	</div>
			                </div>
			                <div class="form-group">
			                	<label for="page" class="col-md-2 control-label">Страница</label>
			                	<div class="col-md-8">
			                		<select name="page" id="page" class="form-control">
			                			@foreach($page_settings as $page_setting)
			                				<option value="{{ $page_setting->id }}">{{ $page_setting->page_title }}</option>
			                			@endforeach
			                		</select>
			                	</div>
			                </div>
			                <div class="form-group">
			                 	<div class="col-md-8 col-md-offset-2">
			                    	<button class="btn btn-primary" type="submit">Запиши</button>
			                  	</div>
			                </div>
			              </fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection