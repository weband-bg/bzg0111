@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Генерално Търговско Пълномощно')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Генерално Търговско Пълномощно</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>Настоящо име на фирмата</th>
                        <th>ЕИК</th>
                        <th>Информация за управител</th>
                        <th>Информация за пълномощник</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($proxies as $proxy)
                        <tr id="{{ $proxy->id }}">
                            <td>{{ $proxy->current_company_name }}</td>
                            <td>{{ $proxy->eik }}</td>
                            <td>
                                Име: {{ $proxy->getManagerNames() }}
                                <br>
                                ЕГН: {{ $proxy->manager_pin }}
                                <br>
                                Номер на лична карта: {{ $proxy->manager_id_number }}
                                <br>
                                Дата на издаване: {{ $proxy->manager_id_date_created }}
                                <br>
                                Издадена от: {{ $proxy->manager_id_created_by }}

                            </td>
                            <td>
                                Име: {{ $proxy->getProxyNames() }}
                                <br>
                                ЕГН: {{ $proxy->proxy_pin }}
                                <br>
                                Номер на лична карта: {{ $proxy->proxy_id_number }}
                                <br>
                                Дата на издаване: {{ $proxy->proxy_id_date_created }}
                                <br>
                                Издадена от: {{ $proxy->proxy_id_created_by }}

                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-info" href="#" data-id="{{ $proxy->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-info').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-general-commercial-proxy') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('tr#' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection