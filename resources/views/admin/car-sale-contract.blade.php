@extends('layouts.admin')

@section('page-name', 'Модули')
@section('sub-page-name', 'Договор за продажба на МПС')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="clearfix">
                <h3 class="card-title">Договор за продажба на МПС</h3>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Талон</th>
                        <th>Продавач</th>
                        <th>Купувач</th>
                        <th>Информация за кола</th>
                        <th>Информация за клиент</th>
                        <th>Изтрий</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contracts as $contract)
                        <tr id="car-sale-contract-{{ $contract->id }}">
                            <td>{{ $contract->id }}</td>
                            <td><a href="{{ asset( '/uploads/files/' .  $contract->talon)  }}">Талон</a></td>
                            <td>
                                @if($contract->seller_type == 'seller-individual')
                                    Физическо лице
                                    <br>
                                    Име: {{ $contract->getSellerNames() }}
                                    <br>
                                    ЕГН: {{ $contract->seller_pin }}
                                @elseif($contract->seller_type == 'seller-legal-entity')
                                    Юридическо лице
                                    <br>
                                    Име: {{ $contract->seller_name }}
                                    <br>
                                    ЕИК: {{ $contract->seller_eik }}
                                @endif
                            </td>
                            <td>
                                @if($contract->buyer_type == 'buyer-individual')
                                    Физическо лице
                                    <br>
                                    Име: {{ $contract->getBuyerNames() }}
                                    <br>
                                    ЕГН: {{ $contract->buyer_pin }}
                                @elseif($contract->buyer_type == 'buyer-legal-entity')
                                    Юридическо лице
                                    <br>
                                    Име: {{ $contract->buyer_name }}
                                    <br>
                                    ЕИК: {{ $contract->buyer_eik }}
                                @endif
                            </td>
                            <td>
                                Тип: {{ $contract->carType->type }}
                                <br>
                                Марка: {{ $contract->carMake->make }}
                                <br>
                                Модел: {{ $contract->car_model }}
                                <br>
                                Номер на шаси: {{ $contract->car_chassis }}
                                <br>
                                Регистрационен номер: {{ $contract->car_registration }}
                                <br>
                                Цена: {{ $contract->price }}
                                <br>
                                @if($contract->payment == 4)
                                    Начин на плащане: Продажната цена ще бъде заплатена в срок до {{ $contract->payment_date }}
                                    Начин на плащане: {{ $contract->getPayment() }}
                                @endif
                            </td>
                            <td>
                                Име: {{ $contract->getClientNames() }}
                                <br>
                                E-мейл: {{ $contract->owner_email }}
                                <br>
                                Телефон: {{ $contract->owner_phone }}
                            </td>
                            <td>
                                <a class="btn btn-danger btn-flat delete-car-sale-contract" href="#"
                                   data-id="{{ $contract->id }}">
                                    <i class="fa fa-lg fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.delete-car-sale-contract').click(function (event) {
                event.preventDefault()

                let id = $(this).data('id')

                swal({
                        title: "Сигурни ли сте че искате да изтриете тази информация?",
                        text: "Внимание! След като веднъж информацията е изтрита няма да може да се върне.",
                        type: "warning",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Изтрий!",
                        cancelButtonText: "Не!"
                    },
                    function () {
                        $.ajax({
                            url: "{{ route('delete-car-sale-contract') }}",
                            type: 'POST',
                            data: {_token: "{{ csrf_token() }}", id: id}
                        })
                            .done(function (data) {
                                if (data == 'success') {
                                    $('#car-sale-contract-' + id).remove()
                                    swal("Успех!", "Информацията беше изтрита успешно!", "success")
                                }
                                else if (data == 'error') {
                                    swal("Опа...", "Възникна грешка!", "error");
                                }
                            })
                            .fail(function () {
                                swal("Опа...", "Възникна грешка!", "error");
                            })
                    })
            })
        })
    </script>
@endsection