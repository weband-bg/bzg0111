@extends('layouts.main')

@section('title', 'Промяна на адрес на управление')
@section('page_title', 'Промяна на адрес на управление')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

        @if(isset($settings))
            <h2>{{ $settings->text }}</h2>
        @endif
    <div id="app">
        <form action="{{ route('store-switch-management-address') }}" method="POST">
            {{csrf_field()}}
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label for="company_name">Име на Фирма</label>
                    <input type="text" name="company_name" id="company_name" class="form-control" required value="{{ old('company_name') }}">
                    @if($errors->has('company_name'))
                    <span class="help-block">{{ $errors->first('company_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="eik">ЕИК</label>
                    <input type="text" name="eik" id="eik" class="form-control" required value="{{ old('eik') }}">
                    @if($errors->has('eik'))
                    <span class="help-block">{{ $errors->first('eik') }}</span>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <h2 class="text-center">Нов адрес на управление на фирмата</h2>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label for="zone">Област</label>
                    <select class="form-control" id="zone" name="zone" v-model="currentZone" @change="getTownships">
                        <option v-for="zone in zones" :value="zone.id">@{{ zone.name }}</option>
                    </select>
                    @if($errors->has('zone'))
                        <span>{{ $errors->first('zone') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="township">Община</label>
                    <select class="form-control" id="township" name="township" v-model="currentTownship" @change="getPopulatedPlaces">
                        <option v-for="township in townships" :value="township.id">@{{ township.name }}</option>
                    </select>
                    @if($errors->has('township'))
                        <span>{{ $errors->first('township') }}</span>
                    @endif  
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="populated_place">Населено място</label>
                    <select class="form-control" id="populated_place" name="populated_place" v-model="currentPopulatedPlace">
                        <option v-for="populated_place in populated_places" :value="populated_place.id">@{{ populated_place.name }}</option>
                    </select>
                    @if($errors->has('populated_place'))
                        <span>{{ $errors->first('populated_place') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="postcode">Пощенски код</label>
                    <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode') }}">
                    @if($errors->has('postcode'))
                    <span class="help-block">{{ $errors->first('postcode') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="quarter">Квартал</label>
                    <input type="text" class="form-control" id="quarter" name="quarter" value="{{ old('quarter') }}">
                    @if($errors->has('quarter'))
                    <span class="help-block">{{ $errors->first('quarter') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="street">Улица</label>
                    <input type="text" class="form-control" id="street" name="street" value="{{ old('street') }}">
                    @if($errors->has('street'))
                    <span class="help-block">{{ $errors->first('street') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="street_number">Номер</label>
                    <input type="text" class="form-control" id="street_number" name="street_number" value="{{ old('street_number') }}">
                    @if($errors->has('street_number'))
                    <span class="help-block">{{ $errors->first('street_number') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="building">Блок</label>
                    <input type="text" class="form-control" id="building" name="building" value="{{ old('building') }}">
                    @if($errors->has('building'))
                    <span class="help-block">{{ $errors->first('building') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="vhod">Вход</label>
                    <input type="text" class="form-control" id="vhod" name="vhod" value="{{ old('vhod') }}">
                    @if($errors->has('vhod'))
                    <span class="help-block">{{ $errors->first('vhod') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="floor">Етаж</label>
                    <input type="text" class="form-control" id="floor" name="floor" value="{{ old('floor') }}">
                    @if($errors->has('floor'))
                    <span class="help-block">{{ $errors->first('floor') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="apartment">Апартамент</label>
                    <input type="text" class="form-control" id="apartment" name="apartment" value="{{ old('apartment') }}">
                    @if($errors->has('apartment'))
                    <span class="help-block">{{ $errors->first('apartment') }}</span>
                    @endif
                </div>
            </div>

            <div class="col-md-12 text-center">
                <button class="btn btn-success" type="button" id="additional-info-button"> <span class="fa fa-legal"> </span> Потвърди заявка</button>
                <p>100 лв. с вкл. държавна, банкова такса и хонорар.</p>
            </div>
            
            <div class="col-md-12 additional-info" style="display: none">
                <div class="column col-md-4">
                    <div class="form-group">
                        <label for="owner_firstname">Име</label>
                        <input type="text" class="form-control" id="owner_firstname" name="owner_firstname" value="{{ old('owner_firstname') }}">
                        @if($errors->has('owner_firstname'))
                        <span class="help-block">{{ $errors->first('owner_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_secondname">Презиме</label>
                        <input type="text" class="form-control" id="owner_secondname" name="owner_secondname" value="{{ old('owner_secondname') }}">
                        @if($errors->has('owner_secondname'))
                        <span class="help-block">{{ $errors->first('owner_secondname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_lastname">Фамилия</label>
                        <input type="text" class="form-control" id="owner_lastname" name="owner_lastname" value="{{ old('owner_lastname') }}">
                        @if($errors->has('owner_lastname'))
                        <span class="help-block">{{ $errors->first('owner_lastname') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-12 additional-info" style="display: none">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_email">И-мейл</label>
                        <input type="text" class="form-control" id="owner_email" name="owner_email" value="{{ old('owner_email') }}">
                        @if($errors->has('owner_email'))
                            <span class="help-block">{{ $errors->first('owner_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_id_expiry_date">Дата на валидност на лична карта</label>
                        <input type="date" class="form-control" id="owner_id_expiry_date" name="owner_id_expiry_date" value="{{ old('owner_id_expiry_date') }}">
                        @if($errors->has('owner_id_expiry_date'))
                            <span class="help-block">{{ $errors->first('owner_id_expiry_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_phone">Телефон</label>
                        <input type="text" class="form-control" id="owner_phone" name="owner_phone" value="{{ old('owner_phone') }}">
                        @if($errors->has('owner_phone'))
                            <span class="help-block">{{ $errors->first('owner_phone') }}</span>
                        @endif
                    </div>
                </div>

            </div>
    
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h3 class="text-center">Начин на плащане</h3>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="office" value="office" checked>
                            <span>На място в офиса</span>
                        </label>
                    </div>
                    <div class="radio radio__custom radio__style1">
                        <label>
                            <input type="radio" name="payment_method" id="econt" value="econt">
                            <span>Еконт</span>
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <h3 class="text-center">Вашата информация</h3>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_firstname">Име</label>
                        <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                        @if($errors->has('payer_firstname'))
                            <span>{{ $errors->first('payer_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_lastname">Фамилия</label>
                        <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                        @if($errors->has('payer_lastname'))
                            <span>{{ $errors->first('payer_lastname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_email">Email</label>
                        <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                        @if($errors->has('payer_email'))
                            <span>{{ $errors->first('payer_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="payer_phone">Телефон</label>
                        <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                        @if($errors->has('payer_phone'))
                            <span>{{ $errors->first('payer_phone') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-md-12 additional-info text-center" style="display: none">
                <button class="btn btn-success" type="submit" id="submit"> <span class="fa fa-legal"> </span> Завърши заявка</button>
            </div>
            {{-- @if($settings->price > 0)
                <div style="display: none;" id="payments">
                    <div class="dt-sc-hr-invisible-medium"></div>
                    <div class="column dt-sc-one-column payments text-center">
                        <h2 class="text-center">
                            Начин на плащане
                            <br>
                            ({{ $settings->price }}) EUR
                        </h2>

                        <div class="column dt-sc-one-fourth first">
                            <div id="paypal-button"></div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="epay">Epay</label>
                                <input type="radio" name="payment" value="epay" id="epay">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="econt">Еконт</label>
                                <input type="radio" name="payment" value="econt" id="econt">
                            </div>
                        </div>

                        <div class="column dt-sc-one-fourth">
                            <div class="form-group">
                                <label for="office">На място в офиса</label>
                                <input type="radio" name="payment" value="office" id="office">
                            </div>
                        </div>
                    </div>
                </div>
            @endif --}}
        </form>
    </div>
    
    @include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

<script>
    let app = new Vue({
        el: '#app',
        data: {
            currentZone: {{ old('zone') ? old('zone') : 1 }},
            currentTownship: {{ old('township') ? old('township') : 1 }},
            currentPopulatedPlace: {{ old('populated_place') ? old('populated_place') : 1 }},
            zones: [],
            townships: [],
            populated_places: []
        },
        created: function() {
            this.getZones(),
            this.getTownships()
        },
        methods: {
            getZones: function() {
                let app = this

                axios.get('{{ route('zones-json') }}')
                    .then(function(response) {
                        app.zones = response.data
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            },
            getTownships: function() {
                let app = this

                axios.get('/api/zone/'+this.currentZone+'/townships')
                    .then(function(response) {
                        app.townships = response.data
                        app.currentTownship = response.data[0].id

                        app.getPopulatedPlaces()
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            },
            getPopulatedPlaces: function() {
                let app = this

                axios.get('/api/township/'+this.currentTownship+'/populated-places')
                    .then(function(response) {
                        app.populated_places = response.data
                        app.currentPopulatedPlace = response.data[0].id
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            }
        }
    })
</script>
@endsection

@section('scripts')
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript">
(function ($) {
    var additionalInfoButton = $('#additional-info-button');
    var additionalInfoDiv = $('.additional-info');

    additionalInfoButton.click(function (event) {
        event.preventDefault();

        additionalInfoDiv.fadeIn('slow');

    });

    submitButton.click(function (event) {
        event.preventDefault();

    });


    $('[data-toggle="tooltip"]').tooltip();
})(jQuery)
</script>
{{-- Paypal script --}}
@if($settings->price > 0)
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script type="text/javascript">
    (function ($) {
        $('#submit').click(function(event) {
            event.preventDefault()

            $('#payments').fadeIn('slow')

            var CREATE_PAYMENT_URL  = "{{ route('switch-management-address-paypal-create') }}"

            var EXECUTE_PAYMENT_URL = "{{ route('switch-management-address-paypal-execute') }}"

            paypal.Button.render({

                env: 'sandbox', // Or 'sandbox'

                commit: true, // Show a 'Pay Now' button

                payment: function() {
                    return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function(data) {
                        // console.log(data)
                        return data.id
                    })
                },

                // onAuthorize() is called when the buyer approves the payment
                onAuthorize: function(data, actions) {
                    console.log(data)
                    // Set up the data you need to pass to your server
                    var data = {
                        paymentID: data.paymentID,
                        payerID: data.payerID,
                        _token: '{{ csrf_token() }}'
                    };

                    let form_data = $('form').serialize()

                    //Make a POST request with the form data
                    $.ajax({
                        url: '{{ route('store-switch-management-address') }}',
                        type: 'POST',
                        data: form_data,
                        success: function(response) {
                            // Make a call to your server to execute the payment
                            return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                .then(function (res) {
                                    res = JSON.parse(res)
                                    console.log(res)
                                    
                                    //clear form and show success message
                                    $('form input').each(function(index, el) {
                                        $(this).val('')
                                    })

                                    alert('Заявката ви беше успешно изпратена')
                                })
                        },
                        error: function(data) {
                            let errors = data.responseJSON.errors

                            for(error in errors) {
                                $('input[name='+error+']').after('<span class="help-block">'+errors[error][0]+'</span>')
                            }
                        }
                    })
                }

            }, '#paypal-button')

            console.log('paypal')
        })
    })(jQuery)
</script>
@endif
@endsection
