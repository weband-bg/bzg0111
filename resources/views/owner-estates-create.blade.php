@extends('layouts.main')

@section('title', 'Проверка за недвижими имоти на лице')
@section('page_title', 'Проверка за недвижими имоти на лице')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
	<div class="alert alert-success alert-dismissible fade in text-uppercase">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
		<strong>{{ Session::get('success') }}</strong>
	</div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
	<strong>{{ Session::has('error') }}</strong>
</div>
@endif

<div class="title-bordered border__dashed">
	<h3>{{ $settings->text }}</h3>
</div>

<div id="app">
	<form action="{{ route('store-owner-estate') }}" method="POST">
		{{csrf_field()}}

		<div class="row">
			<div class="col-md-12">
				<h4 class="text-center">Вие сте</h4>

				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="radio radio__custom radio__style1">
						<label>
							<input type="radio" name="type" id="individual" value="individual" v-model="type">
							<span>Физическо лице</span>
						</label>
					</div>
					<div class="radio radio__custom radio__style1">
						<label>
							<input type="radio" name="type" id="legal-entity" value="legal-entity" v-model="type">
							<span>Юридическо лице</span>
						</label>
					</div>
					@if($errors->has('type'))
						<span>{{ $errors->first('type') }}</span>
					@endif
				</div>
			</div>
		</div>

		{{-- Individual --}}
		<transition name="fade" mode="out-in">
			<div class="row" v-if="type == 'individual'" key="individual">
				<div class="col-md-3">
					<div class="form-group">
						<label for="firstname">Име</label>
						<input type="text" class="form-control" id="firstname" name="firstname" value="{{ old('firstname') }}">
						@if($errors->has('firstname'))
							<span>{{ $errors->first('firstname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="secondname">Презиме</label>
						<input type="text" class="form-control" id="secondname" name="secondname" value="{{ old('secondname') }}">
						@if($errors->has('secondname'))
							<span>{{ $errors->first('secondname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="lastname">Фамилия</label>
						<input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}">
						@if($errors->has('lastname'))
							<span>{{ $errors->first('lastname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="pin">ЕГН</label>
						<input type="text" class="form-control" id="pin" name="pin" value="{{ old('pin') }}">
						@if($errors->has('pin'))
							<span>{{ $errors->first('pin') }}</span>
						@endif
					</div>
				</div>
			</div>

		{{-- Legal entity --}}
			<div class="row" v-else-if="type == 'legal-entity'" key="legal-entity">
				<div class="col-md-6">
					<div class="form-group">
						<label for="name">Име</label>
						<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
						@if($errors->has('name'))
							<span>{{ $errors->first('name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="eik">ЕИК</label>
						<input type="text" class="form-control" id="eik" name="eik" value="{{ old('eik') }}">
						@if($errors->has('eik'))
							<span>{{ $errors->first('eik') }}</span>
						@endif
					</div>
				</div>
			</div>
		</transition>

		<div class="row">
		    <div class="col-md-4 col-md-offset-4">
		        <h3 class="text-center">Начин на плащане</h3>
		        <div class="radio radio__custom radio__style1">
		            <label>
		                <input type="radio" name="payment_method" id="office" value="office" checked>
		                <span>На място в офиса</span>
		            </label>
		        </div>
		        <div class="radio radio__custom radio__style1">
		            <label>
		                <input type="radio" name="payment_method" id="econt" value="econt">
		                <span>Еконт</span>
		            </label>
		        </div>
		    </div>
		</div>

		<div class="row">
		    <h3 class="text-center">Вашата информация</h3>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_firstname">Име</label>
		            <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
		            @if($errors->has('payer_firstname'))
		                <span>{{ $errors->first('payer_firstname') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_lastname">Фамилия</label>
		            <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
		            @if($errors->has('payer_lastname'))
		                <span>{{ $errors->first('payer_lastname') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_email">Email</label>
		            <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
		            @if($errors->has('payer_email'))
		                <span>{{ $errors->first('payer_email') }}</span>
		            @endif
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="form-group">
		            <label for="payer_phone">Телефон</label>
		            <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
		            @if($errors->has('payer_phone'))
		                <span>{{ $errors->first('payer_phone') }}</span>
		            @endif
		        </div>
		    </div>
		</div>

		{{-- Submit --}}
		<div class="gap-10"></div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center">
				<button class="btn btn-success btn-block btn-has-icon" type="submit"><i class="fa fa-gavel"></i> Изпрати</button>
			</div>
		</div>
	</form>
</div>

@include('reviews.review-form')

@endsection

@section('vue')
<script src="https://unpkg.com/vue"></script>

<script>
	let app = new Vue({
		el: '#app',
		data: {
			type: '{{ old('type') }}'
		}
	})
</script>

<style>
	.fade-enter-active, .fade-leave-active {
	  transition: opacity .4s
	}
	.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
	  opacity: 0
	}
</style>
@endsection