@extends('layouts.main')

@section('content')
<section id="primary" class="content-full-width">
	<div class="container">
		<h2 class="dt-sc-hr-title">Превод на фирмени документи</h2>

		@if(isset($settings))
		    <h2 class="dt-sc-hr-icon-title">{{ $settings->text }}</h2>
		@endif

		<div class="dt-sc-one-column">
			<div class="dt-sc-one-column">
				<form action="{{ route('store-company-paper-translation') }}" method="POST">
					
                    <div class="dt-sc-hr-invisible-medium"></div>
					<div class="column dt-sc-one-column text-center">
						<button class="dt-sc-button large brown" type="submit" id="submit"> <span class="fa fa-legal"> </span> Изпрати заявка </button>
					</div>

                    @if($settings->price > 0)
                        <div style="display: none;" id="payments">
                            <div class="dt-sc-hr-invisible-medium"></div>
                            <div class="column dt-sc-one-column payments text-center">
                                <h2 class="text-center">
                                    Начин на плащане
                                    <br>
                                    ({{ $settings->price }}) EUR
                                </h2>

                                <div class="column dt-sc-one-fourth first">
                                    <div id="paypal-button"></div>
                                </div>

                                <div class="column dt-sc-one-fourth">
                                    <div class="form-group">
                                        <label for="epay">Epay</label>
                                        <input type="radio" name="payment" value="epay" id="epay">
                                    </div>
                                </div>

                                <div class="column dt-sc-one-fourth">
                                    <div class="form-group">
                                        <label for="econt">Еконт</label>
                                        <input type="radio" name="payment" value="econt" id="econt">
                                    </div>
                                </div>

                                <div class="column dt-sc-one-fourth">
                                    <div class="form-group">
                                        <label for="office">На място в офиса</label>
                                        <input type="radio" name="payment" value="office" id="office">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
				</form>
			</div>
		</div>
	</div>

    @include('reviews.review-form')

</section>
@endsection

@section('scripts')
<script type="text/javascript">
	(function($){
		$('input[name=type]').click(function(event) {
			if ($(this).val() == 'individual') {
				$('#legal-entity-form').hide('150')
				$('#individual-form').show('300')
			}
			else {
				$('#individual-form').hide('150')
				$('#legal-entity-form').show('300')
			}
		})
	})(jQuery)
</script>

{{-- Paypal script --}}
@if($settings->price > 0)
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script type="text/javascript">
    (function ($) {
        $('#submit').click(function(event) {
            event.preventDefault()

            $('#payments').fadeIn('slow')

            var CREATE_PAYMENT_URL  = '{{ route('company-paper-translation-paypal-create') }}'

            var EXECUTE_PAYMENT_URL = '{{ route('company-paper-translation-paypal-execute') }}'

            paypal.Button.render({

                env: 'sandbox', // Or 'sandbox'

                commit: true, // Show a 'Pay Now' button

                payment: function() {
                    return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function(data) {
                        // console.log(data)
                        return data.id
                    })
                },

                // onAuthorize() is called when the buyer approves the payment
                onAuthorize: function(data, actions) {
                    console.log(data)
                    // Set up the data you need to pass to your server
                    var data = {
                        paymentID: data.paymentID,
                        payerID: data.payerID,
                        _token: '{{ csrf_token() }}'
                    };

                    let form_data = $('form').serialize()

                    //Make a POST request with the form data
                    $.ajax({
                        url: '{{ route('store-company-paper-translation') }}',
                        type: 'POST',
                        data: form_data,
                        success: function(response) {
                            // Make a call to your server to execute the payment
                            return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                .then(function (res) {
                                    res = JSON.parse(res)
                                    console.log(res)
                                    
                                    //clear form and show success message
                                    $('form input').each(function(index, el) {
                                        $(this).val('')
                                    })

                                    alert('Заявката ви беше успешно изпратена')
                                })
                        },
                        error: function(data) {
                            let errors = data.responseJSON.errors

                            for(error in errors) {
                                $('input[name='+error+']').after('<span class="help-block">'+errors[error][0]+'</span>')
                            }
                        }
                    })
                }

            }, '#paypal-button')

            console.log('paypal')  
        })
    })(jQuery)
</script>
@endif
@endsection
