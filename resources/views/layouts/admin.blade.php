<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/custom.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>{{ config('app.name') }} | @yield('title', 'Admin')</title>
</head>
<body class="sidebar-mini fixed">
<div class="wrapper">
    <!-- Navbar-->
    <header class="main-header hidden-print"><a class="logo" href="#">{{ config('app.name') }}</a>
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->
                    <li class="dropdown notification-menu"><a class="dropdown-toggle" href="#" data-toggle="dropdown"
                                                              aria-expanded="false"><i
                                    class="fa fa-bell-o fa-lg"></i></a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have 4 new notifications.</li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-primary"></i><i
                                                    class="fa fa-envelope fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-danger"></i><i
                                                    class="fa fa-hdd-o fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li><a class="media" href="javascript:;"><span class="media-left media-icon"><span
                                                class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x text-success"></i><i
                                                    class="fa fa-money fa-stack-1x fa-inverse"></i></span></span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span
                                                class="text-muted block">2min ago</span></div>
                                </a></li>
                            <li class="not-footer"><a href="#">See all notifications.</a></li>
                        </ul>
                    </li>
                    <!-- User Menu-->
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button"
                                            aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out fa-lg"></i> Изход
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image"><img class="img-circle"
                                                  src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg"
                                                  alt="User Image"></div>
                <div class="pull-left info">
                    <p>{{ ucfirst(Auth::user()->name) }}</p>
                    <p class="designation">Админ</p>
                </div>
            </div>
            <!-- Sidebar Menu-->
            <ul class="sidebar-menu">

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-puzzle-piece" aria-hidden="true"></i>
                        <span>Модули</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="{{ route('ltd-registrations') }}">
                                <i class="fa fa-circle-o"></i>
                                Регистрация на ЕООД
                            </a>
                       </li>
                       <li>
                            <a href="{{ route('ood-registrations') }}">
                                <i class="fa fa-circle-o"></i>
                                Регистрация на ООД
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('et-registrations') }}">
                                <i class="fa fa-circle-o"></i>
                                Регистрация на ЕТ
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('list-property-weight') }}">
                                <i class="fa fa-circle-o"></i>
                                Има ли тежести върху имота ми
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('company-revenue') }}">
                                <i class="fa fa-circle-o"></i>
                                Приходи на фирма
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('website-owner') }}">
                                <i class="fa fa-circle-o"></i>
                                Проверка за собтвеността на уеб сайт
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('estate-opening') }}">
                                <i class="fa fa-circle-o"></i>
                                Откриване на имот
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('property-lawsuit') }}">
                                <i class="fa fa-circle-o"></i>
                                Водят ли се съдебни дела за имота ми
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('signature-sample') }}">
                                <i class="fa fa-circle-o"></i>
                                Образец от подпис
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('owner-estates') }}">
                                <i class="fa fa-circle-o"></i>
                                Проверка за недвижими имоти на лице
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('company-fortune') }}">
                                <i class="fa fa-circle-o"></i>
                                Актуално състояние на фирма
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('company-participations') }}">
                                <i class="fa fa-circle-o"></i>
                                Проверка за участие в дружества
                            </a>
                        </li>
                        {{-- <li>
                          <a href="#">
                            <i class="fa fa-circle-o"></i>
                            Превод на фирмени документи
                          </a>
                        </li> --}}
                        <li>
                            <a href="{{ route('switch-management-address') }}">
                                <i class="fa fa-circle-o"></i>
                                Промяна на адрес на управление
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('manager-operations') }}">
                                <i class="fa fa-circle-o"></i>
                                Смяна, добавяне или заличаване на управител
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('change-company-name') }}">
                                <i class="fa fa-circle-o"></i>
                                Промяна на наименование на фирма
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('change-company-activity-subject') }}">
                                <i class="fa fa-circle-o"></i>
                                Промяна на предмета на дейност
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('annual-financial-statements-declarations') }}">
                                <i class="fa fa-circle-o"></i>
                                Обявяване на годишен финансов отчет
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('general-commercial-proxy') }}">
                                <i class="fa fa-circle-o"></i>
                                Генерално търговско пълномощно
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('website-sale-contract') }}">
                                <i class="fa fa-circle-o"></i>
                                Договор за продажба на уеб сайт
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('wedding-contract') }}">
                                <i class="fa fa-circle-o"></i>
                                Брачен договор
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('car-sale-contract') }}">
                                <i class="fa fa-circle-o"></i>
                                Договор за продажба на МПС
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('car-sale-proxy') }}">
                                <i class="fa fa-circle-o"></i>
                                Пълномощно за продажба на МПС
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        <span>Настройки</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($page_settings as $page_setting)
                            <li>
                                <a href="{{ route('page-settings', $page_setting->name) }}">
                                    <i class="fa fa-circle-o"></i>
                                    {{ $page_setting->page_title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{ route('list-hover-balloons') }}">
                        <i class="fa fa-commenting" aria-hidden="true"></i>
                        Ховър балони
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('activities') }}">
                        <i class="fa fa-commenting" aria-hidden="true"></i>
                        Дейности на Фирма
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('list-reviews') }}">
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                        Отзиви
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('banners') }}">
                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                        Банер
                    </a>
                </li>
            </ul>
        </section>
    </aside>
    <div class="content-wrapper">
        <div class="page-title">
            <div>
                <h1><i class="fa fa-@yield('fa-icon', 'home')" aria-hidden="true"></i>@yield('page-name')</h1>
                <p>@yield('sub-page-name')</p>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li>@yield('page-name')</li>
                    <li><a href="#">@yield('sub-page-name')</a></li>
                </ul>
            </div>
        </div>

        @if(Session::has('success'))
            <div class="alert alert-success">
                <strong>{{ Session::get('success') }}</strong>
            </div>
        @endif
        @yield('content')

    </div>
</div>
<!-- Javascripts-->
<script src="{{ asset('js/admin/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin/plugins/pace.min.js') }}"></script>
<script src="{{ asset('js/admin/main.js') }}"></script>
<script src="{{ asset('js/admin/plugins/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/admin/plugins/select2.min.js') }}"></script>

@yield('scripts')

@yield('vue')
</body>
</html>