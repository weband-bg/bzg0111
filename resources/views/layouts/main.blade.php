<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>@yield('title', 'Bezgishe | Начало')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta name="description" content="Rocket — Creative Multipurpose HTML Template - 1.0.0">
    <meta name="author" content="Dan Fisher">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('images/favicons/favicon.ico') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/favicons/favicon-120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicons/favicon-152.png"') }}">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

    <!-- CSS
    ================================================== -->
    <!-- Base + Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">

    <!-- Theme CSS-->
    <link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/nouislider/jquery.nouislider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/royalslider/royalslider.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/royalslider/skins/universal-custom/rs-universal.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- Skin CSS -->
    <!-- <link rel="stylesheet" href="css/skins/red.css"> -->
    
    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    @yield('styles')
    
</head>
<body>

    <!-- Page Loader -->
    <div class="page-loader">
        <div class="loader">
            <div class="dot1"></div>
        <div class="dot2"></div>
        </div>
    </div>
    <!-- End Page Loader -->

    <div class="page-wrapper">
        <div class="top-wrapper top-wrapper__bg1" id="top">
            <!-- Header -->
            <header class="header header__fixed affix-top">

                <div id="header-top-bar" class="h-top-bar h-top-bar__layout1 h-top-bar__color1 h-top-bar__border-between-items h-top-bar__border-between-items__dashed">
                    <div class="container">
                        <div class="h-top-bar_item h-top-bar_item__phone">088888888</div>
                        <div class="h-top-bar_item h-top-bar_item__email">
                            <a href="mailto:bezgishe@gmail.com">bezgishe@gmail.com</a>
                        </div>
                        @auth
                            <div class="h-top-bar_item h-top-bar_item__login">
                                <span>Добре дошъл {{ Auth::user()->name }}</span> |
                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit()">Изход</a>

                                <form action="{{ route('logout') }}" method="POST" id="logout-form" style="display: none;">
                                    {{csrf_field()}}
                                </form>
                            </div>
                        @endauth
                        @guest
                            <div class="h-top-bar_item h-top-bar_item__login">
                                <a href="{{ route('login') }}" title="Login" class="h-top-bar_link h-top-bar_link__sign-in">Вход</a>
                                <span class="h-top-bar_item__login-txt">или</span>
                                <a href="{{ route('register') }}" title="Register" class="h-top-bar_link h-top-bar_link__sign-up"> Регистрация </a>
                            </div>
                        @endguest
                    </div>
                </div>

                <div class="header-main">
                    <div class="container">

                        <!-- Navigation -->
                        <nav class="navbar navbar-default fhmm" role="navigation">

                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <!-- Logo -->
                                <div class="logo">
                                    {{-- <a href="#"><img src="{{ asset('images/logo.png') }}" alt="Rocket"></a>  --}}
                                    <h1><a href="{{ route('home') }}">{{ config('app.name') }}</a></h1>
                                </div>
                                <!-- Logo / End -->
                            </div><!-- end navbar-header -->

                            <div id="main-nav" class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">

                                    <li><a href="{{ route('home') }}">Начало</a></li>

                                    <!-- Elements (Mega Menu) -->
                                    <li class="dropdown active fhmm-fw">
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle disabled dropdown-toggle__caret-holder">Модули</a>
                                        <a class="dropdown-toggle dropdown-toggle__caret"><b class="caret"></b></a>

                                        <ul class="dropdown-menu fullwidth">
                                            <li class="fhmm-content withoutdesc">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <h3 class="title">Форми</h3>
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <ul>
                                                                    <li><a href="{{ route('register-ltd') }}"><i class="fa fa-plus-square"></i> Регистрация на ЕООД</a></li>
                                                                    <li><a href="{{ route('register-ood') }}"><i class="fa fa-plus-square"></i> Регистрация на ООД</a></li>
                                                                    <li><a href="{{ route('register-et') }}"><i class="fa fa-plus-square"></i> Регистрация на ЕТ</a></li>
                                                                    <li><a href="{{ route('property-weight') }}"><i class="fa fa-plus-square"></i> Има ли тежести върху имота ми</a></li>
                                                                    <li><a href="{{ route('store-property-lawsuit') }}"><i class="fa fa-plus-square"></i> Водят ли се съдебни дела за имота ми</a></li>
                                                                    <li><a href="{{ route('store-owner-estate') }}"><i class="fa fa-plus-square"></i> Проверка за недвижими имоти на лице</a></li>
                                                                    <li><a href="{{ route('store-company-participation') }}"><i class="fa fa-plus-square"></i> Проверка за участие в дружества</a></li>
                                                                    <li><a href="{{ route('store-change-company-activity-subject') }}"><i class="fa fa-plus-square"></i> Прoмяна на предмета на дейност</a></li>
                                                                    <li><a href="{{ route('store-general-commercial-proxy') }}"><i class="fa fa-plus-square"></i> Генерално търговско пълномощно</a></li>
                                                                    <li><a href="{{ route('store-wedding-contract') }}"><i class="fa fa-plus-square"></i> Брачен договор</a></li>
                                                                    <li><a href="{{ route('store-car-sale-proxy') }}"><i class="fa fa-plus-square"></i> Пълномощно за продажба на МПС</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <ul>
                                                                    <li><a href="{{ route('store-website-owner') }}"><i class="fa fa-plus-square"></i> Проверка за собствеността на уеб сайт</a></li>
                                                                    <li><a href="{{ route('store-estate-opening') }}"><i class="fa fa-plus-square"></i> Откриване на имот</a></li>
                                                                    <li><a href="{{ route('store-signature-sample') }}"><i class="fa fa-plus-square"></i> Обрзаец от подпис</a></li>
                                                                    <li><a href="{{ route('store-company-fortune') }}"><i class="fa fa-plus-square"></i> Актуално състояние на фирма</a></li>
                                                                    <li><a href="{{ route('store-switch-management-address') }}"><i class="fa fa-plus-square"></i> Промяна на адрес на управление</a></li>
                                                                    <li><a href="{{ route('store-manager-operation') }}"><i class="fa fa-plus-square"></i> Смяна добавяне или заличаване на управител</a></li>
                                                                    <li><a href="{{ route('store-company-revenue') }}"><i class="fa fa-plus-square"></i> Приходи на фирма</a></li>
                                                                    <li><a href="{{ route('store-change-company-name') }}"><i class="fa fa-plus-square"></i> Промяна на наименование на фирма</a></li>
                                                                    <li><a href="{{ route('store-annual-financial-statements-declaration') }}"><i class="fa fa-plus-square"></i> Обявяване на годишен финансов отчет</a></li>
                                                                    <!-- <li><a href="{{ route('store-company-paper-translation') }}"><i class="fa fa-plus-square"></i> Превод на фирмени документи</a></li> -->
                                                                    <li><a href="{{ route('store-website-sale-contract') }}"><i class="fa fa-plus-square"></i> Договор за продажба на уеб сайт</a></li>
                                                                    <li><a href="{{ route('store-car-sale-contract') }}"><i class="fa fa-plus-square"></i> Договор за продажба на МПС</a></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- end row -->
                                            </li><!-- end grid demo -->
                                        </ul><!-- end drop down menu -->
                                    </li>
                                    <!-- Elements (Mega Menu) / End -->

                                    <li><a href="gallery-main.html">Как работим</a></li>
    
                                    <li>
                                        <a href="#">Отзиви</a>
                                    </li>
                                </ul><!-- end nav navbar-nav -->
                            </div><!-- end #main-nav -->

                        </nav><!-- end navbar navbar-default fhmm -->
                        <!-- Navigation / End -->
                    </div>
                </div>
                
            </header>
            <!-- Header / End -->
        </div>


        <div class="content-wrapper">
            
            <!-- Page Section -->
            <section class="page-section">
                <div class="container">

                    
                        <div class="section-title-wrapper">
                            <div class="section-title-inner">
                                <h1 class="section-title section-title__lg">@yield('page_title')</h1>
                                @if(!isset($noBreadcrumbs))
                                    <ol class="breadcrumb">
                                        <li><a href="{{ route('home') }}">Начало</a></li>
                                        <li><a href="#">@yield('parent_page', 'Модули')</a></li>
                                        <li class="active">@yield('page_title')</li>
                                    </ol>
                                @endif
                            </div>
                        </div>

                    @yield('content')

                </div>
            </section>
        </div>

        <!-- Footer -->
        <footer class="footer page-section">
            <div class="container">
                <div class="footer-inner">

                    <!-- Footer Social Links -->
                    <ul class="footer-social-links footer-social-links__rounded list-inline text-center">
                        <li>
                            <a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" title="VK"><i class="fa fa-vk"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Instagram"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Github"><i class="fa fa-github"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Youtube"><i class="fa fa-youtube-play"></i></a>
                        </li>
                    </ul>
                    <!-- Footer Social Links / End -->

                    <!-- Footer Text -->
                    <div class="footer-text text-center">
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                &copy; All rights reserved
                            </div>
                            <div class="col-sm-4 col-md-4">
                                thanks for watching
                            </div>
                            <div class="col-sm-4 col-md-4">
                                Made with <i class="fa fa-heart"></i> by <a href="#">weband</a> 
                            </div>
                        </div>
                    </div>
                    <!-- Footer Text / End -->

                </div>
            </div>
        </footer>
        <!-- Footer / End -->
        

    </div>


    <!-- Back to Top -->
    <div id="back-top">
        <div class="link-holder scroll-local">
            <a href="#top" class="top-link"><i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
    <!-- Back to Top / End -->

    @yield('vue')

    <!-- Javascript Files
    ================================================== -->
    <script src="{{ asset('vendor/jquery/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/bootstrap-scripts.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/fhmm.js') }}"></script>
    <script src="{{ asset('vendor/jquery.flickrfeed.js') }}"></script>
    <script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('vendor/isotope/jquery.imagesloaded.min.js') }}"></script>
    <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('vendor/nouislider/jquery.nouislider.all.min.js') }}"></script>
    <script src="{{ asset('vendor/royalslider/jquery.royalslider.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('vendor/jcountdown/jquery.jcountdown.js') }}"></script>
    <script src="{{ asset('vendor/wow.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.countTo.js') }}"></script>
    <script src="{{ asset('vendor/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.localScroll.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.viewport.mini.js') }}"></script>

    <!-- Google Map -->
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="{{ asset('vendor/jquery.gmap3.min.js') }}"></script>

    <script src="{{ asset('js/custom.js') }}"></script>

    @if(isset($module))
        <script type="text/javascript">
            $(document).ready(function() {
                let hovers = JSON.parse('{!! $module->hoverBalloons->toJson() !!}')

                console.log(hovers) 

                let element = '';

                for(let hover of hovers) {
                    element = $('input[name="'+hover.form_field+'"], select[name="'+hover.form_field+'"], textarea[name="'+hover.form_field+'"]')
                    // Search for an input with the same name or id as the form_field
                    if (element.length > 0) {
                        let id = element.attr('id')
                        $('label[for="'+id+'"]').text(hover.label)                   
                    }
                    else {
                        element = $('input#'+hover.form_field + ', select#'+hover.form_field + ', textarea#'+hover.form_field)

                        // If the element is radio button change the span next to it
                        if (element.attr('type') == 'radio') {
                            element.next('span').text(hover.label)
                        }
                        else {
                            $('label[for="'+hover.form_field+'"]').text(hover.label)
                        }
                    }

                    // Add tooltip
                    if (hover.hover_text) {
                        // if radio add hover to the span next to it
                        if (element.attr('type') == 'radio') {
                            element.next('span').attr('data-toggle', 'tooltip')
                            element.next('span').attr('title', hover.hover_text)
                        }

                        element.attr('data-toggle', 'tooltip')
                        element.attr('title', hover.hover_text)
                    } 
                }

                // setTimeout(function() {
                //     for(let hover of hovers) {
                //         // Search for an input with the same name or id as the form_field
                //         if ($('input[name="'+hover.form_field+'"]').length > 0) {
                //             let id = $('input[name="'+hover.form_field+'"]').attr('id')
                //             $('label[for="'+id+'"]').text(hover.label)
                //         }
                //         else {
                //             $('input#'+hover.form_field)
                //             $('label[for="'+hover.form_field+'"]').text(hover.label)
                //         }
                //     }
                // }, 1000)

                $('[data-toggle="tooltip"]').tooltip(); 
            })
        </script>
    @endif

    @yield('scripts')
</body>
</html>