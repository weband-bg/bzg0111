{{-- Review --}}
<div class="gap-40"></div>
<h3 class="text-center">Остави отзив</h3>
<form action="{{ route('store-review') }}" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="page" value="{{ $settings->id }}">

	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="review_name">Име</label>
				<input type="text" class="form-control" id="review_name" name="review_name" value="{{ old('review_name') }}">
				@if($errors->has('review_name'))
					<span>{{ $errors->first('review_name') }}</span>
				@endif
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="review_email">Email</label>
				<input type="email" class="form-control" id="review_email" name="review_email" value="{{ old('review_email') }}">
				@if($errors->has('review_email'))
					<span>{{ $errors->first('review_email') }}</span>
				@endif
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="review_phone">Телефон</label>
				<input type="tel" class="form-control" id="review_phone" name="review_phone" value="{{ old('review_phone') }}">
				@if($errors->has('review_phone'))
					<span>{{ $errors->first('review_phone') }}</span>
				@endif
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<label for="review_text">Отзив</label>
				<textarea name="review_text" id="review_text" class="form-control" rows="5">{{ old('review_text') }}</textarea>
			</div>
		</div>
	</div>

	{{-- Submit --}}
	<div class="gap-10"></div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">
			<button class="btn btn-success btn-block" type="submit"> Изпрати отзив</button>
		</div>
	</div>
</form>