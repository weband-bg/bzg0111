@extends('layouts.main')

@section('title', 'Отзиви')

@section('content')
<section id="primary" class="content-full-width">
	<div class="container">
		<h2 class="dt-sc-hr-title"> Отзиви </h2>

		@foreach($reviews as $review)
			<div class="dt-sc-one-column">
				<blockquote> 
				    <q>{{ $review->text }}
					<br>
				    - {{ $review->getName() }}</q> 
				</blockquote>
			</div>
			<div class="dt-sc-hr-invisible-medium"></div>
		@endforeach

		<form action="{{ route('store-review') }}" method="POST">
			{{csrf_field()}}
			@if(Auth::guest())
				<div class="column dt-sc-one-half first">
					<label for="name">Име</label>
					<input type="text" id="name" name="name" value="{{ old('name') }}">
					@if($errors->has('name'))
						<span class="help-bock">{{ $errors->first('name') }}</span>
					@endif
				</div>
				<div class="column dt-sc-one-half">
					<label for="email">Email</label>
					<input type="email" id="email" name="email" value="{{ old('email') }}">
					@if($errors->has('email'))
						<span class="help-block">{{ $errors->first('email') }}</span>
					@endif
				</div>
			@endif
			<div class="dt-sc-one-column">
				<label for="review">Напиши отзив</label>
				<textarea name="review" id="review" rows="5">{{ old('review') }}</textarea>
				@if($errors->has('review'))
					<span class="help-block">{{ $errors->first('review') }}</span>
				@endif
			</div>
			<div class="dt-sc-hr-invisible-medium"></div>
			<div class="column dt-sc-one-column">
				<button class="dt-sc-button large" type="submit"> <span class="fa fa-pencil"> </span> Изпрати отзив </button>
			</div>
		</form>
	</div>
</section>
@endsection