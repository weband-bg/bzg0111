@extends('layouts.main')


@section('title', 'Промяна на наименование на фирма')
@section('page_title', 'Промяна на наименование на фирма')

@section('content')
{{-- Success --}}
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade in text-uppercase">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        <strong>{{ Session::get('success') }}</strong>
    </div>
@endif

{{-- Error --}}
@if(Session::has('error'))
<div class="alert alert-danger alert-dismissible fade in text-uppercase">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
    <strong>{{ Session::has('error') }}</strong>
</div>
@endif

    @if(isset($settings))
        <h2>{{ $settings->text }}</h2>
    @endif


    <form action="{{ route('store-change-company-name') }}" method="POST">
        {{csrf_field()}}
        <h3>Юридическо лице</h3>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="current_company_name">Настоящо име на фирма</label>
                    <input type="text" name="current_company_name" id="current_company_name" class="form-control" value="{{ old('current_company_name') }}" required>ЕООД/ООД/ЕТ
                    @if($errors->has('current_company_name'))
                    <span class="help-block">{{ $errors->first('current_company_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label for="eik">ЕИК на фирма</label>
                    <input type="text" name="eik" id="eik" class="form-control" value="{{ old('eik') }}" required>
                    @if($errors->has('eik'))
                    <span class="help-block">{{ $errors->first('eik') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="new_company_name">Ново име на фирмата</label>
                    <input type="text" id="new_company_name" class="form-control" name="new_company_name" value="{{ old('new_company_name') }}" required>ЕООД/ООД/ЕТ
                    <p>
                        <a href="https://public.brra.bg/CheckUps/Verifications/RightsOverCompany.ra"
                           target="_blank">Проверете дали избраното от вас име не е заето</a>
                    </p>
                    @if($errors->has('new_company_name'))
                    <span class="help-block">{{ $errors->first('new_company_name') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3 col-md-offset-3">
                <div class="form-group">
                    <label for="company-foreign-language-name">Ново име на фирмата на чужд език</label>
                    <input type="text" id="new_company_foreign_name" class="form-control" name="new_company_foreign_name"
                           value="{{ old('new_company_foreign_name') }}" required>LTD
                    @if($errors->has('new_company_foreign_name'))
                    <span class="help-block">{{ $errors->first('new_company_foreign_name') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <div class="text-center">
                <button type="submit" id="additional-info-button" class="btn btn-success"> <span class="fa fa-legal"> </span> Потвърди заявка </button>
                <p>100 лв. с вкл. държавна, банкова такса и хонорар</p>
            </div>
        </div>

        <div class="additional-info" style="display: none">
            <div class="col-md-12 additional-info" style="display: none">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="owner_firstname">Име</label>
                        <input type="text" id="owner_firstname" class="form-control" name="owner_firstname" value="{{ old('owner_firstname') }}">
                        @if($errors->has('owner_firstname'))
                        <span class="help-block">{{ $errors->first('owner_firstname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="owner-second-name">Презиме</label>
                        <input type="text" id="owner-second-name" class="form-control" name="owner_secondname" value="{{ old('owner_secondname') }}">
                        @if($errors->has('owner_secondname'))
                        <span class="help-block">{{ $errors->first('owner_secondname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="owner-lastname">Фамилия</label>
                        <input type="text" id="owner-lastname" class="form-control" name="owner_lastname" value="{{ old('owner_lastname') }}">
                        @if($errors->has('owner_lastname'))
                        <span class="help-block">{{ $errors->first('owner_lastname') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12 additional-info" style="display: none">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="email">Е-мейл</label>
                        <input type="text" class="owner_email form-control" name="owner_email" value="{{ old('owner_email') }}">
                        @if($errors->has('owner_email'))
                        <span class="help-block">{{ $errors->first('owner_email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="owner_id_expiry_date">Дата на валидност на лична карта</label>
                        <input type="text" class="owner_id_expiry_date form-control" name="owner_id_expiry_date" value="{{ old('owner_id_expiry_date') }}">
                        @if($errors->has('owner_id_expiry_date'))
                        <span class="help-block">{{ $errors->first('owner_id_expiry_date') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="phone">Телефон</label>
                        <input type="text" class="owner_phone form-control" name="owner_phone" value="{{ old('owner_phone') }}">
                        @if($errors->has('owner_phone'))
                        <span class="help-block">{{ $errors->first('owner_phone') }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h3 class="text-center">Начин на плащане</h3>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="office" value="office" checked>
                        <span>На място в офиса</span>
                    </label>
                </div>
                <div class="radio radio__custom radio__style1">
                    <label>
                        <input type="radio" name="payment_method" id="econt" value="econt">
                        <span>Еконт</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <h3 class="text-center">Вашата информация</h3>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_firstname">Име</label>
                    <input type="text" class="form-control" id="payer_firstname" name="payer_firstname" value="{{ old('payer_firstname') }}">
                    @if($errors->has('payer_firstname'))
                        <span>{{ $errors->first('payer_firstname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_lastname">Фамилия</label>
                    <input type="text" class="form-control" id="payer_lastname" name="payer_lastname" value="{{ old('payer_lastname') }}">
                    @if($errors->has('payer_lastname'))
                        <span>{{ $errors->first('payer_lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_email">Email</label>
                    <input type="email" class="form-control" id="payer_email" name="payer_email" value="{{ old('payer_email') }}">
                    @if($errors->has('payer_email'))
                        <span>{{ $errors->first('payer_email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="payer_phone">Телефон</label>
                    <input type="tel" class="form-control" id="payer_phone" name="payer_phone" value="{{ old('payer_phone') }}">
                    @if($errors->has('payer_phone'))
                        <span>{{ $errors->first('payer_phone') }}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-12 text-center additional-info" style="display: none">
            <button class="btn btn-success" type="submit" id="submit"> <span class="fa fa-legal"> </span> Завърши заявка</button>
        </div>

       {{--  @if($settings->price > 0)
            <div style="display: none;" id="payments">
                <div class="dt-sc-hr-invisible-medium"></div>
                <div class="column dt-sc-one-column payments text-center">
                    <h2 class="text-center">
                        Начин на плащане
                        <br>
                        ({{ $settings->price }}) EUR
                    </h2>

                    <div class="column dt-sc-one-fourth first">
                        <div id="paypal-button"></div>
                    </div>

                    <div class="column dt-sc-one-fourth">
                        <div class="form-group">
                            <label for="epay">Epay</label>
                            <input type="radio" name="payment" value="epay" id="epay">
                        </div>
                    </div>

                    <div class="column dt-sc-one-fourth">
                        <div class="form-group">
                            <label for="econt">Еконт</label>
                            <input type="radio" name="payment" value="econt" id="econt">
                        </div>
                    </div>

                    <div class="column dt-sc-one-fourth">
                        <div class="form-group">
                            <label for="office">На място в офиса</label>
                            <input type="radio" name="payment" value="office" id="office">
                        </div>
                    </div>
                </div>
            </div>
        @endif --}}
    </form>

    @include('reviews.review-form')

@endsection

@section('scripts')
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript">
(function ($) {
    var submitButton = $('#submit');
    var additionalInfoButton = $('#additional-info-button');
    var additionalInfoDiv = $('.additional-info');
    var operations = $('#operations');

    var addManagerOperation = $('#add-manager-operation');
    var removeManagerOperation = $('#remove-manager-operation');
    var switchManagerOperation = $('#switch-manager-operation');

    additionalInfoButton.click(function (event) {
        event.preventDefault();

        additionalInfoDiv.fadeIn('slow');

    });

    operations.on('change', function (event) {
        const selectedOperation = event.target.value;

        addManagerOperation.hide();
        removeManagerOperation.hide();
        switchManagerOperation.hide();

        switch (selectedOperation) {
            case '1':
                addManagerOperation.fadeIn('slow');
                break;
            case '2':
                removeManagerOperation.fadeIn('slow');
                break;
            case '3':
                switchManagerOperation.fadeIn('slow');
                break;
            default:
                break;
        }
    });

    submitButton.click(function (event) {
        // handle error
        //event.preventDefault();



    });


    $('[data-toggle="tooltip"]').tooltip();
})(jQuery)
</script>

{{-- Paypal script --}}
@if($settings->price > 0)
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script type="text/javascript">
    (function ($) {
        $('#submit').click(function(event) {
            event.preventDefault()

            $('#payments').fadeIn('slow')

            var CREATE_PAYMENT_URL  = '{{ route('change-company-name-paypal-create') }}'

            var EXECUTE_PAYMENT_URL = '{{ route('change-company-name-subject-paypal-execute') }}'

            paypal.Button.render({

                env: 'sandbox', // Or 'sandbox'

                commit: true, // Show a 'Pay Now' button

                payment: function() {
                    return paypal.request.post(CREATE_PAYMENT_URL, {_token: '{{ csrf_token() }}'}).then(function(data) {
                        // console.log(data)
                        return data.id
                    })
                },

                // onAuthorize() is called when the buyer approves the payment
                onAuthorize: function(data, actions) {
                    console.log(data)
                    // Set up the data you need to pass to your server
                    var data = {
                        paymentID: data.paymentID,
                        payerID: data.payerID,
                        _token: '{{ csrf_token() }}'
                    };

                    let form_data = $('form').serialize()

                    //Make a POST request with the form data
                    $.ajax({
                        url: '{{ route('store-change-company-name') }}',
                        type: 'POST',
                        data: form_data,
                        success: function(response) {
                            // Make a call to your server to execute the payment
                            return paypal.request.post(EXECUTE_PAYMENT_URL, data)
                                .then(function (res) {
                                    res = JSON.parse(res)
                                    console.log(res)
                                    
                                    //clear form and show success message
                                    $('form input').each(function(index, el) {
                                        $(this).val('')
                                    })

                                    alert('Заявката ви беше успешно изпратена')
                                })
                        },
                        error: function(data) {
                            let errors = data.responseJSON.errors

                            for(error in errors) {
                                $('input[name='+error+']').after('<span class="help-block">'+errors[error][0]+'</span>')
                            }
                        }
                    })
                }

            }, '#paypal-button')

            console.log('paypal')  
        })
    })(jQuery)
</script>
@endif
@endsection

