<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
// API
Route::group(['prefix' => 'api'], function () {
    Route::get('/zones', 'ZonesController@getAll')->name('zones-json');
    Route::get('/zone/{id}/townships', 'ZonesController@getTownships')->name('townships-json');
    Route::get('/township/{id}/populated-places', 'ZonesController@getPopulatedPlaces')->name('populated-places-json');
    Route::get('/activities', 'ActivitiesController@getAll')->name('get-activities');
});


Route::get('/', 'PageController@index')->name('home');

Auth::routes();

//Company registration
Route::get('/company-registration', 'CompanyRegistrationController@index')->name('company-registration');

// LTD (EOOD)
Route::get('/company-registration/eood', 'CompanyRegistrationController@registerLTD')->name('register-ltd');
Route::post('/company-registration/eood/store', 'CompanyRegistrationController@storeLTD')->name('store-ltd');

// LTD (OOD)
Route::get('/company-registration/ood', 'CompanyRegistrationController@registerOOD')->name('register-ood');
Route::post('/company-registration/ood/store', 'CompanyRegistrationController@storeOOD')->name('store-ood');

// ET
Route::get('/company-registration/et', 'CompanyRegistrationController@registerET')->name('register-et');
Route::post('/company-registration/et/store', 'CompanyRegistrationController@storeET')->name('store-et');

// Reviews
Route::get('/reviews', 'ReviewsController@index')->name('reviews');
Route::post('/reviews/store', 'ReviewsController@store')->name('store-review');

// Facebook login
Route::get('/login/facebook', 'Auth\LoginController@redirectToProvider')->name('facebook-login');
Route::get('/login/facebook/callback', 'Auth\LoginController@handleProviderCallback')->name('facebook-callback');

// Property weight
Route::get('/property-weight', 'PropertyWeightController@index')->name('property-weight');
Route::post('/property-weight', 'PropertyWeightController@store')->name('store-property-weight');

// Property lawsuits
Route::get('/property-lawsuit', 'PropertyLawsuitsController@create')->name('store-property-lawsuit');
Route::post('/property-lawsuit', 'PropertyLawsuitsController@store')->name('store-property-lawsuit');

// Owner estates
Route::get('/owner-estates', 'OwnerEstatesController@create')->name('store-owner-estate');
Route::post('/owner-estates', 'OwnerEstatesController@store')->name('store-owner-estate');

// Company participations
Route::get('/company-participations', 'CompanyParticipationsController@create')->name('store-company-participation');
Route::post('/company-participations', 'CompanyParticipationsController@store')->name('store-company-participation');

// Company revenue
Route::get('/company-revenue', 'CompanyRevenueController@create')->name('store-company-revenue');
Route::post('/company-revenue', 'CompanyRevenueController@store')->name('store-company-revenue');

// Website owner
Route::get('/website-owner', 'WebsiteOwnerController@create')->name('store-website-owner');
Route::post('/website-owner', 'WebsiteOwnerController@store')->name('store-website-owner');

// Estate opening
Route::get('/estate-opening', 'EstateOpeningController@create')->name('store-estate-opening');
Route::post('/estate-opening', 'EstateOpeningController@store')->name('store-estate-opening');

// Signature sample
Route::get('/signature-sample', 'SignatureSampleController@create')->name('store-signature-sample');
Route::post('/signature-sample', 'SignatureSampleController@store')->name('store-signature-sample');

// Company fortune
Route::get('/company-fortune', 'CompanyFortuneController@create')->name('store-company-fortune');
Route::post('/company-fortune', 'CompanyFortuneController@store')->name('store-company-fortune');

// Company paper translation
Route::get('/company-paper-translation', 'CompanyPaperTranslationController@create')->name('store-company-paper-translation');
Route::post('/company-paper-translation', 'CompanyPaperTranslationController@store')->name('store-company-paper-translation');

// Switch management address
Route::get('/switch-management-address', 'SwitchManagementAddressController@create')->name('store-switch-management-address');
Route::post('/switch-management-address', 'SwitchManagementAddressController@store')->name('store-switch-management-address');

// Manager operation
Route::get('/manager-operation', 'ManagerOperationController@create')->name('store-manager-operation');
Route::post('/manager-operation', 'ManagerOperationController@store')->name('store-manager-operation');

// Change company activity subject
Route::get('/change-company-activity-subject', 'ChangeCompanyActivitySubjectController@create')->name('store-change-company-activity-subject');
Route::post('/change-company-activity-subject', 'ChangeCompanyActivitySubjectController@store')->name('store-change-company-activity-subject');

// Change company name
Route::get('/company-name', 'ChangeCompanyNameController@create')->name('store-change-company-name');
Route::post('/company-name', 'ChangeCompanyNameController@store')->name('store-change-company-name');

// Annual financial statements declaration
Route::get('/annual-financial-statements-declaration', 'AnnualFinancialStatementsDeclarationController@create')->name('store-annual-financial-statements-declaration');
Route::post('/annual-financial-statements-declaration', 'AnnualFinancialStatementsDeclarationController@store')->name('store-annual-financial-statements-declaration');

// General commercial proxy
Route::get('/general-commercial-proxy', 'GeneralCommercialProxyController@create')->name('store-general-commercial-proxy');
Route::post('/general-commercial-proxy', 'GeneralCommercialProxyController@store')->name('store-general-commercial-proxy');

// Website sale contract
Route::get('/website-sale-contract', 'WebsiteSaleContractController@create')->name('store-website-sale-contract');
Route::post('/website-sale-contract', 'WebsiteSaleContractController@store')->name('store-website-sale-contract');

// Wedding contract
Route::get('/wedding-contract', 'WeddingContractController@create')->name('store-wedding-contract');
Route::post('/wedding-contract', 'WeddingContractController@store')->name('store-wedding-contract');


// Paypal
Route::post('/annual-financial-statements-declaration-payment-create', 'AnnualFinancialStatementsDeclarationController@paypalCreate')->name('annual-financial-statements-declaration-paypal-create');
Route::post('/annual-financial-statements-declaration-payment-execute', 'AnnualFinancialStatementsDeclarationController@paypalExecute')->name('annual-financial-statements-declaration-paypal-execute');

Route::post('/change-company-activity-subject-paypal-create', 'ChangeCompanyActivitySubjectController@paypalCreate')->name('change-company-activity-subject-paypal-create');
Route::post('/change-company-activity-subject-paypal-execute', 'ChangeCompanyActivitySubjectController@paypalExecute')->name('change-company-activity-subject-paypal-execute');

Route::post('/change-company-name-paypal-create', 'ChangeCompanyNameController@paypalCreate')->name('change-company-name-paypal-create');
Route::post('/change-company-name-paypal-execute', 'ChangeCompanyNameController@paypalExecute')->name('change-company-name-paypal-execute');

Route::post('/company-fortune-paypal-create', 'CompanyFortuneController@paypalCreate')->name('company-fortune-paypal-create');
Route::post('/company-fortune-paypal-execute', 'CompanyFortuneController@paypalExecute')->name('company-fortune-paypal-execute');

Route::post('/company-paper-translation-paypal-create', 'CompanyPaperTranslationController@paypalCreate')->name('company-paper-translation-paypal-create');
Route::post('/company-paper-translation-paypal-execute', 'CompanyPaperTranslationController@paypalExecute')->name('company-paper-translation-paypal-execute');

Route::post('/company-participations-paypal-create', 'CompanyParticipationsController@paypalCreate')->name('company-participations-paypal-create');
Route::post('/company-participations-paypal-execute', 'CompanyParticipationsController@paypalExecute')->name('company-participations-paypal-execute');

Route::post('/company-revenue-paypal-create', 'CompanyRevenueController@paypalCreate')->name('company-revenue-paypal-create');
Route::post('/company-revenue-paypal-execute', 'CompanyRevenueController@paypalExecute')->name('company-revenue-paypal-execute');

Route::post('/estate-opening-paypal-create', 'EstateOpeningController@paypalCreate')->name('estate-opening-paypal-create');
Route::post('/estate-opening-paypal-execute', 'EstateOpeningController@paypalExecute')->name('estate-opening-paypal-execute');

Route::post('/general-commercial-proxy-paypal-create', 'GeneralCommercialProxyController@paypalCreate')->name('general-commercial-proxy-paypal-create');
Route::post('/general-commercial-proxy-paypal-execute', 'GeneralCommercialProxyController@paypalExecute')->name('general-commercial-proxy-paypal-execute');

Route::post('/manager-operation-paypal-create', 'ManagerOperationController@paypalCreate')->name('manager-operation-paypal-create');
Route::post('/manager-operation-paypal-execute', 'ManagerOperationController@paypalExecute')->name('manager-operation-paypal-execute');

Route::post('/owner-estates-paypal-create', 'OwnerEstatesController@paypalCreate')->name('owner-estates-paypal-create');
Route::post('/owner-estates-paypal-execute', 'OwnerEstatesController@paypalExecute')->name('owner-estates-paypal-execute');

Route::post('/property-lawsuit-paypal-create', 'PropertyLawsuitsController@paypalCreate')->name('property-lawsuit-paypal-create');
Route::post('/property-lawsuit-paypal-execute', 'PropertyLawsuitsController@paypalExecute')->name('property-lawsuit-paypal-execute');

Route::post('/property-weight-paypal-create', 'PropertyWeightController@paypalCreate')->name('property-weight-paypal-create');
Route::post('/property-weight-paypal-execute', 'PropertyWeightController@paypalExecute')->name('property-weight-paypal-execute');

Route::post('/signature-sample-paypal-create', 'SignatureSampleController@paypalCreate')->name('signature-sample-paypal-create');
Route::post('/signature-sample-paypal-execute', 'SignatureSampleController@paypalExecute')->name('signature-sample-paypal-execute');

Route::post('/switch-management-address-paypal-create', 'SwitchManagementAddressController@paypalCreate')->name('switch-management-address-paypal-create');
Route::post('/switch-management-address-paypal-execute', 'SwitchManagementAddressController@paypalExecute')->name('switch-management-address-paypal-execute');

Route::post('/website-owner-paypal-create', 'WebsiteOwnerController@paypalCreate')->name('website-owner-paypal-create');
Route::post('/website-owner-paypal-execute', 'WebsiteOwnerController@paypalExecute')->name('website-owner-paypal-execute');

Route::post('/website-sale-contract-paypal-create', 'WebsiteSaleContractController@paypalCreate')->name('website-sale-contract-paypal-create');
Route::post('/website-sale-contract-paypal-execute', 'WebsiteSaleContractController@paypalExecute')->name('website-sale-contract-paypal-execute');

Route::post('/wedding-contract-paypal-create', 'WeddingContractController@paypalCreate')->name('wedding-contract-paypal-create');
Route::post('/wedding-contract-paypal-execute', 'WeddingContractController@paypalExecute')->name('wedding-contract-paypal-execute');

Route::post('/company-ltd-paypal-create', 'CompanyRegistrationController@ltdPaypalCreate')->name('company-ltd-paypal-create');
Route::post('/company-ltd-paypal-execute', 'CompanyRegistrationController@ltdPaypalExecute')->name('company-ltd-paypal-execute');

Route::post('/company-ood-paypal-create', 'CompanyRegistrationController@oodPaypalCreate')->name('company-ood-paypal-create');
Route::post('/company-ood-paypal-execute', 'CompanyRegistrationController@oodPaypalExecute')->name('company-ood-paypal-execute');

Route::post('/company-et-paypal-create', 'CompanyRegistrationController@etPaypalCreate')->name('company-et-paypal-create');
Route::post('/company-et-paypal-execute', 'CompanyRegistrationController@etPaypalExecute')->name('company-et-paypal-execute');

// Car sale contract
Route::get('/car-sale-contract', 'CarSaleContractController@create')->name('store-car-sale-contract');
Route::post('/car-sale-contract', 'CarSaleContractController@store')->name('store-car-sale-contract');

// Car sale proxy
Route::get('/car-sale-proxy', 'CarSaleProxyController@create')->name('store-car-sale-proxy');
Route::post('/car-sale-proxy', 'CarSaleProxyController@store')->name('store-car-sale-proxy');

// How we work
Route::get('how-we-work', 'HomeController@about')->name('about');

// Admin
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin-home');

    //Company registration
    // EOOD
    Route::get('/companies/ltd', 'CompanyRegistrationController@adminIndex')->name('ltd-registrations');
    Route::post('/companies/ltd/delete', 'CompanyRegistrationController@deleteLtd')->name('delete-company-ltd');

    // ODO
    Route::get('/companies/ood', 'CompanyRegistrationController@adminOODIndex')->name('ood-registrations');
    Route::post('/companies/odd/delete', 'CompanyRegistrationController@deleteOOD')->name('delete-company-ood');

    // ET
    Route::get('/companies/et', 'CompanyRegistrationController@adminETIndex')->name('et-registrations');
    Route::post('/companies/et/delete', 'CompanyRegistrationController@deleteET')->name('delete-company-et');

    // Property weight
    Route::get('/property-weight/list', 'PropertyWeightController@list')->name('list-property-weight');
    Route::post('/property-weight/delete', 'PropertyWeightController@delete')->name('delete-property-weight');

    //Settings
    Route::get('/settings/categories', 'SettingsController@getCategories')->name('settings-categories');
    Route::get('/settings/{page}', 'SettingsController@index')->name('page-settings');
    Route::post('/settings/store', 'SettingsController@store')->name('store-page-settings');
    Route::get('/settings/question-answers/{id}', 'SettingsController@getQuestionAnsers')->name('get-question-answers');

    //Hover balloons
    Route::get('/hover-balloons', 'HoverBalloonsController@list')->name('list-hover-balloons');
    Route::get('/hover-balloons/show/{id}', 'HoverBalloonsController@show')->name('show-hover-balloons');
    Route::get('/hover-balloons/{id}', 'HoverBalloonsController@edit')->name('edit-hover-balloon');
    Route::post('/hover-balloons/{id}', 'HoverBalloonsController@update')->name('edit-hover-balloon');
    // Route::get('/hover-balloons/ltd', 'HoverBalloonsController@ltdRegistration')->name('ltd-hover-balloons');
    // Route::post('/hover-balloons/ltd', 'HoverBalloonsController@storeLtdRegistration')->name('store-ltd-hover-ballons');

    //Reviews
    Route::get('/reviews', 'ReviewsController@list')->name('list-reviews');
    Route::post('/reviews/delete', 'ReviewsController@delete')->name('delete-review');
    Route::post('/reviews/change-approved', 'ReviewsController@changeApproved')->name('review-change-approved');

    //Company revenue
    Route::get('/company-revenue', 'CompanyRevenueController@adminIndex')->name('company-revenue');
    Route::post('/company-revenue/delete', 'CompanyRevenueController@delete')->name('delete-revenue');

    // Website owner
    Route::get('/website-owner', 'WebsiteOwnerController@adminIndex')->name('website-owner');
    Route::post('/website-owner/delete', 'WebsiteOwnerController@delete')->name('delete-website');

    // Estate opening
    Route::get('/estate-opening', 'EstateOpeningController@adminIndex')->name('estate-opening');
    Route::post('/estate-opening/delete', 'EstateOpeningController@delete')->name('delete-estate');

    //Property lawsuits
    Route::get('/property-lawsuit', 'PropertyLawsuitsController@adminIndex')->name('property-lawsuit');
    Route::post('/property-lawsuit/delete', 'PropertyLawsuitsController@delete')->name('delete-property-lawsuit');

    //Signature sample
    Route::get('/signature-sample', 'SignatureSampleController@adminIndex')->name('signature-sample');
    Route::post('/signature-sample/delete', 'SignatureSampleController@delete')->name('delete-signature-sample');

    //Owner estates
    Route::get('/owner-estates', 'OwnerEstatesController@adminIndex')->name('owner-estates');
    Route::post('/owner-estate/delete', 'OwnerEstatesController@delete')->name('delete-owner-estate');

    // Company fortune
    Route::get('/company-fortune', 'CompanyFortuneController@adminIndex')->name('company-fortune');
    Route::post('/company-fortune/delete', 'CompanyFortuneController@delete')->name('delete-company-fortune');

    // Company participations
    Route::get('/company-participations', 'CompanyParticipationsController@adminIndex')->name('company-participations');
    Route::post('/company-participations/delete', 'CompanyParticipationsController@delete')->name('delete-company-participation');

    // Switch managmetn address
    Route::get('/switch-management-address', 'SwitchManagementAddressController@adminIndex')->name('switch-management-address');
    Route::post('/switch-management-address/delete', 'SwitchManagementAddressController@delete')->name('delete-switch');

    // Manager operations
    Route::get('/manager-operations', 'ManagerOperationController@adminIndex')->name('manager-operations');
    Route::post('/manager-operation/delete', 'ManagerOperationController@delete')->name('delete-manager-operation');

    // Change company activity subject
    Route::get('/change-company-activity-subject', 'ChangeCompanyActivitySubjectController@adminIndex')->name('change-company-activity-subject');
    Route::post('/change-company-activity-subject/delete', 'ChangeCompanyActivitySubjectController@delete')->name('delete-company-activity-subject');

    // Change company name
    Route::get('/change-company-name', 'ChangeCompanyNameController@adminIndex')->name('change-company-name');
    Route::post('/change-company-name/delete', 'ChangeCompanyNameController@delete')->name('delete-change-company-name');

    // General commercial proxy
    Route::get('/general-commercial-proxy', 'GeneralCommercialProxyController@adminIndex')->name('general-commercial-proxy');
    Route::post('/general-commercial-proxy/delete', 'GeneralCommercialProxyController@delete')->name('delete-general-commercial-proxy');

    // Annual financial statements declarations
    Route::get('/annual-financial-statements-declarations', 'AnnualFinancialStatementsDeclarationController@adminIndex')->name('annual-financial-statements-declarations');
    Route::post('/annual-financial-statements-declaration/delete', 'AnnualFinancialStatementsDeclarationController@delete')->name('delete-annual');//Annual financial statements declarations

    // Website sale contract
    Route::get('/website-sale-contract', 'WebsiteSaleContractController@adminIndex')->name('website-sale-contract');
    Route::post('/website-sale-contract', 'WebsiteSaleContractController@delete')->name('delete-website-sale-contract');

    // Wedding contract
    Route::get('/wedding-contract', 'WeddingContractController@adminIndex')->name('wedding-contract');
    Route::post('/wedding-contract', 'WeddingContractController@delete')->name('delete-wedding-contract');

    // Car sale contract
    Route::get('/car-sale-contract', 'CarSaleContractController@adminIndex')->name('car-sale-contract');
    Route::post('/car-sale-contract', 'CarSaleContractController@delete')->name('delete-car-sale-contract');

    // Car sale proxy
    Route::get('/car-sale-proxy', 'CarSaleProxyController@adminIndex')->name('car-sale-proxy');
    Route::post('/car-sale-proxy', 'CarSaleProxyController@delete')->name('delete-car-sale-proxy');

    // Company Activities
    Route::get('/company-activities', 'ActivitiesController@adminIndex')->name('activities');
    Route::get('/company-activities/create', 'ActivitiesController@create')->name('create-activity');
    Route::post('/company-activities/create', 'ActivitiesController@store')->name('create-activity');
    Route::get('/company-activities/edit/{id}', 'ActivitiesController@edit')->name('edit-activity');
    Route::post('/company-activities/edit/{id}', 'ActivitiesController@update')->name('edit-activity');
    Route::post('/company-activities/delete', 'ActivitiesController@delete')->name('delete-activity');

    // Banners
    Route::get('/banners', 'BannerController@list')->name('banners');
    Route::get('/banner/create', 'BannerController@create')->name('create-banner');
    Route::post('/banner/store', 'BannerController@store')->name('store-banner');
    Route::get('/banner/edit/{id}', 'BannerController@edit')->name('edit-banner');
    Route::post('/banner/update/{id}', 'BannerController@update')->name('update-banner');
    Route::post('/banner/delete', 'BannerController@delete')->name('delete-banner');
});


// Test
Route::get('/test', 'AdminController@getZones')->name('zones');