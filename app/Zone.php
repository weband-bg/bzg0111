<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
	
    public function townships()
    {
    	return $this->hasMany('App\Township');
    }
}
