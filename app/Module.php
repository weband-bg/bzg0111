<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function hoverBalloons()
    {
    	return $this->hasMany('App\HoverBalloon');
    }
}
