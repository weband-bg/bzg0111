<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeddingContract extends Model
{
    public function getHusbandNames()
    {
        return $this->husband_firstname . ' ' . $this->husband_secondname . ' ' . $this->husband_lastname;
    }

    public function getWifeNames()
    {
        return $this->wife_firstname . ' ' . $this->wife_secondname . ' ' . $this->wife_lastname;
    }

    public function getClientNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function getMarriageStep()
    {
        switch ($this->marriage_step) {
            case 1:
                return 'Бракът предстои да бъде сключен';
            case 2:
                return 'Бракът е вече сключен';
            default:
                return 'Стъпка';
        }
    }
}
