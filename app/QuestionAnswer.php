<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    public function category()
    {
    	return $this->belongsTo('App\QuestionAnswerCategory', 'question_answer_category_id');
    }

    public function pageSettings()
    {
    	return $this->belongsTo('App\PageSetting');
    }
}
