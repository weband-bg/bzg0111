<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model
{
    public function questionAnswers()
    {
    	return $this->hasMany('App\QuestionAnswer', 'page_settings_id');
    }
}
