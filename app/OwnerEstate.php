<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerEstate extends Model
{
    public function getType()
    {
    	if ($this->type == 'individual') {
    		return 'Физическо лице';
    	}
    	elseif($this->type == 'legal-entity') {
    		return 'Юридическо лице';
    	}
    }

    public function getNames()
    {
    	if ($this->type == 'individual') {
    		return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    	}
    	elseif ($this->type == 'legal-entity') {
    		return $this->name;
    	}
    }

    public function getPINEIK()
    {
    	if ($this->type == 'individual') {
    		return $this->pin;
    	}
    	elseif ($this->type == 'legal-entity') {
    		return $this->eik;
    	}
    }
}
