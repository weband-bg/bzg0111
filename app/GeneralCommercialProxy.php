<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralCommercialProxy extends Model
{
    public function getManagerNames()
    {
        return $this->manager_firstname . ' ' . $this->manager_secondname . ' ' . $this->manager_lastname;
    }

    public function getProxyNames()
    {
        return $this->proxy_firstname . ' ' . $this->proxy_secondname . ' ' . $this->proxy_lastname;
    }
}
