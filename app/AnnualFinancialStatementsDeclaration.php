<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualFinancialStatementsDeclaration extends Model
{
    public function getNames()
    {
    	return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }
}
