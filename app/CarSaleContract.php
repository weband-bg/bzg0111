<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSaleContract extends Model
{
    public function carType()
    {
        return $this->belongsTo('App\CarType');
    }

    public function carMake()
    {
        return $this->belongsTo('App\CarMake');
    }

    public function getSellerNames()
    {
        if ($this->seller_type == 'seller-legal-entity') {
            return $this->seller_name;
        } else {
            return $this->seller_firstname . ' ' . $this->seller_secondname . ' ' . $this->seller_lastname;
        }

    }

    public function getBuyerNames()
    {
        if ($this->buyer_type == 'buyer-legal-entity') {
            return $this->buyer_name;
        } else {
            return $this->buyer_firstname . ' ' . $this->buyer_secondname . ' ' . $this->buyer_lastname;
        }

    }

    public function getClientNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }


    public function getPayment()
    {
        switch ($this->payment) {
            case 1:
                return 'Продажната цена е платен предварително и продавачът заявява това пред нотариуса';
            case 2:
                return 'Продажната цена се заплаща в брой в офиса на нотрариуса';
            case 3:
                return 'Продажната цена се заплаща непосредствено след подписване на договора, по банков път';
            case 4:
                return 'Продажната цена ще бъде заплатена в срок до _________ и при избиране на тази опция се показва клетка за въвеждане на крайната дата за плащане';
            default:
                return 'Стъпка';
        }
    }



}
