<?php

namespace App\Http\Controllers;

use App\HoverBalloon;
use App\Http\Requests\ManagerOperation;
use App\Http\Traits\PaymentTrait;
use App\ManagerOperation as ManagerOperationModel;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;

class ManagerOperationController extends Controller {

    use PaymentTrait;

    public function create() {
        $settings = PageSetting::where('name', 'manager-operation')->first();

        $module = Module::where('name', 'Смяна, Добавяне или Заличаване на Управител')->first();

        return view('manager-operation-create', compact("module", "settings"));
    }

    public function adminIndex()
    {
        $manager_operations = ManagerOperationModel::all();

    	return view('admin.manager-operations', compact("manager_operations"));
    }

    public function store(ManagerOperation $request) {
        $manager_operation = new ManagerOperationModel();
        $manager_operation->company_name = $request->company_name;
        $manager_operation->eik = $request->eik;
        $manager_operation->operation = $request->operation;

        switch ($request->operation) {
            // Add
            case '1':
                $manager_operation->add_manager_firstname = $request->add_manager_firstname;
                $manager_operation->add_manager_secondname = $request->add_manager_secondname;
                $manager_operation->add_manager_lastname = $request->add_manager_lastname;
                $manager_operation->add_manager_pin = $request->add_manager_pin;
                $manager_operation->add_manager_id_number = $request->add_manager_id_number;
                $manager_operation->add_manager_id_date_created = $request->add_manager_id_date_created;
                $manager_operation->add_manager_id_created_by = $request->add_manager_id_created_by;
                break;
            // Remove
            case '2':
                $manager_operation->remove_manager_name = $request->remove_manager_name;
                break;
            // Switch
            case '3':
                $manager_operation->switch_manager_name = $request->switch_manager_name;
                $manager_operation->switch_manager_firstname = $request->switch_manager_firstname;
                $manager_operation->switch_manager_secondname = $request->switch_manager_secondname;
                $manager_operation->switch_manager_lastname = $request->switch_manager_lastname;
                $manager_operation->switch_manager_pin = $request->switch_manager_pin;
                $manager_operation->switch_manager_id_number = $request->switch_manager_id_number;
                $manager_operation->switch_manager_id_date_created = $request->switch_manager_id_date_created;
                $manager_operation->switch_manager_id_created_by = $request->switch_manager_id_created_by;
                break;
            default:
                break;
        }

        $manager_operation->owner_firstname = $request->owner_firstname;
        $manager_operation->owner_secondname = $request->owner_secondname;
        $manager_operation->owner_lastname = $request->owner_lastname;
        $manager_operation->owner_email = $request->owner_email;
        $manager_operation->owner_id_expiry_date = $request->owner_id_expiry_date;
        $manager_operation->owner_phone = $request->owner_phone;

        // Payment methods
        $manager_operation->payment_method = $request->payment_method;

        // Payer info
        $manager_operation->payer_firstname = $request->payer_firstname;
        $manager_operation->payer_lastname = $request->payer_lastname;
        $manager_operation->payer_email = $request->payer_email;
        $manager_operation->payer_phone = $request->payer_phone;

        $manager_operation->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }
    public function delete(Request $request)
    {
      if ($request->ajax()) {
        if (ManagerOperationModel::destroy($request->id)) {
          return response()->json('success');
        }
          return response()->json('error');
      }

      return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'manager-operation')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'manager-operation')->first();

        return $this->paymentExecute($request, $settings);
    }
}
