<?php

namespace App\Http\Controllers;

use App\GeneralCommercialProxy as GeneralCommercialProxyModel;
use App\Http\Requests\GeneralCommercialProxy;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;

class GeneralCommercialProxyController extends Controller {

    use PaymentTrait;

    public function create() {
        $settings = PageSetting::where('name', 'general-commercial-proxy')->first();

        $module = Module::where('name', 'Генерално търговско пълномощно')->first();

        return view('general-commercial-proxy', compact("module", "settings"));
    }

    public function store(GeneralCommercialProxy $request) {
        $general_commercial_proxy = new GeneralCommercialProxyModel();
        $general_commercial_proxy->company_name = $request->company_name;
        $general_commercial_proxy->eik = $request->eik;

        $general_commercial_proxy->manager_firstname = $request->manager_firstname;
        $general_commercial_proxy->manager_secondname = $request->manager_secondname;
        $general_commercial_proxy->manager_lastname = $request->manager_lastname;
        $general_commercial_proxy->manager_pin = $request->manager_pin;
        $general_commercial_proxy->manager_id_number = $request->manager_id_number;
        $general_commercial_proxy->manager_id_date_created = $request->manager_id_date_created;
        $general_commercial_proxy->manager_id_created_by = $request->manager_id_created_by;

        $general_commercial_proxy->proxy_firstname = $request->proxy_firstname;
        $general_commercial_proxy->proxy_secondname = $request->proxy_secondname;
        $general_commercial_proxy->proxy_lastname = $request->proxy_lastname;
        $general_commercial_proxy->proxy_pin = $request->proxy_pin;
        $general_commercial_proxy->proxy_id_number = $request->proxy_id_number;
        $general_commercial_proxy->proxy_id_date_created = $request->proxy_id_date_created;
        $general_commercial_proxy->proxy_id_created_by = $request->proxy_id_created_by;

        // Payment methods
        $general_commercial_proxy->payment_method = $request->payment_method;

        // Payer info
        $general_commercial_proxy->payer_firstname = $request->payer_firstname;
        $general_commercial_proxy->payer_lastname = $request->payer_lastname;
        $general_commercial_proxy->payer_email = $request->payer_email;
        $general_commercial_proxy->payer_phone = $request->payer_phone;

        $general_commercial_proxy->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function adminIndex()
    {
        $proxies = GeneralCommercialProxyModel::all();

        $settings = PageSetting::where('name', 'general-commercial-proxy')->first();

        return view('admin.general-commercial-proxy', compact("settings", "proxies"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (GeneralCommercialProxyModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'general-commercial-proxy')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'general-commercial-proxy')->first();

        return $this->paymentExecute($request, $settings);
    }
}
