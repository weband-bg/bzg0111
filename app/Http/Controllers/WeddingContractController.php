<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeddingContract;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\WeddingContract as WeddingContractModel;
use Illuminate\Http\Request;

class WeddingContractController extends Controller
{

    use PaymentTrait;

    public function create()
    {
        $settings = PageSetting::where('name', 'wedding-contract')->first();

        $module = Module::where('name', 'Брачен договор')->first();

        return view('wedding-contract-create', compact("module", "settings"));
    }

    public function store(WeddingContract $request)
    {
        $wedding_contract = new WeddingContractModel();
        $wedding_contract->husband_firstname = $request['husband_firstname'];
        $wedding_contract->husband_secondname = $request['husband_secondname'];
        $wedding_contract->husband_lastname = $request['husband_lastname'];
        $wedding_contract->husband_pin = $request['husband_pin'];
        $wedding_contract->husband_id_number = $request['husband_id_number'];
        $wedding_contract->husband_id_date_created = $request['husband_id_date_created'];
        $wedding_contract->husband_id_created_by = $request['husband_id_created_by'];

        $wedding_contract->wife_firstname = $request['wife_firstname'];
        $wedding_contract->wife_secondname = $request['wife_secondname'];
        $wedding_contract->wife_lastname = $request['wife_lastname'];
        $wedding_contract->wife_pin = $request['wife_pin'];
        $wedding_contract->wife_id_number = $request['wife_id_number'];
        $wedding_contract->wife_id_date_created = $request['wife_id_date_created'];
        $wedding_contract->wife_id_created_by = $request['wife_id_created_by'];

        $wedding_contract->marriage_step = $request['marriage_step'];

        if ($request->has('marriage_future_date')) {
            $wedding_contract->marriage_future_date = $request->marriage_future_date;
        }
        
        if ($request->has('marriage_past_date')) {
            $wedding_contract->marriage_past_date = $request->marriage_past_date;
        }

        if ($request->has('future_spouses_dispose_property_freely') && $request->future_spouses_dispose_property_freely == 'on') {
            $wedding_contract->future_spouses_dispose_property_freely = true;
        }

        if ($request->has('future_spouses_no_property_right') && $request->future_spouses_no_property_right == 'on') {
            $wedding_contract->future_spouses_no_property_right = true;
        }

        if ($request->has('future_spouses_divisive_duties') && $request->future_spouses_divisive_duties == 'on') {
            $wedding_contract->future_spouses_divisive_duties = true;
        }

        if ($request->has('future_spouses_property_acquired_during_marriage') && $request->future_spouses_property_acquired_during_marriage == 'on') {
            $wedding_contract->future_spouses_property_acquired_during_marriage = true;
        }

        if ($request->has('future_spouses_inheritance_property') && $request->future_spouses_inheritance_property == 'on') {
            $wedding_contract->future_spouses_inheritance_property = true;
        }

        if ($request->has('future_spouses_no_right_claim_part_property') && $request->future_spouses_no_right_claim_part_property == 'on') {
            $wedding_contract->future_spouses_no_right_claim_part_property = true;
        }


        if ($request->has('past_acquired_estate') && $request->past_acquired_estate == 'on') {
            $wedding_contract->past_acquired_estate = true;
        }

        if ($request->has('past_acquired_vehicle') && $request->past_acquired_vehicle == 'on') {
            $wedding_contract->past_acquired_vehicle = true;
        }

        if ($request->has('past_spouses_dispose_property_freely') && $request->past_spouses_dispose_property_freely == 'on') {
            $wedding_contract->past_spouses_dispose_property_freely = true;
        }

        if ($request->has('past_spouses_no_right_claim_part_property') && $request->past_spouses_no_right_claim_part_property == 'on') {
            $wedding_contract->past_spouses_no_right_claim_part_property = true;
        }

        if ($request->has('no_duties') && $request->no_duties == 'on') {
            $wedding_contract->no_duties = true;
        }

        if ($request->has('own_property') && $request->own_property == 'on') {
            $wedding_contract->own_property = true;
        }

        if ($request->has('no_heritage_claims') && $request->no_heritage_claims == 'on') {
            $wedding_contract->no_heritage_claims = true;
        }

        if ($request->has('part_of_ownership_claim') && $request->part_of_ownership_claim) {
            $wedding_contract->part_of_ownership_claim = true;
        }

        $wedding_contract->additional_clauses = $request->additional_clauses;

        $wedding_contract->owner_firstname = $request->owner_firstname;
        $wedding_contract->owner_secondname = $request->owner_secondname;
        $wedding_contract->owner_lastname = $request->owner_lastname;
        $wedding_contract->owner_pin = $request->owner_pin;
        $wedding_contract->owner_email = $request->owner_email;
        $wedding_contract->owner_phone = $request->owner_phone;

        // Payment methods
        $wedding_contract->payment_method = $request->payment_method;

        // Payer info
        $wedding_contract->payer_firstname = $request->payer_firstname;
        $wedding_contract->payer_lastname = $request->payer_lastname;
        $wedding_contract->payer_email = $request->payer_email;
        $wedding_contract->payer_phone = $request->payer_phone;

        $wedding_contract->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }


    public function adminIndex()
    {
        $contracts = WeddingContractModel::all();

        $settings = PageSetting::where('name', 'wedding-contract')->first();

        return view('admin.wedding-contract', compact("settings", "contracts"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (WeddingContractModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'wedding-contract')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'wedding-contract')->first();

        return $this->paymentExecute($request, $settings);
    }
}
