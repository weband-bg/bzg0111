<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
    	return view('admin.dashboard');
    }

    public function getZones()
    {
    	$filepath = public_path() . '/database/oblasti.js';

    	$file = file_get_contents($filepath);

    	$data = json_decode($file);

    	dd($data[0]->name);
    }
}
