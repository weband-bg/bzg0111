<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteSaleContract;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\WebsiteSaleContract as WebsiteSaleContractModel;
use Illuminate\Http\Request;

class WebsiteSaleContractController extends Controller
{
    use PaymentTrait;

    public function create()
    {
        $settings = PageSetting::where('name', 'website-sale-contract')->first();

        $module = Module::where('name', 'Договор за продажба на уеб сайт')->first();

        return view('website-sale-contract-create', compact("module", "settings"));
    }

    public function store(WebsiteSaleContract $request)
    {
        $website_sale_contract = new WebsiteSaleContractModel();
        $website_sale_contract->seller_type = $request->seller_type;
        $website_sale_contract->seller_firstname = $request->seller_firstname;
        $website_sale_contract->seller_secondname = $request->seller_secondname;
        $website_sale_contract->seller_lastname = $request->seller_lastname;
        $website_sale_contract->seller_pin = $request->seller_pin;
        $website_sale_contract->seller_name = $request->seller_name;
        $website_sale_contract->seller_eik = $request->seller_eik;

        $website_sale_contract->buyer_type = $request->buyer_type;
        $website_sale_contract->buyer_firstname = $request->buyer_firstname;
        $website_sale_contract->buyer_secondname = $request->buyer_secondname;
        $website_sale_contract->buyer_lastname = $request->buyer_lastname;
        $website_sale_contract->buyer_pin = $request->buyer_pin;
        $website_sale_contract->buyer_name = $request->buyer_name;
        $website_sale_contract->buyer_eik = $request->buyer_eik;

        $website_sale_contract->domain = $request->domain;
        $website_sale_contract->additional_website_info = $request->additional_website_info;

        $website_sale_contract->owner_firstname = $request->owner_firstname;
        $website_sale_contract->owner_secondname = $request->owner_secondname;
        $website_sale_contract->owner_lastname = $request->owner_lastname;
        $website_sale_contract->owner_email = $request->owner_email;
        $website_sale_contract->owner_id_expiry_date = $request->owner_id_expiry_date;
        $website_sale_contract->owner_phone = $request->owner_phone;

        // Payment methods
        $website_sale_contract->payment_method = $request->payment_method;

        // Payer info
        $website_sale_contract->payer_firstname = $request->payer_firstname;
        $website_sale_contract->payer_lastname = $request->payer_lastname;
        $website_sale_contract->payer_email = $request->payer_email;
        $website_sale_contract->payer_phone = $request->payer_phone;


        $website_sale_contract->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }


    public function adminIndex()
    {
        $contracts = WebsiteSaleContractModel::all();

        $settings = PageSetting::where('name', 'website-sale-contract')->first();

        return view('admin.website-sale-contract', compact("settings", "contracts"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (WebsiteSaleContractModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'website-sale-contract')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'website-sale-contract')->first();

        return $this->paymentExecute($request, $settings);
    }
}
