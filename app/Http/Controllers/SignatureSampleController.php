<?php

namespace App\Http\Controllers;

use App\Http\Requests\SignatureSample;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\SignatureSample as SignatureSampleModel;
use Illuminate\Http\Request;

class SignatureSampleController extends Controller
{  
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'signature-sample')->first();

    $module = Module::where('name', 'Образец от подпис')->first();
    
    return view('signature-sample-create', compact("module", "settings"));
  }

  public function store(SignatureSample $request)
  {
    $signature_sample = new SignatureSampleModel;
    $signature_sample->firstname = $request->firstname;
    $signature_sample->secondname = $request->secondname;
    $signature_sample->lastname = $request->lastname;
    $signature_sample->pin = $request->pin;

    // Payment methods
    $signature_sample->payment_method = $request->payment_method;

    // Payer info
    $signature_sample->payer_firstname = $request->payer_firstname;
    $signature_sample->payer_lastname = $request->payer_lastname;
    $signature_sample->payer_email = $request->payer_email;
    $signature_sample->payer_phone = $request->payer_phone;

    
    $signature_sample->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'signature-sample')->first();

    $signature_samples = SignatureSampleModel::latest()->get();

    return view('admin.signature-sample', compact("settings", "signature_samples"));
  }

  public function delete(Request $request)
  {
      if ($request->ajax()) {
          if (SignatureSampleModel::destroy($request->id)) {
              return response()->json('success');
          }
          return response()->json('error');
      }

      return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'signature-sample')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'signature-sample')->first();

      return $this->paymentExecute($request, $settings);
  }
}
