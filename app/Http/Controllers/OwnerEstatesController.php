<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\Http\Requests\OwnerEstate;
use App\OwnerEstate as OwnerEstateModel;
use App\PageSetting;
use Illuminate\Http\Request;

class OwnerEstatesController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'owner-estates')->first();
    
    return view('owner-estates-create', compact("settings"));
  }

  public function store(OwnerEstate $request)
  {
    $owner_estates = new OwnerEstateModel;

    // Update fields to match OwnerEstate
    $owner_estates->type = $request->type;

    if ($request->type == 'legal-entity') {
      $owner_estates->name = $request->name;
      $owner_estates->eik = $request->eik;
    }
    elseif ($request->type == 'individual') {
      $owner_estates->firstname = $request->firstname;
      $owner_estates->secondname = $request->secondname;
      $owner_estates->lastname = $request->lastname;
      $owner_estates->pin = $request->pin;
    }
    else {
      return redirect()->back()->with(['error' => 'Невалидни данни']);
    }

    // Payment methods
    $owner_estates->payment_method = $request->payment_method;

    // Payer info
    $owner_estates->payer_firstname = $request->payer_firstname;
    $owner_estates->payer_lastname = $request->payer_lastname;
    $owner_estates->payer_email = $request->payer_email;
    $owner_estates->payer_phone = $request->payer_phone;

    $owner_estates->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
      $settings = PageSetting::where('name', 'owner-estates')->first();

      $owner_estates = OwnerEstateModel::latest()->get();

      return view('admin.owner-estates', compact("settings", "owner_estates"));
  }

  public function delete(Request $request)
  {
      if ($request->ajax()) {
          if (OwnerEstateModel::destroy($request->id)) {
              return response()->json('success');
          }
          return response()->json('error');
      }

      return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'owner-estates')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'owner-estates')->first();

      return $this->paymentExecute($request, $settings);
  }
}
