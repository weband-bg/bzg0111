<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $userSocial = Socialite::driver('facebook')->user();

        $findUser = User::where('custom_id', $userSocial->id)->first();

        if ($findUser) {
            $user = $findUser;
        }
        else {
            $user = new User;
            $user->name = $userSocial->name;
            if ($userSocial->email) {
                $user->email = $userSocial->email;
            }
            else {
                $user->email_aviable = false;
                $user->email = 'none-'.Carbon::now().'@gmail.com';
            }
            $user->password = bcrypt('default');
            $user->custom_id = strval($userSocial->id);
            $user->save();
        }

        Auth::login($user);

        return redirect()->route('home');
    }
}
