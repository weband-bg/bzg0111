<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PageSetting;
use App\Company\Activity as ActivityModel;

class ActivitiesController extends Controller
{
    public function adminIndex()
    {
        $settings = PageSetting::where('name', 'activities')->first();

        $activities = ActivityModel::latest()->get();

        return view('admin.company-registrations.activities.activities', compact("settings", "activities"));
    }

    public function create()
    {
        return view('admin.company-registrations.activities.create');
    }

    public function store(Request $request)
    {
        $activity = new ActivityModel;
        $activity->name = $request->name;
        $activity->save();

        return redirect()->route('activities')->with(['success' => 'Дейността беше записана']);
    }

    public function edit($id)
    {
        $activity = ActivityModel::find($id);

        return view('admin.company-registrations.activities.activity-edit', compact("activity"));
    }

    public function update(Request $request, $id)
    {
        $activity = ActivityModel::find($id);
        $activity->name = $request['name'];
        $activity->save();

        return redirect()->back()->with(['success' => 'Дейността е променена успешно']);
    }

    public function delete(Request $request)
    {
        if (ActivityModel::destroy($request->id)) {
            return response()->json('success');
        }

        return response()->json('error');
    }

    public function getAll()
    {
        $activities = ActivityModel::all();

        return response()->json($activities);
    }
}
