<?php

namespace App\Http\Controllers;

use App\Banner;
use App\PageSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerController extends Controller
{
	private function hashImageName($image) {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }


    public function list()
    {
    	$banners = Banner::all();

    	return view('admin.banner.banners', compact("banners"));
    }

    public function create()
    {
    	$page_settings = PageSetting::all();

    	return view('admin.banner.banner-form', compact("page_settings"));
    }

    public function store(Request $request)
    {
    	$banner = new Banner;
    	$banner->name = $request->name;
    	$banner->link = $request->url;

    	//image
    	if ($request->has('image') && $request->image->isValid()) {
    		$image = $request->image;
    		$image_name = $this->hashImageName($image);

    		Storage::disk('banner_images')->putFileAs('', $image, $image_name);

    		$banner->image = $image_name;
    	}

    	$banner->page_settings()->associate($request->page);
    	$banner->save();

    	return redirect()->route('banners')->with(['success' => 'Банерът беше качен']);
    }

    public function edit($id)
    {
    	$banner = Banner::find($id);

    	return view('admin.banner.edit', compact("banner"));
    }

    public function update(Request $request, $id)
    {
    	$banner = Banner::find($id);

    	$banner->name = $request->name;
    	$banner->link = $request->url;

    	//image
    	if ($request->has('image') && $request->image->isValid()) {
    		// delete old image
    		unlink(public_path().config('app.banner-images').$banner->image);

    		$image = $request->image;
    		$image_name = $this->hashImageName($image);

    		Storage::disk('banner_images')->putFileAs('', $image, $image_name);

    		$banner->image = $image_name;
    	}

    	$banner->page_settings()->associate($request->page);
    	$banner->save();

    	return redirect()->route('banners')->with(['success' => 'Банерът беше качен']);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
        	$banner = Banner::find($request->id);

        	// Delete image
        	unlink(public_path().config('app.banner-images').$banner->image);

            if ($banner->delete()) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }
}
