<?php

namespace App\Http\Controllers;

use App\HoverBalloon;
use App\Module;
use Illuminate\Http\Request;

class HoverBalloonsController extends Controller
{
    public function list()
    {
        $modules = Module::all();

        return view('admin.hover-balloons.list', compact('modules'));
    }

    public function edit($id)
    {
        $hover_balloon = HoverBalloon::find($id);
        $modules = Module::all();

        return view('admin.hover-balloons.edit', compact('hover_balloon', 'modules'));
    }

    public function update(Request $request, $id)
    {
        $hover_balloon = HoverBalloon::find($id);
        $hover_balloon->label = $request->label;
        $hover_balloon->hover_text = $request->hover_text;
        $hover_balloon->save();

        return redirect()->back()->with(['success' => 'Ховър Балона е променен успешно']);
    }

    public function show($id)
    {
        $module = Module::find($id);

        return view('admin.hover-balloons.show', compact("module"));
    }


    public function ltdRegistration()
    {
        $hover_balloons = HoverBalloon::where('page_name', 'Регистрация на ЕООД')->get();

        $hovers = array();

        foreach ($hover_balloons as $hover_balloon) {
            $hovers[$hover_balloon->form_field] = $hover_balloon->hover_text;
        }

        return view('admin.hover-balloons.ltd-registration', compact("hovers"));
    }

    public function storeLtdRegistration(Request $request)
    {
        foreach ($request->all() as $form_field => $hover_text) {
            if ($form_field != '_token') {
                $ltd_hover = HoverBalloon::where([
                    ['page_name', 'Регистрация на ЕООД'],
                    ['form_field', $form_field]
                ])->first();

                $ltd_hover->hover_text = $hover_text;
                $ltd_hover->save();
            }
        }

        return redirect()->route('list-hover-balloons')->with(['success' => 'Ховърите на страница "Регистрация на ЕООД" бяха редактирани']);
    }
}
