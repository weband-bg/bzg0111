<?php

namespace App\Http\Controllers;

use App\Company\Activity;
use App\Company\ET\ET;
use App\Company\LTD\LTD;
use App\Company\LTD\OtherLTDManager;
use App\Company\OOD\OOD;
use App\Company\OOD\OtherOODManager;
use App\Company\OOD\OtherOODOwner;
use App\HoverBalloon;
use App\Http\Requests\CompanyET;
use App\Http\Requests\CompanyLTD;
use App\Http\Requests\CompanyOOD;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\Zone;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CompanyRegistrationController extends Controller
{
    use PaymentTrait;

    public function index()
    {
        return view('company-registration.index');
    }

    public function registerLTD()
    {
        $settings = PageSetting::where('name', 'registration-eood')->first();

        $module = Module::where('name', 'Регистрация на ЕООД')->first();

        $zones = Zone::all();

        return view('company-registration.register-ltd', compact("module", "settings", "zones"));
    }

    public function storeLTD(CompanyLTD $request)
    {
        // dd($request->all());
        // exit;

        $ltd = new LTD();

        $ltd->company_name = $request->company_name;
        $ltd->company_foreign_name = $request->company_foreign_language_name;
        $ltd->zone_id = $request->zone;
        $ltd->township_id = $request->township;
        $ltd->populated_place_id = $request->populated_place;
        $ltd->postcode = $request->postcode;
        $ltd->quarter = $request->quarter;
        $ltd->street = $request->street;
        $ltd->street_number = $request->street_number;
        $ltd->building = $request->building;
        $ltd->vhod = $request->entrance;
        $ltd->floor = $request->floor;
        $ltd->apartment = $request->apartment;

        if ($request->has('different_address') && $request->different_address == 'different_address') {
            $ltd->different_address = true;

            $ltd->address_2_zone_id = $request->address_2_zone;
            $ltd->address_2_township_id = $request->address_2_township;
            $ltd->address_2_populated_place_id = $request->address_2_populated_place;
            $ltd->address_2_postcode = $request->address_2_postcode;
            $ltd->address_2_quarter = $request->address_2_quarter;
            $ltd->address_2_street = $request->address_2_street;
            $ltd->address_2_street_number = $request->address_2_street_number;
            $ltd->address_2_building = $request->address_2_building;
            $ltd->address_2_vhod = $request->address_2_vhod;
            $ltd->address_2_floor = $request->address_2_floor;
            $ltd->address_2_apartment = $request->address_2_apartment;
        }

        $ltd->additional_activities = $request->additional_activities;

        $ltd->money = $request->money;

        // Owner
        $ltd->owner_firstname = $request->owner_firstname;
        $ltd->owner_secondname = $request->owner_secondname;
        $ltd->owner_lastname = $request->owner_lastname;
        $ltd->owner_pin = $request->owner_pin;
        $ltd->owner_id_number = $request->owner_id_number;
        $owner_id_date_created = Carbon::createFromFormat('Y-m-d', $request->owner_id_date_created);
        $ltd->owner_id_date_created = $owner_id_date_created->format('Y-m-d');
        $ltd->owner_id_created_by = $request->owner_id_created_by;
        $ltd->manager = $request->company_manager;

        // Payment methods
        $ltd->payment_method = $request->payment_method;

        // Payer info
        $ltd->payer_firstname = $request->payer_firstname;
        $ltd->payer_lastname = $request->payer_lastname;
        $ltd->payer_email = $request->payer_email;
        $ltd->payer_phone = $request->payer_phone;

        $ltd->save();

        if (count($request->manager)) {
            foreach ($request->manager as $manager) {
                $other_ltd_manager = new OtherLTDManager;
                $other_ltd_manager->company()->associate($ltd);
                $other_ltd_manager->firstname = $manager['firstname'];
                $other_ltd_manager->secondname = $manager['secondname'];
                $other_ltd_manager->lastname = $manager['lastname'];
                $other_ltd_manager->pin = $manager['pin'];
                $other_ltd_manager->id_number = $manager['id_number'];
                $other_ltd_manager_id_date_created = Carbon::createFromFormat('Y-m-d', $manager['id_date_created']);
                $other_ltd_manager->id_date_created = $other_ltd_manager_id_date_created->format('Y-m-d');
                $other_ltd_manager->id_created_by = $manager['id_created_by'];
                $other_ltd_manager->save();
            }

            $ltd->representation_method = $request->representation_method;
        }

        // Activities
        foreach ($request->activities as $activity_id) {
            $activity = Activity::find($activity_id);

            $ltd->activities()->attach($activity);
        }

        $ltd->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function registerOOD()
    {
        $settings = PageSetting::where('name', 'registration-ood')->first();

        $module = Module::where('name', 'Регистрация на ООД')->first();

        return view('company-registration.register-ood', compact("module", "settings"));
    }

    public function storeOOD(CompanyOOD $request)
    {
        $ood = new OOD();

        $ood->company_name = $request->company_name;
        $ood->company_foreign_name = $request->company_foreign_language_name;
        $ood->zone_id = $request->zone;
        $ood->township_id = $request->township;
        $ood->populated_place_id = $request->populated_place;
        $ood->postcode = $request->postcode;
        $ood->quarter = $request->quarter;
        $ood->street = $request->street;
        $ood->street_number = $request->street_number;
        $ood->building = $request->building;
        $ood->vhod = $request->entrance;
        $ood->floor = $request->floor;
        $ood->apartment = $request->apartment;

        if ($request->has('different_address') && $request->different_address == 'on') {
            $ood->different_address = true;

            $ood->address_2_zone_id = $request->address_2_zone;
            $ood->address_2_township_id = $request->address_2_township;
            $ood->address_2_populated_place_id = $request->address_2_populated_place;
            $ood->address_2_postcode = $request->address_2_postcode;
            $ood->address_2_quarter = $request->address_2_quarter;
            $ood->address_2_street = $request->address_2_street;
            $ood->address_2_street_number = $request->address_2_street_number;
            $ood->address_2_building = $request->address_2_building;
            $ood->address_2_vhod = $request->address_2_vhod;
            $ood->address_2_floor = $request->address_2_floor;
            $ood->address_2_apartment = $request->address_2_apartment;
        }

        $ood->additional_activities = $request->additional_activities;

        $ood->money = $request->money;

        $ood->representation_method = $request->representation_method;

        // Payment methods
        $ood->payment_method = $request->payment_method;

        // Payer info
        $ood->payer_firstname = $request->payer_firstname;
        $ood->payer_lastname = $request->payer_lastname;
        $ood->payer_email = $request->payer_email;
        $ood->payer_phone = $request->payer_phone;

        $ood->save();

        if ($request->has('owner') && count($request->owner)) {
            foreach ($request->owner as $owner) {
                $other_ood_owner = new OtherOODOwner;
                $other_ood_owner->company()->associate($ood);
                $other_ood_owner->firstname = $owner['firstname'];
                $other_ood_owner->secondname = $owner['secondname'];
                $other_ood_owner->lastname = $owner['lastname'];
                $other_ood_owner->pin = $owner['pin'];
                $other_ood_owner->id_number = $owner['id_number'];
                $other_ood_owner_id_date_created = Carbon::createFromFormat('Y-m-d', $owner['id_date_created']);
                $other_ood_owner->id_date_created = $other_ood_owner_id_date_created->format('Y-m-d');
                $other_ood_owner->id_created_by = $owner['id_created_by'];
                $other_ood_owner->shareholding = $owner['money'];
                
                if ($owner['manager'] == 'on') {
                    $other_ood_owner->manager = true;
                }

                $other_ood_owner->save();
            }
        }

        if ($request->has('manager') && count($request->manager)) {
            foreach ($request->manager as $manager) {
                $other_ood_manager = new OtherOODManager;
                $other_ood_manager->company()->associate($ood);
                $other_ood_manager->firstname = $manager['firstname'];
                $other_ood_manager->secondname = $manager['secondname'];
                $other_ood_manager->lastname = $manager['lastname'];
                $other_ood_manager->pin = $manager['pin'];
                $other_ood_manager->id_number = $manager['id_number'];
                $other_ood_manager_id_date_created = Carbon::createFromFormat('Y-m-d', $manager['id_date_created']);
                $other_ood_manager->id_date_created = $other_ood_manager_id_date_created->format('Y-m-d');
                $other_ood_manager->id_created_by = $manager['id_created_by'];
                $other_ood_manager->save();
            }
        }

        // Activities
        foreach ($request->activities as $activity_id) {
            $activity = Activity::find($activity_id);

            $ood->activities()->attach($activity);
        }

        $ood->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function registerET()
    {
        $settings = PageSetting::where('name', 'registration-et')->first();

        $module = Module::where('name', 'Регистрация на ЕТ')->first();

        return view('company-registration.register-et', compact("module", "settings"));
    }

    function storeET(CompanyET $request)
    {

        $et = new ET();

        $et->sole_trader_name = $request->input('sole_trader_name');
        $et->sole_trader_foreign_name = $request->input('sole_trader_foreign_name');
        $et->zone_id = $request->zone;
        $et->township_id = $request->township;
        $et->populated_place_id = $request->populated_place;
        $et->postcode = $request->postcode;
        $et->quarter = $request->quarter;
        $et->street = $request->street;
        $et->street_number = $request->street_number;
        $et->building = $request->building;
        $et->vhod = $request->vhod;
        $et->floor = $request->floor;
        $et->apartment = $request->apartment;
        $et->additional_activities = $request->additional_activities;

        if ($request->has('different-address') && $request->input('different-address') == 'on') {
            $et->different_address = true;

            $et->address_2_zone_id = $request->address_2_zone;
            $et->address_2_township_id = $request->address_2_township;
            $et->address_2_populated_place_id = $request->address_2_populated_place;
            $et->address_2_postcode = $request->address_2_postcode;
            $et->address_2_quarter = $request->address_2_quarter;
            $et->address_2_street = $request->address_2_street;
            $et->address_2_street_number = $request->address_2_street-number;
            $et->address_2_building = $request->address_2_building;
            $et->address_2_vhod = $request->address_2_vhod;
            $et->address_2_floor = $request->address_2_floor;
            $et->address_2_apartment = $request->address_2_apartment;
        }

        // Owner
        $et->owner_firstname = $request->owner_firstname;
        $et->owner_secondname = $request->owner_secondname;
        $et->owner_lastname = $request->owner_lastname;
        $et->owner_pin = $request->owner_pin;
        $et->owner_id_number = $request->owner_id_number;
        $owner_id_date_created = Carbon::createFromFormat('Y-m-d', $request->input('owner_id_date_created'));
        $et->owner_id_date_created = $owner_id_date_created->format('Y-m-d');

        $et->owner_id_created_by = $request->owner_id_created_by;

        // Payment methods
        $et->payment_method = $request->payment_method;

        // Payer info
        $et->payer_firstname = $request->payer_firstname;
        $et->payer_lastname = $request->payer_lastname;
        $et->payer_email = $request->payer_email;
        $et->payer_phone = $request->payer_phone;

        $et->save();

        // Activities
        foreach ($request->activities as $activity_id) {
            $activity = Activity::find($activity_id);

            $et->activities()->attach($activity);
        }

        $et->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена']);
    }

    //Paypal
    public function ltdPaypalCreate()
    {
        $settings = PageSetting::where('name', 'registration-eood')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function ltdPaypalExecute()
    {
        $settings = PageSetting::where('name', 'registration-eood')->first();

        return $this->paymentExecute($request, $settings);

    }

    public function oodPaypalCreate()
    {
        $settings = PageSetting::where('name', 'registration-ood')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function oddPaypalExecute()
    {
        $settings = PageSetting::where('name', 'registration-ood')->first();

        return $this->paymentExecute($request, $settings);
    }

    public function etPaypalCreate()
    {
        $settings = PageSetting::where('name', 'registration-et')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function etPaypalExecute()
    {
        $settings = PageSetting::where('name', 'registration-et')->first();

        return $this->paymentExecute($request, $settings);
    }

    public function adminIndex()
    {
        $settings = PageSetting::where('name', 'registration-eood')->first();

        $companies = LTD::latest()->get();

        return view('admin.company-registrations.ltd-list', compact("companies", "settings"));
    }

    public function adminOODIndex()
    {
        $companies = OOD::latest()->get();

        return view('admin.company-registrations.ood-list', compact("companies"));
    }

    public function adminETIndex()
    {
        $companies = ET::latest()->get();

        return view('admin.company-registrations.et-list', compact("companies"));
    }

    public function deleteLtd(Request $request)
    {
        if ($request->ajax()) {
            if (LTD::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    public function deleteET(Request $request)
    {
        if ($request->ajax()) {
            if (ET::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    public function deleteOOD(Request $request)
    {
        if ($request->ajax()) {
            if (OOD::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }
}
