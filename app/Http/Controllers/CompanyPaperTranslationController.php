<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\CompanyPaperTranslation as CompanyPaperTranslationModel;
use App\Http\Requests\CompanyPaperTranslation;
use App\PageSetting;
use Illuminate\Http\Request;

class CompanyPaperTranslationController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'company-paper-translation')->first();

    return view('company-paper-translation-create', compact("settings"));
  }

  public function store(CompanyPaperTranslation $request)
  {
    $company_paper_translation = new CompanyPaperTranslationModel;
    $company_paper_translation->eik = $request->eik;
    $company_paper_translation->company_name = $request->eik;

    // Payment methods
    $ltd->payment_method = $request->payment_method;

    // Payer info
    $ltd->payer_firstname = $request->payer_firstname;
    $ltd->payer_lastname = $request->payer_lastname;
    $ltd->payer_email = $request->payer_email;
    $ltd->payer_phone = $request->payer_phone;
    
    $company_paper_translation->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  // public function adminIndex()
  // {
  //   $company_paper_translations = CompanyPaperTranslationModel::latest()->get();

  //   return view('admin.company-paper-translation', compact("company_paper_translations"));
  // }

  public function delete(Request $request)
  {
      if ($request->ajax()) {
          if (CompanyPaperTranslationModel::destroy($request->id)) {
              return response()->json('success');
          }
          return response()->json('error');
      }

      return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'company-paper-translation')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'company-paper-translation')->first();

      return $this->paymentExecute($request, $settings);
  }
}
