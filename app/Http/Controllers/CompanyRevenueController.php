<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\CompanyRevenue as CompanyRevenueModel;
use App\Http\Requests\CompanyRevenue;
use App\PageSetting;
use Illuminate\Http\Request;

class CompanyRevenueController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'company-revenue')->first();
    
    return view('company-revenue-create', compact("settings"));
  }

  public function store(CompanyRevenue $request)
  {
    $company_revenue = new CompanyRevenueModel;
    $company_revenue->name = $request->company_name;
    $company_revenue->eik = $request->eik;

    // Payment methods
    $company_revenue->payment_method = $request->payment_method;

    // Payer info
    $company_revenue->payer_firstname = $request->payer_firstname;
    $company_revenue->payer_lastname = $request->payer_lastname;
    $company_revenue->payer_email = $request->payer_email;
    $company_revenue->payer_phone = $request->payer_phone;
    
    $company_revenue->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'company-revenue')->first();

    $revenues = CompanyRevenueModel::all();

    return view('admin.company-revenue', compact("revenues", "settings"));
  }

  public function delete(Request $request)
  {
    if ($request->ajax()) {
      if (CompanyRevenueModel::destroy($request->id)) {
        return response()->json('success');
      }
        return response()->json('error');
    }

    return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'company-revenue')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'company-revenue')->first();

      return $this->paymentExecute($request, $settings);
  }
}
