<?php

namespace App\Http\Controllers;

use App\Township;
use App\Zone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ZonesController extends Controller
{
    public function getAll()
    {
    	$zones = Zone::all();

    	return response()->json($zones);
    }

    public function getTownships($zone_id)
    {
    	$zone = Zone::find($zone_id);

    	return response()->json($zone->townships);
    }

    public function getPopulatedPlaces($township_id)
    {
        $township = Township::find($township_id);

        return response()->json($township->populatedPlaces);
    }
}
