<?php

namespace App\Http\Controllers;

use App\AnnualFinancialStatementsDeclaration as AnnualFinancialStatementsDeclarationModel;
use App\Http\Requests\AnnualFinancialStatementsDeclaration;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\PaypalPayment;
use Illuminate\Http\Request;

class AnnualFinancialStatementsDeclarationController extends Controller
{  
    use PaymentTrait;

    public function create()
    {
        $settings = PageSetting::where('name', 'annual-financial-statements-declaration')->first();

        $module = Module::where('name', 'Обявяване на годишен финансов отчет')->first();

        return view('annual-financial-statements-declaration-create', compact("module", "settings"));
    }

    public function store(AnnualFinancialStatementsDeclaration $request) 
    {
        $annual_financial_statements_declaration = new AnnualFinancialStatementsDeclarationModel();
        $annual_financial_statements_declaration->company_name = $request->company_name;
        $annual_financial_statements_declaration->eik = $request->eik;

        $annual_financial_statements_declaration->owner_firstname = $request->owner_firstname;
        $annual_financial_statements_declaration->owner_secondname = $request->owner_secondname;
        $annual_financial_statements_declaration->owner_lastname = $request->owner_lastname;
        $annual_financial_statements_declaration->owner_email = $request->owner_email;
        $annual_financial_statements_declaration->owner_id_expiry_date = $request->owner_id_expiry_date;
        $annual_financial_statements_declaration->owner_phone = $request->owner_phone;

        // Payment methods
        $annual_financial_statements_declaration->payment_method = $request->payment_method;

        // Payer info
        $annual_financial_statements_declaration->payer_firstname = $request->payer_firstname;
        $annual_financial_statements_declaration->payer_lastname = $request->payer_lastname;
        $annual_financial_statements_declaration->payer_email = $request->payer_email;
        $annual_financial_statements_declaration->payer_phone = $request->payer_phone;

        $annual_financial_statements_declaration->save();

        $settings = PageSetting::where('name', 'annual-financial-statements-declaration')->first();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (AnnualFinancialStatementsDeclarationModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    public function adminIndex()
    {
        $annual_financial_statements_declarations = AnnualFinancialStatementsDeclarationModel::latest()->get();

        return view('admin.annual-financial-statements-declarations', compact("annual_financial_statements_declarations"));
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'annual-financial-statements-declaration')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'annual-financial-statements-declaration')->first();

        return $this->paymentExecute($request, $settings);
    }
}