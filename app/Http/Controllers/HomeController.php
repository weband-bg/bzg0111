<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PageSetting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        $settings = PageSetting::where('name', 'wedding-contract')->first();

        return view('about', compact("settings", "settings"));

    }
}
