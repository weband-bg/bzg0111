<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\EstateOpening as EstateOpeningModel;
use App\Http\Requests\EstateOpening;
use App\PageSetting;
use Illuminate\Http\Request;

class EstateOpeningController extends Controller
{
  use PaymentTrait;    

  public function create()
  {
    $settings = PageSetting::where('name', 'estate-opening')->first();
    
    return view('estate-opening-create', compact("settings"));
  }

  public function store(EstateOpening $request)
  {
    $estate_opening = new EstateOpeningModel;
    $estate_opening->info_link = $request->info_link;
    
    // Payment methods
    $estate_opening->payment_method = $request->payment_method;

    // Payer info
    $estate_opening->payer_firstname = $request->payer_firstname;
    $estate_opening->payer_lastname = $request->payer_lastname;
    $estate_opening->payer_email = $request->payer_email;
    $estate_opening->payer_phone = $request->payer_phone;

    $estate_opening->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'estate-opening')->first();

    $estates = EstateOpeningModel::all();

    return view('admin.estate-opening', compact("estates", "settings"));
  }

  public function delete(Request $request)
  {
    if ($request->ajax()) {
      if (EstateOpeningModel::destroy($request->id)) {
        return response()->json('success');
      }
        return response()->json('error');
    }

    return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'estate-opening')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'estate-opening')->first();

      return $this->paymentExecute($request, $settings);
  }
}
