<?php

namespace App\Http\Controllers;

use App\CompanyParticipation as CompanyParticipationModel;
use App\Http\Requests\CompanyParticipation;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;

class CompanyParticipationsController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'company-participations')->first();
    
    $module = Module::where('name', 'Проверка за участие в дружества')->first();

    return view('company-participations-create', compact("module", "settings"));
  }

  public function store(CompanyParticipation $request)
  {
    $company_participation = new CompanyParticipationModel;
    $company_participation->firstname = $request->firstname;
    $company_participation->secondname = $request->secondname;
    $company_participation->lastname = $request->lastname;
    $company_participation->pin = $request->pin;

    // Payment methods
    $company_participation->payment_method = $request->payment_method;

    // Payer info
    $company_participation->payer_firstname = $request->payer_firstname;
    $company_participation->payer_lastname = $request->payer_lastname;
    $company_participation->payer_email = $request->payer_email;
    $company_participation->payer_phone = $request->payer_phone;

    $company_participation->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'company-participations')->first();

    $company_participations = CompanyParticipationModel::latest()->get();

    return view('admin.company-participations', compact("settings", "company_participations"));
  }

  public function delete(Request $request)
  {
      if ($request->ajax()) {
          if (CompanyParticipationModel::destroy($request->id)) {
              return response()->json('success');
          }
          return response()->json('error');
      }

      return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'company-participations')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'company-participations')->first();

      return $this->paymentExecute($request, $settings);
  }
}
