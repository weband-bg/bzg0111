<?php

namespace App\Http\Controllers;

use App\PageSetting;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
    	$settings = PageSetting::where('name', 'home')->first();

    	return view('index', compact("settings"));
    }
}
