<?php

namespace App\Http\Controllers;

use App\CarMake;
use App\CarSaleContract as CarSaleContractModel;
use App\CarType;
use App\Http\Requests\CarSaleContract;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CarSaleContractController extends Controller
{
    public function create()
    {
        $settings = PageSetting::where('name', 'car-sale-contract')->first();

        $module = Module::where('name', 'Договор за продажба на МПС')->first();

        $types = CarType::all();
        $makes = CarMake::all();

        return view('car-sale-contract-create', compact("module", "settings", "types", "makes", "payments"));
    }

    public function store(CarSaleContract $request)
    {
        $car_sale_contract = new CarSaleContractModel();

        if ($request->hasFile('talon')) {
            $car_sale_contract->talon = Storage::disk('uploads')->putFile('talons', $request->file('talon'));
        }

        $car_sale_contract->seller_type = $request->seller_type;
        $car_sale_contract->seller_firstname = $request->seller_firstname;
        $car_sale_contract->seller_secondname = $request->seller_secondname;
        $car_sale_contract->seller_lastname = $request->seller_lastname;
        $car_sale_contract->seller_pin = $request->seller_pin;
        $car_sale_contract->seller_name = $request->seller_name;
        $car_sale_contract->seller_eik = $request->seller_eik;

        $car_sale_contract->buyer_type = $request->buyer_type;
        $car_sale_contract->buyer_firstname = $request->buyer_firstname;
        $car_sale_contract->buyer_secondname = $request->buyer_secondname;
        $car_sale_contract->buyer_lastname = $request->buyer_lastname;
        $car_sale_contract->buyer_pin = $request->buyer_pin;
        $car_sale_contract->buyer_name = $request->buyer_name;
        $car_sale_contract->buyer_eik = $request->buyer_eik;

        $car_sale_contract->car_type_id = $request->type;
        $car_sale_contract->car_make_id = $request->make;
        $car_sale_contract->car_model = $request->model;
        $car_sale_contract->car_chassis = $request->chassis;
        $car_sale_contract->car_registration = $request->registration;

        $car_sale_contract->price = $request->price;
        $car_sale_contract->payment = $request->payment;
        $car_sale_contract->payment_date = $request->payment_date;
        $car_sale_contract->expenses = $request->expenses;


        $car_sale_contract->owner_firstname = $request->owner_firstname;
        $car_sale_contract->owner_secondname = $request->owner_secondname;
        $car_sale_contract->owner_lastname = $request->owner_lastname;
        $car_sale_contract->owner_email = $request->owner_email;
        $car_sale_contract->owner_phone = $request->owner_phone;

        // Payment methods
        $car_sale_contract->payment_method = $request->payment_method;

        // Payer info
        $car_sale_contract->payer_firstname = $request->payer_firstname;
        $car_sale_contract->payer_lastname = $request->payer_lastname;
        $car_sale_contract->payer_email = $request->payer_email;
        $car_sale_contract->payer_phone = $request->payer_phone;

        $car_sale_contract->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function adminIndex()
    {
        $contracts = CarSaleContractModel::all();

        $settings = PageSetting::where('name', 'car-sale-contract')->first();

        return view('admin.car-sale-contract', compact("settings", "contracts"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (CarSaleContractModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }
}
