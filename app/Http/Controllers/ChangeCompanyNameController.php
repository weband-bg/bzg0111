<?php

namespace App\Http\Controllers;

use App\ChangeCompanyName as ChangeCompanyNameModel;
use App\Http\Requests\ChangeCompanyName;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;

class ChangeCompanyNameController extends Controller {

    use PaymentTrait;

    public function create() {
        $settings = PageSetting::where('name', 'change-company-name')->first();

        $module = Module::where('name', 'Промяна на наименование на фирма')->first();

        return view('change-company-name-create', compact("module", "settings"));
    }

    public function store(ChangeCompanyName $request) {
        $change_company_name = new ChangeCompanyNameModel();
        $change_company_name->current_company_name = $request->current_company_name;
        $change_company_name->eik = $request->eik;
        $change_company_name->new_company_name = $request->new_company_name;
        $change_company_name->new_company_foreign_name = $request->new_company_foreign_name;
        $change_company_name->owner_firstname = $request->owner_firstname;
        $change_company_name->owner_secondname = $request->owner_secondname;
        $change_company_name->owner_lastname = $request->owner_lastname;
        $change_company_name->owner_email = $request->owner_email;
        $change_company_name->owner_id_expiry_date = $request->owner_id_expiry_date;
        $change_company_name->owner_phone = $request->owner_phone;

        // Payment methods
        $change_company_name->payment_method = $request->payment_method;

        // Payer info
        $change_company_name->payer_firstname = $request->payer_firstname;
        $change_company_name->payer_lastname = $request->payer_lastname;
        $change_company_name->payer_email = $request->payer_email;
        $change_company_name->payer_phone = $request->payer_phone;

        $change_company_name->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function adminIndex()
    {
        $names = ChangeCompanyNameModel::all();

        $settings = PageSetting::where('name', 'change-company-name')->first();

        return view('admin.change-company-name', compact("settings", "names"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (ChangeCompanyNameModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'change-company-name')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'change-company-name')->first();

        return $this->paymentExecute($request, $settings);
    }
}
