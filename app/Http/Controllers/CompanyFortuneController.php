<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\CompanyFortune as CompanyFortuneModel;
use App\Http\Requests\CompanyFortune;
use App\PageSetting;
use Illuminate\Http\Request;

class CompanyFortuneController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'company-fortune')->first();
    
    return view('company-fortune-create', compact("settings"));
  }

  public function store(CompanyFortune $request)
  {
    $company_fortune = new CompanyFortuneModel;
    $company_fortune->eik = $request->eik;
    $company_fortune->company_name = $request->company_name;

    // Payment methods
    $company_fortune->payment_method = $request->payment_method;

    // Payer info
    $company_fortune->payer_firstname = $request->payer_firstname;
    $company_fortune->payer_lastname = $request->payer_lastname;
    $company_fortune->payer_email = $request->payer_email;
    $company_fortune->payer_phone = $request->payer_phone;
    
    $company_fortune->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }
  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'company-fortune')->first();

    $company_fortunes = CompanyFortuneModel::latest()->get();

    return view('admin.company-fortune', compact("settings", "company_fortunes"));
  }

  public function delete(Request $request)
  {
      if ($request->ajax()) {
          if (CompanyFortuneModel::destroy($request->id)) {
              return response()->json('success');
          }
          return response()->json('error');
      }

      return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'company-fortune')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'company-fortune')->first();

      return $this->paymentExecute($request, $settings);
  }
}
