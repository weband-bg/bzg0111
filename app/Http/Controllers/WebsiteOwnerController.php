<?php

namespace App\Http\Controllers;

use App\Http\Requests\WebsiteOwner;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\WebsiteOwner as WebsiteOwnerModel;
use Illuminate\Http\Request;

class WebsiteOwnerController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'website-owner')->first();

     $module = Module::where('name', 'Проверка за собствеността на уеб сайт')->first();

    return view('website-owner-create', compact("module", "settings"));
  }

  public function store(WebsiteOwner $request)
  {
    $website_owner = new WebsiteOwnerModel;
    $website_owner->domain = $request->domain;

    // Payment methods
    $website_owner->payment_method = $request->payment_method;

    // Payer info
    $website_owner->payer_firstname = $request->payer_firstname;
    $website_owner->payer_lastname = $request->payer_lastname;
    $website_owner->payer_email = $request->payer_email;
    $website_owner->payer_phone = $request->payer_phone;
    
    $website_owner->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
    $settings = PageSetting::where('name', 'website-owner')->first();

    $websites = WebsiteOwnerModel::all();

    return view('admin.website-owner', compact("websites", "settings"));
  }

  public function delete(Request $request)
  {
    if ($request->ajax()) {
      if (WebsiteOwnerModel::destroy($request->id)) {
        return response()->json('success');
      }
        return response()->json('error');
    }

    return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'website-owner')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'website-owner')->first();

      return $this->paymentExecute($request, $settings);
  }
}
