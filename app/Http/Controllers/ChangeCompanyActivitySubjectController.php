<?php

namespace App\Http\Controllers;

use App\ChangeCompanyActivitySubject as ChangeCompanyActivitySubjectModel;
use App\Company\Activity;
use App\Http\Requests\ChangeCompanyActivitySubject;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;

class ChangeCompanyActivitySubjectController extends Controller {

    public function create() {
        $settings = PageSetting::where('name', 'change-company-activity-subject')->first();

        $module = Module::where('name', 'Промяна на предмета на дейност')->first();

        return view('change-company-activity-subject-create', compact("module", "settings"));
    }
    
    public function store(ChangeCompanyActivitySubject $request) {
        $change_company_activity_subject = new ChangeCompanyActivitySubjectModel();
        $change_company_activity_subject->company_name = $request->company_name;
        $change_company_activity_subject->eik = $request->eik;

        $change_company_activity_subject->additional_activities = $request->additional_activities;
        $change_company_activity_subject->owner_firstname = $request->owner_firstname;
        $change_company_activity_subject->owner_secondname = $request->owner_secondname;
        $change_company_activity_subject->owner_lastname = $request->owner_lastname;
        $change_company_activity_subject->owner_email = $request->owner_email;
        $change_company_activity_subject->owner_id_expiry_date = $request->owner_id_expiry_date;
        $change_company_activity_subject->owner_phone = $request->owner_phone;

        // Payment methods
        $change_company_activity_subject->payment_method = $request->payment_method;

        // Payer info
        $change_company_activity_subject->payer_firstname = $request->payer_firstname;
        $change_company_activity_subject->payer_lastname = $request->payer_lastname;
        $change_company_activity_subject->payer_email = $request->payer_email;
        $change_company_activity_subject->payer_phone = $request->payer_phone;

        $change_company_activity_subject->save();

        // Activities
        foreach ($request->activities as $activity_id) {
            $activity = Activity::find($activity_id);

            $change_company_activity_subject->activities()->attach($activity);
        }

        $change_company_activity_subject->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function adminIndex()
    {
        $settings = PageSetting::where('name', 'change-company-activity-subject')->first();

        $changes = ChangeCompanyActivitySubjectModel::all();

        return view('admin.company-change-activity-subject', compact("changes", "settings"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (ChangeCompanyActivitySubjectModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'change-company-activity-subject')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'change-company-activity-subject')->first();

        return $this->paymentExecute($request, $settings);
    }
}