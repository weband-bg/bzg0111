<?php

namespace App\Http\Controllers;

use App\Http\Requests\SwitchManagementAddress;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\SwitchManagementAddress as SwitchManagementAddressModel;
use Illuminate\Http\Request;

class SwitchManagementAddressController extends Controller {

    use PaymentTrait;

    public function create() {
        $settings = PageSetting::where('name', 'switch-management-address')->first();

        $module = Module::where('name', 'Промяна на адрес на управление')->first();
        
        return view('switch-management-address', compact("module", "settings"));
    }

    public function store(SwitchManagementAddress $request) {
        $switch_management_address = new SwitchManagementAddressModel();
        $switch_management_address->company_name = $request->company_name;
        $switch_management_address->eik = $request->eik;
        $switch_management_address->zone_id = $request->zone;
        $switch_management_address->township_id = $request->township;
        $switch_management_address->postcode = $request->postcode;
        $switch_management_address->quarter = $request->quarter;
        $switch_management_address->street = $request->street;
        $switch_management_address->street_number = $request->input('street_number');
        $switch_management_address->building = $request->building;
        $switch_management_address->vhod = $request->vhod;
        $switch_management_address->floor = $request->floor;
        $switch_management_address->apartment = $request->apartment;
        $switch_management_address->owner_firstname = $request->owner_firstname;
        $switch_management_address->owner_secondname = $request->owner_secondname;
        $switch_management_address->owner_lastname = $request->owner_lastname;
        $switch_management_address->owner_email = $request->owner_email;
        $switch_management_address->owner_id_expiry_date = $request->owner_id_expiry_date;
        $switch_management_address->owner_phone = $request->owner_phone;

        // Payment methods
        $switch_management_address->payment_method = $request->payment_method;

        // Payer info
        $switch_management_address->payer_firstname = $request->payer_firstname;
        $switch_management_address->payer_lastname = $request->payer_lastname;
        $switch_management_address->payer_email = $request->payer_email;
        $switch_management_address->payer_phone = $request->payer_phone;


        $switch_management_address->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function adminIndex()
    {
        $settings = PageSetting::where('name', 'switch-management-address')->first();

        $switches = SwitchManagementAddressModel::latest()->get();

        return view('admin.switch-managment-address', compact("settings", "switches"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (SwitchManagementAddressModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'switch-management-address')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'switch-management-address')->first();

        return $this->paymentExecute($request, $settings);
    }
}
