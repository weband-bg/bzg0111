<?php

namespace App\Http\Controllers;

use App\CarMake;
use App\CarSaleProxy as CarSaleProxyModel;
use App\CarType;
use App\Http\Requests\CarSaleProxy;
use App\Module;
use App\PageSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CarSaleProxyController extends Controller
{
    public function create()
    {
        $settings = PageSetting::where('name', 'car-sale-proxy')->first();

        $module = Module::where('name', 'Пълномощно за продажба на МПС')->first();

        $car_types = CarType::all();
        $car_makes = CarMake::all();

        return view('car-sale-proxy-create', compact("module", "settings", "car_types", "car_makes"));
    }

    public function store(CarSaleProxy $request)
    {
        $car_sale_proxy = new CarSaleProxyModel();

        $car_sale_proxy->seller_type = $request->seller_type;

        if ($request->seller_type == 'individual') {
            $car_sale_proxy->seller_firstname = $request->seller_firstname;
            $car_sale_proxy->seller_secondname = $request->seller_secondname;
            $car_sale_proxy->seller_lastname = $request->seller_lastname;
            $car_sale_proxy->seller_pin = $request->seller_pin;   
            $car_sale_proxy->seller_id_number = $request->seller_id_number;
            $car_sale_proxy->seller_id_date_created = $request->seller_id_date_created;
            $car_sale_proxy->seller_id_created_by = $request->seller_id_created_by;
        }
        elseif($request->seller_type == 'legal-entity') {
            $car_sale_proxy->seller_name = $request->seller_name;
            $car_sale_proxy->seller_eik = $request->seller_eik;
            $car_sale_proxy->manager_firstname = $request->manager_firstname;
            $car_sale_proxy->manager_secondname = $request->manager_secondname;
            $car_sale_proxy->manager_lastname = $request->manager_lastname;
            $car_sale_proxy->manager_pin = $request->manager_pin;
            $car_sale_proxy->manager_id_number = $request->manager_id_number;
            $car_sale_proxy->manager_id_date_created = $request->manager_id_date_created;
            $car_sale_proxy->manager_id_created_by = $request->manager_id_created_by;
        }

        $car_sale_proxy->proxy_firstname = $request->proxy_firstname;
        $car_sale_proxy->proxy_secondname = $request->proxy_secondname;
        $car_sale_proxy->proxy_lastname = $request->proxy_lastname;
        $car_sale_proxy->proxy_pin = $request->proxy_pin;
        $car_sale_proxy->proxy_id_number = $request->proxy_id_number;
        $car_sale_proxy->proxy_id_date_created = $request->proxy_id_date_created;
        $car_sale_proxy->proxy_id_created_by = $request->proxy_id_created_by;

        if ($request->hasFile('talon')) {
            $car_sale_proxy->talon = Storage::disk('uploads')->putFile('talons', $request->file('talon'));
        }

        $car_sale_proxy->car_type_id = $request->car_type;
        $car_sale_proxy->car_make_id = $request->car_make;
        $car_sale_proxy->car_model = $request->car_model;
        $car_sale_proxy->car_chassis = $request->car_chassis;
        $car_sale_proxy->car_registration = $request->car_registration;
        $car_sale_proxy->car_color = $request->car_color;

        $car_sale_proxy->price = $request->price;

        if ($request->self_contract_clause == 'on') {
            $car_sale_proxy->self_contract_clause = true;
        }

        // Payment methods
        $car_sale_proxy->payment_method = $request->payment_method;

        // Payer info
        $car_sale_proxy->payer_firstname = $request->payer_firstname;
        $car_sale_proxy->payer_lastname = $request->payer_lastname;
        $car_sale_proxy->payer_email = $request->payer_email;
        $car_sale_proxy->payer_phone = $request->payer_phone;

        $car_sale_proxy->save();

        return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }


    public function adminIndex()
    {
        $proxies = CarSaleProxyModel::all();

        $settings = PageSetting::where('name', 'car-sale-proxy')->first();

        return view('admin.car-sale-proxy', compact("settings", "proxies"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (CarSaleProxyModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }
}
