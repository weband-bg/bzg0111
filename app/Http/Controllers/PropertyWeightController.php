<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyWeight;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\PropertyWeight as PropertyWeightModel;
use Illuminate\Http\Request;

class PropertyWeightController extends Controller
{
    use PaymentTrait;
    
    public function index()
    {
        $settings = PageSetting::where('name', 'property-weight')->first();

        $module = Module::where('name', 'Има ли тежести върху имота ми')->first();
        
    	return view('property-weight', compact("module", "settings"));
    }

    public function store(PropertyWeight $request)
    {
    	// dd($request->all());
    	// exit;

    	$property_weight = new PropertyWeightModel;

    	$property_weight->type = $request->type;

        if ($request->type == 'individual') {
            $property_weight->firstname = $request->firstname;
            $property_weight->secondname = $request->secondname;
            $property_weight->lastname = $request->lastname;
            $property_weight->pin = $request->pin;
        }
        elseif($request->type == 'legal-entity') {
            $property_weight->name = $request->name;
            $property_weight->eik = $request->eik;
        }

    	$property_weight->zone_id = $request->zone;
    	$property_weight->township_id = $request->township;
        $property_weight->postcode = $request->postcode;
    	$property_weight->quarter = $request->quarter;
    	$property_weight->street = $request->street;
    	$property_weight->street_number = $request->street_number;
    	$property_weight->building = $request->building;
    	$property_weight->entrance = $request->entrance;
    	$property_weight->floor = $request->floor;
    	$property_weight->apartment = $request->apartment;
    	$property_weight->owner_firstname = $request->owner_firstname;
    	$property_weight->owner_secondname = $request->owner_secondname;
    	$property_weight->owner_lastname = $request->owner_lastname;
    	$property_weight->owner_pin = $request->owner_pin;

        // Payment methods
        $property_weight->payment_method = $request->payment_method;

        // Payer info
        $property_weight->payer_firstname = $request->payer_firstname;
        $property_weight->payer_lastname = $request->payer_lastname;
        $property_weight->payer_email = $request->payer_email;
        $property_weight->payer_phone = $request->payer_phone;

    	$property_weight->save();

    	return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
    }

    public function list()
    {
        $settings = PageSetting::where('name', 'property-weight')->first();

    	$property_weights = PropertyWeightModel::all();

    	return view('admin.property-weight.property_weights_list', compact("property_weights", "settings"));
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            if (PropertyWeightModel::destroy($request->id)) {
                return response()->json('success');
            }
            return response()->json('error');
        }

        return redirect()->back();
    }

    // Paypal
    public function paypalCreate(Request $request)
    {
        $settings = PageSetting::where('name', 'property-weight')->first();

        return $this->paymentCreate($request, $settings->price);
    }

    public function paypalExecute(Request $request)
    {
        $settings = PageSetting::where('name', 'property-weight')->first();

        return $this->paymentExecute($request, $settings);
    }
}
