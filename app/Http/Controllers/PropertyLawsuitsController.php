<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyLawsuit;
use App\Http\Traits\PaymentTrait;
use App\Module;
use App\PageSetting;
use App\PropertyLawsuit as PropertyLawsuitModel;
use Illuminate\Http\Request;


class PropertyLawsuitsController extends Controller
{
  use PaymentTrait;
  
  public function create()
  {
    $settings = PageSetting::where('name', 'property-lawsuit')->first();

    $module = Module::where('name', 'Водят ли се съдебни дела за имота ми')->first();
    
    return view('property-lawsuit-create', compact("module", "settings"));
  }

  public function store(PropertyLawsuit $request)
  {
    $property_lawsuit = new PropertyLawsuitModel;

    $property_lawsuit->type = $request->type;

    if ($request->type == 'legal-entity') {
      $property_lawsuit->name = $request->name;
      $property_lawsuit->eik = $request->eik;
    }

    elseif($request->type == 'individual') {
      $property_lawsuit->firstname = $request->firstname;
      $property_lawsuit->secondname = $request->secondname;
      $property_lawsuit->lastname = $request->lastname;
      $property_lawsuit->pin = $request->pin;
    }   
     
    $property_lawsuit->zone_id = $request->zone;
    $property_lawsuit->township_id = $request->township;
    $property_lawsuit->postcode = $request->postcode;
    $property_lawsuit->quarter = $request->quarter;
    $property_lawsuit->street = $request->street;
    $property_lawsuit->street_number = $request->street_number;
    $property_lawsuit->building = $request->building;
    $property_lawsuit->entrance = $request->entrance;
    $property_lawsuit->floor = $request->floor;
    $property_lawsuit->apartment = $request->apartment;
    $property_lawsuit->owner_firstname = $request->owner_firstname;
    $property_lawsuit->owner_secondname = $request->owner_secondname;
    $property_lawsuit->owner_lastname = $request->owner_lastname;
    $property_lawsuit->owner_pin = $request->owner_pin;

    // Payment methods
    $property_lawsuit->payment_method = $request->payment_method;

    // Payer info
    $property_lawsuit->payer_firstname = $request->payer_firstname;
    $property_lawsuit->payer_lastname = $request->payer_lastname;
    $property_lawsuit->payer_email = $request->payer_email;
    $property_lawsuit->payer_phone = $request->payer_phone;

    $property_lawsuit->save();

    return redirect()->back()->with(['success' => 'Заявката беше изпратена успешно']);
  }

  public function adminIndex()
  {
      $settings = PageSetting::where('name', 'property-lawsuit')->first();

      $properties = PropertyLawsuitModel::all();

      return view('admin.property-lawsuit', compact("properties", "settings"));
  }

  public function delete(Request $request)
  {
    if ($request->ajax()) {
      if (PropertyLawsuitModel::destroy($request->id)) {
        return response()->json('success');
      }
        return response()->json('error');
    }

    return redirect()->back();
  }

  // Paypal
  public function paypalCreate(Request $request)
  {
      $settings = PageSetting::where('name', 'property-lawsuit')->first();

      return $this->paymentCreate($request, $settings->price);
  }

  public function paypalExecute(Request $request)
  {
      $settings = PageSetting::where('name', 'property-lawsuit')->first();

      return $this->paymentExecute($request, $settings);
  }
}
