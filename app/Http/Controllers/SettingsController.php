<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePageSettings;
use App\PageSetting;
use App\QuestionAnswer;
use App\QuestionAnswerCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    private function hashImageName($image) {
        return md5($image->getClientOriginalName() . uniqid('weband')) . '.' . $image->getClientOriginalExtension();
    }

    public function index($page)
    {
        $settings = PageSetting::where('name', $page)->first();

    	return view('admin.settings.index', compact("settings", "categories"));
    }

    public function store(StorePageSettings $request)
    {
 		$pageSetting = PageSetting::find($request->id);
    	if (!$pageSetting) {
    		$pageSetting = new PageSetting;
    		$pageSetting->name = $request->name;
    	}

    	$pageSetting->page_title = $request->title;
    	$pageSetting->meta_keywords = $request->keywords;
    	$pageSetting->meta_description = $request->description;
        $pageSetting->text = $request->text;

         //image
        if ($request->hasFile('image') && $request->image->isValid()) {
            $image = $request->image;
            $image_name = $this->hashImageName($image);

            Storage::disk('settings_images')->putFileAs('', $image, $image_name);


            if ($pageSetting->image) {
                unlink(public_path().config('app.settings-images').$pageSetting->image);
            }

            $pageSetting->image = $image_name;
        }

        $pageSetting->price = $request->price;
        $pageSetting->foreign_price = $request->foreign_price;

        // Dissociate past question answers
        foreach ($pageSetting->questionAnswers as $question_answer) {
            $question_answer->delete();
        }

        if ($request->has('question_answers')) {
            foreach ($request->question_answers as $question_answer) {
                $question_answer_model = new QuestionAnswer;
                $question_answer_model->pageSettings()->associate($pageSetting);
                $question_answer_model->question = $question_answer['question'];
                $question_answer_model->answer = $question_answer['answer'];
                $question_answer_model->category()->associate($question_answer['category']);
                $question_answer_model->save();
            }
        }

    	$pageSetting->save();

    	return redirect()->back()->with(['success' => 'Настройките на страницата бяха запаметени']);
    }

    public function getCategories()
    {
        $categories = QuestionAnswerCategory::all();

        return response()->json($categories);
    }

    public function getQuestionAnsers($id)
    {
        $question_answers = QuestionAnswer::with('category')->where('page_settings_id', $id)->get();

        return response()->json($question_answers);
    }
}
