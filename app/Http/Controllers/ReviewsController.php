<?php

namespace App\Http\Controllers;

use App\Http\Traits\PaymentTrait;

use App\Http\Requests\Review as ReviewRequest;
use App\Review;
use App\User;
use Auth;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
	public function save(Request $request)
	{
		$review = new Review;

        $review->page()->associate($request->page);
		$review->name = $request->review_name;
		$review->email = $request->review_email;
        $review->phone = $request->review_phone;
		$review->text = $request->review_text;
		$review->save();
	}

    public function index()
    {
    	return view('reviews.reviews');
    }

    public function store(ReviewRequest $request)
    {
    	$this->save($request);

    	return redirect()->back()->with(['success' => 'Отзивът беше изпратен и очаква одобрение']);
    }

    public function list()
    {
    	$reviews = Review::all();

    	return view('admin.reviews', compact("reviews"));
    }

    public function changeApproved(Request $request)
    {
        if ($request->ajax()) {
            $review = Review::find($request->id);

            if ($review->approved) {
                $review->approved = false;
            }
            else {
                $review->approved = true;
            }

            $review->save();

            if ($review->approved) {
                return response()->json('approved');
            }
            else {
                return response()->json('disapproved');
            }
        }

        return redirect()->back();
    }

    public function delete(Request $request)
    {
    	if ($request->ajax()) {
    		if (Review::destroy($request->id)) {
    			return response()->json('success');
    		}

    		return response()->json('error');
    	}

    	return redirect()->back();
    }
}
