<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CompanyET extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'sole_trader_name' => 'required|alpha_num|max:190',
            'sole_trader_foreign_name' => 'required|alpha_num|max:190',

            'zone' => 'sometimes|nullable|numeric',
            'township' => 'sometimes|nullable|numeric',
            'populated-place' => 'sometimes|nullable|numeric',
            'postcode' => 'sometimes|nullable|alpha_dash',
            'quarter' => 'sometimes|nullable|alpha_dash',
            'street' => 'sometimes|nullable|alpha_dash',
            'street-number' => 'sometimes|nullable|numeric',
            'building' => 'sometimes|nullable|alpha_dash',
            'vhod' => 'sometimes|nullable|alpha_dash',
            'floor' => 'sometimes|nullable|alpha_dash',
            'apartment' => 'sometimes|nullable|alpha_dash',


            'owner_firstname' => 'required|alpha_dash',
            'owner_secondname' => 'required|alpha_dash',
            'owner_lastname' => 'required|alpha_dash',
            'owner_pin' => 'required|digits:10',
            'owner_id_number' => 'required|digits:9',
            'owner_id_date_created' => 'required|before_or_equal:'.date('Y-m-d'),
            'owner_id_created_by' => 'required|alpha_dash',
        ];

        if ($request->has('different-address') && $request->input('different-address') == 'on') {
            $rules['address-2-zone'] = 'required_if:different-address,on|numeric';
            $rules['address-2-township'] = 'required_if:different-address,on|numeric';
            $rules['address-2-populated-place'] = 'required_if:different-address,on|numeric';
            $rules['address-2-postcode'] = 'required_if:different-address,on';
            $rules['address-2-quarter'] = 'required_if:different-address,on|alpha_num';
            $rules['address-2-street'] = 'required_if:different-address,on|alpha_num';
            $rules['address-2-street-number'] = 'required_if:different-address,on|numeric';
            $rules['address-2-building'] = 'required_if:different-address,on|alpha_num';
            $rules['address-2-vhod'] = 'required_if:different-address,on|alpha_num';
            $rules['address-2-floor'] = 'required_if:different-address,on|alpha_num';
            $rules['address-2-apartment'] = 'required_if:different-address,on|alpha_num';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'sole_trader_name.required' => 'Името на фирмата е задължително',
            'sole_trader_name.alpha_num' => 'Невалидно име на фирма',
            'sole_trader_name.max' => 'Името на фирмата не може да надхвърля :max символа',
            'sole_trader_foreign_name.required' => 'Името на фирмата на чужд език е задължително',
            'sole_trader_foreign_name.alpha_num' => 'Невалидно име на фирмата на чужд език',
            'sole_trader_foreign_name.max' => 'Името на фирмата на чужд език не може да надхвърля :max символа',
            'zone.required' => 'Областа е задължителна',
            'zone.numeric' => 'Невалидна област',
            'township.numeric' => 'Невалидна община',
            'populated-place.numeric' => 'Невалидно населено място',
            'quarter.required' => 'Кврталът е задължителен',
            'quarter.alpha_num' => 'Невалиден квартал',
            'street.required' => 'Улицата е задължителна',
            'street.alpha_num' => 'Невалидна улица',
            'street-number.required' => 'Номерът на улицата е задължителен',
            'street-number.numeric' => 'Невалиден номер на улица',
            'building.required' => 'Блокът е задължителен',
            'building.alpha_num' => 'Невалиден блок',
            'vhod.required' => 'Входът е задължителен',
            'vhod.alpha_num' => 'Невалиден вход',
            'floor.required' => 'Етажът е задължителен',
            'floor.alpha_num' => 'Невалиден етаж',
            'apartment.required' => 'Апартаментът е задължителен',
            'apartment.alpha_num' => 'Невалиден апартамент',

            'owner_firstname.required' => 'Първото име на собственика е задължително',
            'owner_firstname.alpha_dash' => 'Невалидно първо име на собственика',
            'owner_secondname.required' => 'Пезимето на собственика е задължително',
            'owner_secondname.alpha_dash' => 'Невалидно презиме на собственика',
            'owner_lastname.required' => 'Фамилията на собственика е задължителна',
            'owner_lastname.alpha_dash' => 'Невалидна фамилия на собственика',
            'owner_pin.required' => 'ЕГН-то на собственика е задължително',
            'owner_pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)',
            'owner_id_number.required' => 'Номерът на личната карта е задължителен',
            'owner_id_number.digits' => 'Невалиден номер на лична карта (Номерът на личната карта съдържа точно :digits цифри)',
            'owner_id_date_created.required' => 'Датата на създаване е задължителна',
            'owner_id_date_created.before_or_equal' => 'Датата на създаване не може да е по-голяма от днешната дата',
            'owner_id_created_by.required' => '"Издадена от" е задължително поле'
        ];
    }
}
