<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyFortune extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eik' => 'required|digits:9',
            'company_name' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'eik.required' => 'ЕИК-то е задължително',
            'eik.digits' => 'ЕИК-то трябва да е 9 цифри',
            'company_name.required' => 'Името на фирмата е задължително'
        ];
    }
}
