<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OwnerEstate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|in:legal-entity,individual',
            
            'firstname' => 'required_if:type,individual',
            'secondname'=> 'required_if:type,individual',
            'lastname' => 'required_if:type,individual',
            'pin' => 'required_if:type,individual|nullable|digits:10',

            'name' => 'required_if:type,legal-entity',
            'eik' => 'required_if:type,legal-entity|nullable|digits:9'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Задължително трябва да изберете',
            'type.in' => 'Избрали сте невалидна опиця',

            'firstname.required_if' => 'Името е задължително',
            'secondname.required_if' => 'Презимето е задължително',
            'lastname.required_if' => 'Фамилията е задължителна',
            'pin.required_if' => 'ЕГН-то е задължително',
            'pin.digits' => 'ЕГН-то трябва да е 10 цифри',

            'name.required_if' => 'Името е задължително',
            'eik.required_if' => 'ЕИК-то е задължително',
            'eik.digits' => 'ЕИК-то трябва да е точно 9 цифри'
        ];
    }
}
