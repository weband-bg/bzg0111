<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CompanyOOD extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'company_name' => 'required|max:190',
            'company_foreign_language_name' => 'required|max:190',
            'zone' => 'sometimes|nullable|numeric',
            'township' => 'sometimes|nullable|numeric',
            'postcode' => 'sometimes|nullable',
            'quarter' => 'sometimes|nullable',
            'street' => 'sometimes|nullable',
            'street_number' => 'sometimes|nullable|numeric',
            'building' => 'sometimes|nullable',
            'vhod' => 'sometimes|nullable',
            'floor' => 'sometimes|nullable',
            'apartment' => 'sometimes|nullable',

            'money' => 'required|numeric|min:2',

            'owner.*.firstname' => 'required',
            'owner.*.secondname' => 'required',
            'owner.*.lastname' => 'required',
            'owner.*.pin' => 'required|digits:10',
            'owner.*.id_number' => 'required|digits:9',
            'owner.*.id_date_created' => 'required|before_or_equal:'.date('Y-m-d'),
            'owner.*.id_created_by' => 'required',
            'owner.*.money' => 'required',

            // Payment
            'payer_firstname' => 'required|string',
            'payer_lastname' => 'required|string',
            'payer_email' => 'required|email',
            'payer_phone' => 'required|string'
        ];

        if ($request->has('different_address') && $request->different_address == 'on') {
            $rules['address_2_zone'] = 'required_if:different_address,on|numeric';
            $rules['address_2_township'] = 'required_if:different_address,on|numeric';
            $rules['address_2_populated_place'] = 'required_if:different_address,on|numeric';
            $rules['address_2_postcode'] = 'required_if:different_address,on';
            $rules['address_2_quarter'] = 'required_if:different_address,on|alpha_num';
            $rules['address_2_street'] = 'required_if:different_address,on|alpha_num';
            $rules['address_2_street_number'] = 'required_if:different_address,on|numeric';
            $rules['address_2_building'] = 'required_if:different_address,on|alpha_num';
            $rules['address_2_vhod'] = 'required_if:different_address,on|alpha_num';
            $rules['address_2_floor'] = 'required_if:different_address,on|alpha_num';
            $rules['address_2_apartment'] = 'required_if:different_address,on|alpha_num';
        }

        if ($request->manager) {
            $rules['manager.*.firstname'] = 'required';
            $rules['manager.*.secondname'] = 'required';
            $rules['manager.*.lastname'] = 'required';
            $rules['manager.*.pin'] = 'required|digits:10';
            $rules['manager.*.id_number'] = 'required|digits:9';
            $rules['manager.*.id_date_created'] = 'required|before_or_equal:' . date('Y-m-d');
            $rules['manager.*.id_created_by'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Името на фирмата е задължително',
            'company_name.alpha_num' => 'Невалидно име на фирма',
            'company_name.max' => 'Името на фирмата не може да надхвърля :max символа',
            'company_foreign_language_name.required' => 'Името на фирмата на чужд език е задължително',
            'company_foreign_language_name.alpha_num' => 'Невалидно име на фирмата на чужд език',
            'company_foreign_language_name.max' => 'Името на фирмата на чужд език не може да надхвърля :max символа',
            'zone.required' => 'Областа е задължителна',
            'zone.numeric' => 'Невалидна област',
            'township.required' => 'Общината е задължителна',
            'township.numeric' => 'Невалидна община',
            'populated_place.required' => 'Населеното място е задължително',
            'populated_place.numeric' => 'Невалидно населено място',
            'postcode.required' => 'Пощенският код е задължителен',
            'quarter.required' => 'Кврталът е задължителен',
            'quarter.alpha_num' => 'Невалиден квартал',
            'street.required' => 'Улицата е задължителна',
            'street.alpha_num' => 'Невалидна улица',
            'street_number.required' => 'Номерът на улицата е задължителен',
            'street_number.numeric' => 'Невалиден номер на улица',
            'building.required' => 'Блокът е задължителен',
            'building.alpha_num' => 'Невалиден блок',
            'vhod.required' => 'Входът е задължителен',
            'vhod.alpha_num' => 'Невалиден вход',
            'floor.required' => 'Етажът е задължителен',
            'floor.alpha_num' => 'Невалиден етаж',
            'apartment.required' => 'Апартаментът е задължителен',
            'apartment.alpha_num' => 'Невалиден апартамент',

            'money.required' => 'Капиталът е задължителен',
            'money.numeric' => 'Капиталът може да бъде само цели числа',

            'owner.*.firstname.required' => 'Първото име на собственика е задължително',
            'owner.*.secondname.required' => 'Презимето на собственика е задължително',
            'owner.*.lastname.required' => 'Фамилията на собственика е задължителна',
            'owner.*.pin.required' => 'ЕГН-то на собственика е задължително',
            'owner.*.pin.digits' => 'ЕГН-то на  собственика трябва да е точно :digits цифри',
            'owner.*.id_number.required' => 'Номерът на личната карта е задължителен',
            'owner.*.id_date_created.required' => 'Датата на създаване е задължителна',
            'owner.*.id_created_by.required' => '"Издадена от" е задължително поле',
            'owner.*.shareholding' => 'Дяловото участие е задължително',

            'manager.*.firstname.required' => 'Името на управителя е задължително',
            'manager.*.firstname.alpha_dash' => 'Невалидно име на управителя',
            'manager.*.secondname.required' => 'Презимето на управителя е задължително',
            'manager.*.lastname.required' => 'Фамилията на управителя е задължителна',
            'manager.*.pin.required' => 'ЕГН-то на управителя е задължително',
            'manager.*.pin.digits' => 'ЕГН-то на управителя трябва да бъде точно :digits цифри',
            'manager.*.id_number.required' => 'Номерът на личната карта е задължителен',
            'manager.*.id_date_created.required' => 'Датата на издаване на личната карта е задължителна',
            'manager.*.id_date_createdbefore_or_equal' => 'Датата на издаване не може да е по_голяма от днешната дата',
            'manager.*.id_created_by.required' => '"Издадена от" е задължително поле',

            // Payment
            'payer_firstname.required' => 'Името е задължително',
            'payer_firstname.string' => 'Невалидно име',
            'payer_lastname.required' => 'Фамилията е задължителна',
            'payer_lastname.string' => 'Невалидна фамилия',
            'payer_email.required' => 'Email-ът е задължителен',
            'payer_email.email' => 'Невалиден email',
            'payer_phone.required' => 'Телеофнът е задължителен',
            'payer_phone.string' => 'Невалиден телефон'
        ];
    }
}
