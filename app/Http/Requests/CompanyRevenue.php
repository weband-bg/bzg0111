<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRevenue extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'eik' => 'required|digits:9'
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Името е задължително',
            'eik.required' => 'ЕИК е задължително',
            'eik.digits' => 'Невалидно  ЕИК (ЕИК-то трябва да е 9 цифри)'
        ];
    }
}
