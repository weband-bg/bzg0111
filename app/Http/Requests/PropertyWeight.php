<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PropertyWeight extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'type' => 'required|in:legal-entity,individual',

            'zone' => 'required|numeric',
            'township' => 'required|numeric',
            'postcode' => 'required',
            'quarter' => 'required',
            'street' => 'required',
            'street_number' => 'required|numeric',
            'building' => 'required',
            'entrance' => 'required',
            'floor' => 'required',
            'apartment' => 'required|numeric',

            'owner_firstname' => 'required',
            'owner_secondname' => 'required',
            'owner_lastname' => 'required',
            'owner_pin' => 'required|digits:10'
        ];

        if ($request->has('type') && $request->type == 'individual') {
            $rules['firstname'] = 'required_if:type,individual';
            $rules['secondname'] = 'required_if:type,individual';
            $rules['lastname'] = 'required_if:type,individual';
            $rules['pin'] = 'required_if:type,individual|digits:10';
        }

        if ($request->has('type') && $request->type == 'legal-entity') {
            $rules['name'] = 'required_if:type,legal-entity';
            $rules['eik'] = 'required_if:type,legal-entity|digits:9';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'type.required' => 'Задължително трябва да изберете',
            'type.in' => 'Избрали сте невалидна опиця',
            'firstname.required_if' => 'Името е задължително',
            'secondname.required_if' => 'Презимето е задължително',
            'lastname.required_if' => 'Фамилията е задължителна',
            'pin.required_if' => 'ЕГН-то е задължително',
            'pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)',

            'name.required_if' => 'Името е задължително',
            'eik.required_if' => 'ЕИК-то е задължително',
            'eik.digits' => 'Невалидно ЕИК (ЕИК-то трябва да съдържа точно :digits цифри)',

            'zone.required' => 'Областа е задължителна',
            'zone.numeric' => 'Невалидна област',
            'township.required' => 'Общината е задължителна',
            'township.numeric' => 'Невалидна община',
            'postcode.required' => 'Пощенския код е задължителен',
            'quarter.required' => 'Кварталът е задължително поле',
            'street.required' => 'Улицата е задължително поле',
            'street-number.required' => 'Номерът на улицата е задължителен',
            'street-number.numeric' => 'Невалиден номер на улица',
            'building.required' => 'Блокът е задължително поле',
            'entrance.required' => 'Входът е задължително поле',
            'floor.required' => 'Етажът е задължителен',
            'apartment.required' => 'Апартаментът е задължителен',
            'apartment.numeric' => 'Невалиден апартамент',

            'owner_firstname.required' => 'Името е задължително',
            'owner_secondname.required' => 'Презимето е задължително',
            'owner_lastname.required' => 'Фамилията е задължителна',
            'owner_pin.required' => 'ЕГН-то е задължително',
            'owner_pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)'
        ];
    }
}
