<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Review extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'review_name' => 'nullable|string|max:190',
            'review_email' => 'email|max:190',
            'review_text' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'review_name.string' => 'Името съдържа невалидни символи',
            'review_name.max' => 'Името не може да надхвърля :max символа',
            'review_email.email' => 'Невалиен email',
            'review_email.max' => 'Email-ът не може надхвърля :max символа',
            'review_text.required' => 'Ревюто е задължително'
        ];
    }
}
