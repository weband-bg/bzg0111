<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SwitchManagementAddress extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'eik' => 'required|digits:9',
            'zone' => 'required',
            'township' => 'required',
            'postcode' => 'required',
            'quarter' => 'required',
            'building' => 'required',
            'vhod' => 'required',
            'floor' => 'required',
            'apartment' => 'required',
            'owner_firstname' => 'required',
            'owner_secondname' => 'required',
            'owner_lastname' => 'required',
            'owner_email' => 'required',
            'owner_id_expiry_date' => 'required',
            'owner_phone' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Името на фирмата е задължително поле',
            'eik.required' => 'ЕИК-то на фирмата е задължително поле',
            'eik.digits' => 'ЕИК-то на фирмата трябва да бъде точно 9 цифри',
            'zone.required' => 'Областа е задължително поле',
            'township.required' => 'Общината е задължително поле',
            'postcode.required' => 'Пощенският код е задължително поле',
            'quarter.required' => 'Кварталът е задължително поле',
            'building.required' => 'Блокът е задължително поле',
            'vhod.required' => 'Входът е задължително поле',
            'floor.required' => 'Етажът е задължително поле',
            'apartment.required' => 'Апартаментът е задължително поле',
            'owner_firstname.required' => 'Името на собственика е задължително',
            'owner_secondname.required' => 'Презимето на собственика е задължително',
            'owner_lastname.required' => 'Фамилията на собственика е задължителна',
            'owner_email.required' => 'Email-ът е задължително поле',
            'owner_id_expiry_date.required' => 'Датата на валидност на лична карта е задължителна',
            'owner_phone.required' => 'Телефонът е задължителен'
        ];
    }
}
