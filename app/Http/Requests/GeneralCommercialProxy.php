<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GeneralCommercialProxy extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string|max:190',
            'eik' => 'required|digits:9',
            'manager_firstname' => 'required|string|max:190',
            'manager_secondname' => 'required|string|max:190',
            'manager_lastname' => 'required|string|max:190',
            'manager_pin' => 'required|digits:10',
            'manager_id_number' => 'required|digits:9',
            'manager_id_date_created' => 'required',
            'manager_id_created_by' => 'required',
            'proxy_firstname' => 'required|string|max:190',
            'proxy_secondname' => 'required|string|max:190',
            'proxy_lastname' => 'required|string|max:190',
            'proxy_pin' => 'required|digits:10',
            'proxy_id_number' => 'required|digits:9',
            'proxy_id_date_created' => 'required',
            'proxy_id_created_by' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Името на фирмата е задължително',
            'company_name.string' => 'Невалидно име на фирма',
            'company_name.max' => 'Името на фирмата не може да бъде повече от :max символа',
            'eik.required' => 'ЕИК-то е задължително',
            'eik.digits' => 'Невалидно ЕИК (ЕИк-то трябва да бъде точно :digits цифри)',
            'manager_firstname.required' => 'Името е задължително',
            'manager_firstname.string' => 'Невалидно име',
            'manager_firstname.max' => 'Името не може да бъде повече от :max символа',
            'manager_secondname.required' => 'Презимето е задължително',
            'manager_secondname.string' => 'Невалидно презиме',
            'manager_secondname.max' => 'Презимето не може да бъде повече от :max символа',
            'manager_lastname.required' => 'Фамилията е задължителна',
            'manager_lastname.string' => 'Невалидна фамилия',
            'manager_lastname.max' => 'Фамилията не може да бъде повече от :max символа',
            'manager_pin.required' => 'ЕГН-то е задължително',
            'manager_pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)',
            'manager_id_number.required' => 'Номерът на личната карта е задължителен',
            'manager_id_number.digits' => 'Неваден номер на лична карта (Номерът на личната карта трябва да съдържа точно :digits цифри)',
            'manager_id_date_created.required' => 'Датата на издаване е задължителна',
            'manager_id_created_by' => 'Издадена от е задължително поле',
            'proxy_firstname.required' => 'Името е задължително',
            'proxy_firstname.string' => 'Невалидно име',
            'proxy_firstname.max' => 'Името не може да бъде повече от :max символа',
            'proxy_secondname.required' => 'Презимето е задължително',
            'proxy_secondname.string' => 'Невалидно презиме',
            'proxy_secondname.max' => 'Презимето не може да бъде повече от :max символа',
            'proxy_lastname.required' => 'Фамилията е задължителна',
            'proxy_lastname.string' => 'Невалидна фамилия',
            'proxy_lastname.max' => 'Фамилията не може да бъде повече от :max символа',
            'proxy_pin.required' => 'ЕГН-то е задължително',
            'proxy_pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)',
            'proxy_id_number.required' => 'Номерът на личната карта е задължителен',
            'proxy_id_number.digits' => 'Неваден номер на лична карта (Номерът на личната карта трябва да съдържа точно :digits цифри)',
            'proxy_id_date_created.required' => 'Датата на издаване е задължителна',
            'proxy_id_created_by.required' => 'Издадена от е задължително поле'
        ];
    }
}
