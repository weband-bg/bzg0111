<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePageSettings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->admin == 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'title' => 'required|max:190'
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Името на страницата е задължително',
            'title.required' => 'Заглавието на страницата е задължително',
            'title.max' => 'Заглавието на страницата не може да надхвърля :max символа',
        ];
    }
}
