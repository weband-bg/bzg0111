<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CompanyLTD extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'company_name' => 'required|alpha_num|max:190',
            'company_foreign_language_name' => 'required|alpha_num|max:190',

            'money' => 'required|numeric|min:2',
            'owner_firstname' => 'required',
            'owner_secondname' => 'required',
            'owner_lastname' => 'required',
            'owner_pin' => 'required|digits:10',
            'owner_id_number' => 'required|digits:9',
            'owner_id_date_created' => 'required|before_or_equal:'.date('Y-m-d'),
            'owner_id_created_by' => 'required',

            // Payment
            'payer_firstname' => 'required|string',
            'payer_lastname' => 'required|string',
            'payer_email' => 'required|email',
            'payer_phone' => 'required|string'
        ]; 

        if ($request->has('different_address') && $request->different_address == 'on') {
            $rules['address-2-zone'] = 'required_if:different_address,on|numeric';
            $rules['address-2-township'] = 'required_if:different_address,on|numeric';
            $rules['address-2-populated-place'] = 'required_if:different_address,on|numeric';
            $rules['address-2-postcode'] = 'required_if:different_address,on';
            $rules['address-2-quarter'] = 'required_if:different_address,on|alpha_num';
            $rules['address-2-street'] = 'required_if:different_address,on|alpha_num';
            $rules['address-2-street-number'] = 'required_if:different_address,on|numeric';
            $rules['address-2-building'] = 'required_if:different_address,on|alpha_num';
            $rules['address-2-vhod'] = 'required_if:different_address,on|alpha_num';
            $rules['address-2-floor'] = 'required_if:different_address,on|alpha_num';
            $rules['address-2-apartment'] = 'required_if:different_address,on|alpha_num';
        }

        if ($request->manager) {
            $rules['manager.*.firstname'] = 'required';
            $rules['manager.*.secondname'] = 'required';
            $rules['manager.*.lastname'] = 'required';
            $rules['manager.*.pin'] = 'required|digits:10';
            $rules['manager.*.id_number'] = 'required|digits:9';
            $rules['manager.*.id_date_created'] = 'required|before_or_equal:'.date('Y-m-d');
            $rules['manager.*.id_created_by'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'company_name.required' => 'Името на фирмата е задължително',
            'company_name.alpha_num' => 'Невалидно име на фирма',
            'company_name.max' => 'Името на фирмата не може да надхвърля :max символа',
            'company_foreign_language_name.required' => 'Името на фирмата на чужд език е задължително',
            'company_foreign_language_name.alpha_num' => 'Невалидно име на фирмата на чужд език',
            'company_foreign_language_name.max' => 'Името на фирмата на чужд език не може да надхвърля :max символа',

            'money.required' => 'Капиталът е задължителен',
            'money.numeric' => 'Капиталът може да бъде само цели числа',
            'owner_firstname.required' => 'Първото име на собственика е задължително',
            'owner_second-name.required' => 'Пезимето на собственика е задължително',
            'owner_lastname.required' => 'Фамилията на собственика е задължителна',
            'owner_pin.required' => 'ЕГН-то на собственика е задължително',
            'owner_pin.digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)',
            'owner_id_number.required' => 'Номерът на личната карта е задължителен',
            'owner_id_number.digits' => 'Невалиден номер на лична карта (Номерът на личната карта съдържа точно :digits цифри)',
            'owner_id_date_created.required' => 'Датата на създаване е задължителна',
            'owner_id_date_created.before_or_equal' => 'Датата на създаване не може да е по-голяма от днешната дата',
            'owner_id_created_by.required' => '"Издадена от" е задължително поле',

            // Payment
            'payer_firstname.required' => 'Името е задължително',
            'payer_firstname.string' => 'Невалидно име',
            'payer_lastname.required' => 'Фамилията е задължителна',
            'payer_lastname.string' => 'Невалидна фамилия',
            'payer_email.required' => 'Email-ът е задължителен',
            'payer_email.email' => 'Невалиден email',
            'payer_phone.required' => 'Телеофнът е задължителен',
            'payer_phone.string' => 'Невалиден телефон',

            'manager.*.firstname.required' => 'Името на управителя е задължително',
            'manager.*.secondname' => [
                'required' => 'Презимето на управителя е задължително',
            ],
            'manager.*.lastname' => [
                'required' => 'Фамилията на управителя е задължителна'
            ],
            'manager.*.pin' => [
                'required' => 'ЕГН-то на управителя е задължително',
                'digits' => 'Невалидно ЕГН (ЕГН-то трябва да съдържа точно :digits цифри)'
            ],
            'manager.*.id_number' => [
                'required' => 'Номерът на личната карта е задължителен',
                'digits' => 'Невалиден номер на лична карта (Номерът на личната карта трябва да е точно :digits символа)'
            ],
            'manager.*.id_date_created' => [
                'required' => 'Датата на издаване на личната карта е задължителна',
                'before_or_equal' => 'Датата на издаване не може да е по-голяма от днешната дата'
            ],
            'manager.*.id_created_by.required' => '"Издадена от" е задължително поле'
        ];
    }
}
