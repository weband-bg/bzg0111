<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EstateOpening extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'info_link' => 'required|url'
        ];
    }

    public function messages()
    {
        return [
            'info_link.required' => 'Линкът към информацията за имот е задължително поле',
            'info_link.url' => 'Невалиден линк'
        ];
    }
}
