<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignatureSample extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'secondname' => 'required',
            'lastname' => 'required',
            'pin' => 'required|digits:10'
        ];
    }

    public function messages()
    {
        return [
            'firstname.required' => 'Името е задължително',
            'secondname.required' => 'Презимето е задължително',
            'lastname.required' => 'Фамилията е задължителна',
            'pin.required' => 'ЕГН-то е задължително',
            'pin.digits' => 'ЕГН-то трябва да е 10 цифри'
        ];
    }
}
