<?php 

namespace App\Http\Traits;

use App\PaypalPayment;
use App\PaypalToken;
use Illuminate\Http\Request;

trait PaymentTrait {
	public function getToken()
	{
		$ch = curl_init('https://api.sandbox.paypal.com/v1/oauth2/token');

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Accept-Language: en_US', 'Content-type: application/x-www-form-urlencoded'));

		curl_setopt($ch, CURLOPT_USERPWD, config('app.paypal-client-id').':'.config('app.paypal-secret'));
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');

		$json = curl_exec($ch);
		$error = curl_error($ch);

		// if ($error) {
		// 	return $error;
		// }

		$result = json_decode($json, true);

		return $result['access_token'];
	}

	public function paymentCreate(Request $request, $price)
	{
		$paypal_token = PaypalToken::orderBy('created_at', 'desc')->first();
		$token = $paypal_token->token;

		$data_json = '{
			"intent": "sale",
			"redirect_urls": {
			    "return_url": "https://weband.bg?success=1",
			    "cancel_url": "https://weband.bg?error=1"
			},
			"payer": {
				"payment_method": "paypal"
			},
		  	"transactions": [{
		    	"amount": {
		    		"total": "'.$price.'",
		    		"currency": "EUR"
		    	}
		  	}]
		}';

		$ch = curl_init('https://api.sandbox.paypal.com/v1/payments/payment');

		// curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization: Bearer '.$token));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);                                                                    
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

		$result_raw = curl_exec($ch);
		$error = curl_error($ch);

		if ($error) {
			dd($error);
		}

		$result = json_decode($result_raw, true);

		if (array_key_exists('error', $result) && $result['error'] == 'invalid_token') {
			//Get new token and store it in DB
			$token = $this->getToken();  

			$paypal_token->token = $token;
			$paypal_token->save();

			return $this->paymentCreate($request, $price);
		}

		 return response()->json($result);
	}

	public function paymentExecute(Request $request, $settings)
	{
		$paypal_token = PaypalToken::orderBy('created_at', 'desc')->first();
		$token = $paypal_token->token;

		$data_json = '{
		                "payer_id": "'.$request->payerID.'"
		              }';

		$ch = curl_init('https://api.sandbox.paypal.com/v1/payments/payment/'.$request->paymentID.'/execute/');
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization: Bearer '.$token));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);                                                                    
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

		$result = curl_exec($ch);

		//Register it in the db
		$this->storePayment($result, $settings);

		return response()->json($result);
	}

	private function storePayment($result, $settings)
	{
		$result = json_decode($result, true);

		$paypal_payment = new PaypalPayment;
		$paypal_payment->module()->associate($settings);
		$paypal_payment->price = $result['transactions'][0]['amount']['total'];

		$paypal_payment->save();
	}
}