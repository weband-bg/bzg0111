<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopulatedPlace extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;

    public function township()
    {
    	return $this->belongsTo('App\Township');
    }
}
