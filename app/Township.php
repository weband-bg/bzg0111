<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Township extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
	
    public function zone()
    {
    	return $this->belongsTo('App\Zone');
    }

    public function populatedPlaces()
    {
    	return $this->hasMany('App\PopulatedPlace');
    }
}
