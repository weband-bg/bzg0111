<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswerCategory extends Model
{
    public function questionAnswers()
    {
    	return $this->hasMany('App\QuestionAnswer');
    }
}
