<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteSaleContract extends Model
{
    public function getSellerNames()
    {
        if ($this->seller_type == 'seller-legal-entity') {
            return $this->seller_name;
        } else {
            return $this->seller_firstname . ' ' . $this->seller_secondname . ' ' . $this->seller_lastname;
        }

    }

    public function getBuyerNames()
    {
        if ($this->buyer_type == 'buyer-legal-entity') {
            return $this->buyer_name;
        } else {
            return $this->buyer_firstname . ' ' . $this->buyer_secondname . ' ' . $this->buyer_lastname;
        }

    }

    public function getClientNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }
}
