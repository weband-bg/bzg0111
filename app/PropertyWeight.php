<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyWeight extends Model
{
    public function getAddress()
    {
        return 'Област: ' . $this->zone->name . ', Община: ' . $this->township->name . ', Населено място: '.$this->populatedPlace->name.', Пощенски код: '. $this->postcode .', Квартал: ' . $this->quarter . ', Улица: ' . $this->street . ', Номер: ' . $this->street_number . ', Блок: ' . $this->building . ', Вход: ' . $this->entrance . ', Етаж: ' . $this->floor . ', Апартамент: ' . $this->apartment;
    }

    public function getOwnerNames()
    {
    	return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function getPersonNames()
    {
    	return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    }

    public function getOwnerPIN()
    {
    	return 'ЕГН: ' . $this->owner_pin;
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function township()
    {
        return $this->belongsTo('App\Township');
    }

    public function populatedPlace()
    {
        return $this->belongsTo('App\PopulatedPlace');
    }
}
