<?php

namespace App\Providers;

use App\PageSetting;
use App\Review;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

       $reviews = Review::where('approved', true)->get();

       if ($reviews->count() > 3) {
           $reviews = $reviews->random(3);
       }

        View::share('reviews', $reviews);

        $settings = PageSetting::all();
        View::share('page_settings', $settings);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
