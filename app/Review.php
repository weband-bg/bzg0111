<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function getName()
    {
    	if ($this->name) {
    		return $this->name;
    	}

    	return 'Анонимен';
    }

    public function page()
    {
        return $this->belongsTo('App\PageSetting', 'page_id');
    }
}
