<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HoverBalloon extends Model
{
    public function module()
    {
    	return $this->belongsTo('App\Module');
    }
}
