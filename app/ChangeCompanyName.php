<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeCompanyName extends Model
{
    public function getNames()
    {
    	return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }
}
