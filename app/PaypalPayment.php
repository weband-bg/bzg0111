<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPayment extends Model
{
    public function module()
    {
    	return $this->belongsTo('App\PageSetting', 'module_id');
    }
}
