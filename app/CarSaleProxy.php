<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSaleProxy extends Model
{
    public function carType()
    {
        return $this->belongsTo('App\CarType');
    }

    public function carMake()
    {
        return $this->belongsTo('App\CarMake');
    }

    public function getSellerNames()
    {
        if ($this->seller_type == 'seller-legal-entity') {
            return $this->seller_name;
        } else {
            return $this->seller_firstname . ' ' . $this->seller_secondname . ' ' . $this->seller_lastname;
        }

    }

    public function getBuyerNames()
    {
        if ($this->buyer_type == 'buyer-legal-entity') {
            return $this->buyer_name;
        } else {
            return $this->buyer_firstname . ' ' . $this->buyer_secondname . ' ' . $this->buyer_lastname;
        }

    }

    public function getClientNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function getProxyNames()
    {
        return $this->proxy_firstname . ' ' . $this->proxy_secondname . ' ' . $this->proxy_lastname;
    }
}
