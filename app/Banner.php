<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function page_settings()
    {
    	return $this->belongsTo('App\PageSetting', 'page_setting_id');
    }
}