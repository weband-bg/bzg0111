<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyParticipation extends Model
{
    public function getNames()
    {
    	return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    }
}
