<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeCompanyActivitySubject extends Model
{
    public function getOwnerNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function activities()
    {
        return $this->belongsToMany('App\Company\Activity', 'change_company_activity_subject_activities', 'change_id')->withTimestamps();
    }
}
