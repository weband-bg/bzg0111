<?php

namespace App\Company\LTD;

use Illuminate\Database\Eloquent\Model;

class OtherLTDManager extends Model
{
	protected $table = 'other_ltd_managers';

    public function company()
    {
    	return $this->belongsTo('App\Company\LTD\LTD', 'company_ltd_id');
    }

    public function getNames()
    {
    	return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    }
}
