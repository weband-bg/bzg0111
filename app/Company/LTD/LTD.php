<?php

namespace App\Company\LTD;

use Illuminate\Database\Eloquent\Model;

class LTD extends Model
{
    protected $table = 'company_ltd';

    public function managers()
    {
    	return $this->hasMany('App\Company\LTD\OtherLTDManager', 'company_ltd_id');
    }

    public function activities()
    {
        return $this->belongsToMany('App\Company\Activity', 'ltd_activity', 'ltd_id')->withTimestamps();
    }

    public function ownerFullname()
    {
    	return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function managersNames()
    {
    	if (count($this->managers)) {
    		$managers = array();

            if ($this->manager == 1) {
                $managers[] = $this->ownerFullname();
            }

    		foreach ($this->managers as $manager) {
    			$managers[] = $manager->firstname . ' ' . $manager->secondname . ' ' . $manager->lastname;
    		}

    		return $managers;
    	}
    	else {
    		return array($this->ownerFullname());
    	}
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function township()
    {
        return $this->belongsTo('App\Township');
    }

    public function populatedPlace()
    {
        return $this->belongsTo('App\PopulatedPlace');
    }


    public function isManager()
    {
        return $this->manager ? 'Да' : 'Не' ;
    }

    public function getRepresentationMethod()
    {
        return $this->representation_method ? 'Заедно' : 'Заедно и поотделно' ;
    }
}
