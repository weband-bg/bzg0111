<?php

namespace App\Company\ET;

use Illuminate\Database\Eloquent\Model;

class ET extends Model
{
    protected $table = 'company_et';

    public function fullname()
    {
    	return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function activities()
    {
        return $this->belongsToMany('App\Company\Activity', 'et_activity', 'et_id')->withTimestamps();
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function township()
    {
        return $this->belongsTo('App\Township');
    }

    public function populatedPlace()
    {
        return $this->belongsTo('App\PopulatedPlace');
    }
}
