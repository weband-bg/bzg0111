<?php

namespace App\Company;


use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function ltd_companies()
    {
        return $this->belongsToMany('App\Company\LTD\LTD', 'ltd_activity')->withTimestamps();
    }

    public function ood_companies()
    {
        return $this->belongsToMany('App\Company\OOD\OOD', 'ood_activity')->withTimestamps();
    }

    public function et_companies()
    {
        return $this->belongsToMany('App\Company\ET\ET', 'et_activity')->withTimestamps();
    }
}
