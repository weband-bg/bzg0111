<?php

namespace App\Company\OOD;

use Illuminate\Database\Eloquent\Model;

class OtherOODManager extends Model
{
	protected $table = 'other_ood_managers';

    public function company()
    {
    	return $this->belongsTo('App\Company\OOD\OOD', 'company_ood_id');
    }

    public function fullName()
    {
    	return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    }
}