<?php

namespace App\Company\OOD;

use Illuminate\Database\Eloquent\Model;

class OOD extends Model
{
    protected $table = 'company_ood';

    public function owners()
    {
    	return $this->hasMany('App\Company\OOD\OtherOODOwner', 'company_ood_id');
    }

    public function activities()
    {
        return $this->belongsToMany('App\Company\Activity', 'ood_activity', 'ood_id')->withTimestamps();
    }

    public function managers()
    {
    	return $this->hasMany('App\Company\OOD\OtherOODManager', 'company_ood_id');
    }

    public function getManagers()
    {
        $managers = $this->managers;

        foreach ($this->owners as $owner) {
            if ($owner->manager) {
                $managers->push($owner);
            }
        }

        return $managers;
    }

    public function zone()
    {
        return $this->belongsTo('App\Zone');
    }

    public function township()
    {
        return $this->belongsTo('App\Township');
    }

    public function populatedPlace()
    {
        return $this->belongsTo('App\PopulatedPlace');
    }
}
