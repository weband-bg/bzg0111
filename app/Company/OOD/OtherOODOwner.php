<?php

namespace App\Company\OOD;

use Illuminate\Database\Eloquent\Model;

class OtherOODOwner extends Model
{
	protected $table = 'other_ood_owners';

    public function company()
    {
    	return $this->belongsTo('App\Company\OOD\OOD', 'company_ood_id');
    }

    public function fullName()
    {
    	return $this->firstname . ' ' . $this->secondname . ' ' . $this->lastname;
    }
}
