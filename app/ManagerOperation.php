<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerOperation extends Model
{
    public function getManagerNames()
    {

        switch ($this->operation) {
            case 1:
                return $this->add_manager_firstname . ' ' . $this->add_manager_secondname . ' ' . $this->add_manager_lastname;
                break;

            case 2:
                return $this->remove_manager_name;
                break;

            case 3:
                return $this->switch_manager_firstname . ' ' . $this->switch_manager_secondname . ' ' . $this->switch_manager_lastname;
                break;

            default:
                return 'Операция';
                break;
        }
    }


    public function getOperation()
    {
    	switch ($this->operation) {
    		case 1:
    			return 'Добавяне на още един управител';
    			break;

    		case 2:
    			return 'Заличаване на управител';
    			break;

    		case 3:
    			return 'Замяна на стар управител с нов';
    			break;

    		default:
    			return 'Операция';
    			break;
    	}
    }

    public function getOwnerNames()
    {
        return $this->owner_firstname . ' ' . $this->owner_secondname . ' ' . $this->owner_lastname;
    }

    public function getOwnerPIN()
    {
        return 'ЕГН: ' . $this->owner_pin;
    }
}
